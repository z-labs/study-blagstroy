<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/auth.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/../../vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/constants.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Благовещенскстрой: Материнский капитал</title>
    <link rel="stylesheet" href="/local/assets/local/common/common.css">
    <link rel="stylesheet" href="/local/assets/local/feedback-form/feedback-form.css">
    <link rel="stylesheet" href="/local/assets/local/purchase/purchase.css">
</head>
<body class="page">
<?php require_once 'header.php'; ?>

<div class="page-content">
    <div class="container">
        <?php
        echo $mustache->render('breadcrumbs', include(CONTEXT_DIR . '/article/breadcrumbs.php'));
        ?>
        <h1 class="page__title">
            Материнский капитал
        </h1>
    </div>
    <div class="purchase">
        <div class="container purchase__container">
            <div class="purchase__pattern-circle svg-pattern">
                <?php
                echo $mustache->render('background-circle-dashed');
                ?>
            </div>
            <div class="purchase__pattern-dots svg-pattern">
                <?php
                echo $mustache->render('background-dots');
                ?>
            </div>
            <?php
            echo $mustache->render('how-buy', include(CONTEXT_DIR . '/purchase/how-buy.php'));
            ?>
        </div>
        <div class="purchase__documents">
            <?php
            echo $mustache->render('sample-documents', include(CONTEXT_DIR . '/purchase/sample-documents.php'));
            ?>
        </div>
        <?php
        echo $mustache->render('consultation-form', include(CONTEXT_DIR . '/article/article-form.php'));
        echo $mustache->render(
            'buy-ways',
            include(CONTEXT_DIR . 'index/buy-ways.php')
        );
        echo $mustache->render(
            'other-projects',
            include(CONTEXT_DIR . '/project-detail/other-projects.php')
        );
        ?>
        <?php
        echo $mustache->render('scrollup');
        ?>
    </div>
</div>
<?php require_once 'footer.php'; ?>
<script>
    if (!window.initFeedback) {
        window.initFeedback = [];
    }
    window.initFeedback.push(function () {
        $('#article-form').feedbackForm();
    });
</script>

<script type="text/javascript" src="https://static.yandex.net/browser-updater/v1/script.js" defer></script>
<script src="/local/assets/local/common/common.js" defer></script>
<script src="/local/assets/vendor/form/jquery.form.min.js" defer></script>
<script src="/local/assets/local/feedback-form/feedback-form.js" defer></script>
<script src="https://yastatic.net/share2/share.js"></script>
<script src="/local/assets/local/purchase/purchase.js" defer></script>
</body>
</html>

<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/auth.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/../../vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/constants.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Благовещенскстрой: Контакты</title>
    <link rel="stylesheet" href="/local/assets/local/common/common.css">
    <link rel="stylesheet" href="/local/assets/local/feedback-form/feedback-form.css">
    <link rel="stylesheet" href="/local/assets/local/contacts/contacts.css">
</head>
<body class="page">
<?php require_once 'header.php'; ?>

<div class="page-content">
    <div class="container">
        <?php
        echo $mustache->render('breadcrumbs', include(CONTEXT_DIR . '/article/breadcrumbs.php'));
        ?>
        <h1 class="page__title">
            Контакты
        </h1>
    </div>
    <div class="contacts">
        <div class="contacts__pattern-circle svg-pattern">
            <?php
            echo $mustache->render('background-circle-dashed');
            ?>
        </div>

        <div class="contacts__pattern-dots svg-pattern">
            <?php
            echo $mustache->render('background-dots');
            ?>
        </div>
        <div class="container">
            <div class="contacts-top">
                <div class="contacts-group contacts-group_info">
                    <?
                    echo $mustache->render('contacts-info', include(CONTEXT_DIR . '/contacts/contacts-info.php'));
                    ?>
                </div>
                <div class="contacts-group contacts-group_map">
                    <?
                    echo $mustache->render('contacts-map',  include(CONTEXT_DIR . '/contacts/contacts-info.php'));
                    ?>
                </div>
            </div>
            <?
            echo $mustache->render('contacts-departments', include(CONTEXT_DIR . '/contacts/contacts-departments.php'));
            ?>
            <?
            echo $mustache->render('contacts-messengers-links', include(CONTEXT_DIR . '/contacts/contacts-messengers-links.php'));
            ?>
            <?php
            echo $mustache->render('contact-us-form', include(CONTEXT_DIR . '/contacts/contact-us-form.php'));
            ?>
        </div>
    </div>
</div>
<?php require_once 'footer.php'; ?>
<script>
    if (!window.initFeedback) {
        window.initFeedback = [];
    }
    window.initFeedback.push(function () {
        $('#contact-us-form').feedbackForm();
    });
</script>

<script type="text/javascript" src="https://static.yandex.net/browser-updater/v1/script.js" defer></script>
<script src="https://api-maps.yandex.ru/2.1/?apikey=946ef6a6-0104-4a71-9869-e801244187c6&lang=ru_RU" type="text/javascript"></script>
<script src="/local/assets/local/common/common.js" defer></script>
<script src="/local/assets/vendor/form/jquery.form.min.js" defer></script>
<script src="/local/assets/local/feedback-form/feedback-form.js" defer></script>
<script src="/local/assets/local/contacts/contacts.js" defer></script>
</body>
</html>

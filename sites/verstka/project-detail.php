<?php
//require_once $_SERVER['DOCUMENT_ROOT'] . '/auth.php';
use ZLabs\BxMustache\Projects\Detail;

require_once $_SERVER['DOCUMENT_ROOT'] . '/../../vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/constants.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);

/** @var Detail $detail */
$detail = include(CONTEXT_DIR . '/project-detail/detail.php');
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Благовещенскстрой: Проекты - детальная</title>
    <link rel="stylesheet" href="/local/assets/local/common/common.css">
    <link rel="stylesheet" href="/local/assets/local/feedback-form/feedback-form.css">
    <link rel="stylesheet" href="/local/assets/local/project-detail/project-detail.css">
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.css' rel='stylesheet'/>
    <link href='https://api.mapbox.com/styles/v1/krllgranin/ckanj64ek52kv1imw80uvwor7.html?fresh=true&title=copy&access_token=pk.eyJ1Ijoia3JsbGdyYW5pbiIsImEiOiJja2FuanQ2bzkwdnplMnJtdm56dXgyM2ptIn0.xm79ABTUKfwd4ugyzHW6rw'
          rel='stylesheet'/>
</head>
<body class="page page_mobile-menu-compact">
<?php require_once 'header.php'; ?>

<div class="page-content">
    <div class="project-detail">
        <section class="project-detail-first-screen">
            <?php
            echo $mustache->render('project-detail-first-screen__background', $detail);
            ?>
            <div class="container">
                <?php
                echo $mustache->render('breadcrumbs', include(CONTEXT_DIR . '/project-detail/breadcrumbs.php'));
                ?>
            </div>
            <?php
            echo $mustache->render('project-detail-first-screen-slider', $detail);
            ?>
            <?php
            echo $mustache->render('project-detail-first-screen-info', $detail);
            ?>
        </section>
        <section class="project-detail__navigation">
            <?php
            echo $mustache->render(
                'page-menu-navigation',
                include(CONTEXT_DIR . '/project-detail/page-menu-navigation.php')
            );
            ?>
        </section>
        <div class="project-detail__prices">
            <div class="container">
                <?php
                echo $mustache->render(
                    'project-detail-prices',
                    include(CONTEXT_DIR . '/project-detail/sidebar.php')
                );
                ?>
            </div>
        </div>
        <section class="project-detail__about-section page-screen" id="project-about">
            <div class="project-detail__about-section-pattern svg-pattern">
                <?php
                echo $mustache->render('background-circle-dashed');
                ?>
            </div>
            <div class="container project-detail-about-container">
                <div class="article-main">
                    <h2>О проекте</h2>
                    <div class="accent-text mobile-hide tablet-show">Многоквартирный жилой дом, СХПК Тепличный</div>
                    <?
                    echo $mustache->render('project-detail-about-slider', $detail->carousels->get(0));
                    ?>
                    <h3>Описание</h3>
                    <p>
                        9-этажный кирпичный моноговквартирный жилой дом с встроеными помещениями.
                        Внешнаяя отделка дома из силикатногго и керамического кирпича с расшивкой швов.
                        Внутренние перегородки из силикатного кирпича. На первом этаже предусмотрены коммерческие
                        помощения. В доме 4 подъезда, 210 квартир с черновой отделкой.
                    </p>
                    <blockquote>Проектная декларация размещена в единой информационной системе жилищного строительства
                        <a href="#" title="Перейти на сайт" target="_blank">наш.дом.рф</a>
                    </blockquote>

                    <?
                    echo $mustache->render('characteristics-table', $detail);
                    ?>

                    <h3>Отделка помещений</h3>
                    <?
                    echo $mustache->render('project-detail-futures', $detail);
                    ?>
                    <?
                    echo $mustache->render('index-tour-form', include(CONTEXT_DIR . 'index/tour-form-normal.php'));
                    ?>
                    <h2>Дворовая территория</h2>
                    <?
                    echo $mustache->render('project-detail-about-slider', $detail->carousels->get(1));
                    ?>
                    <h3>Благоустроенная дворовая территория</h3>
                    <p>Предусмотрено благоустройство прилегающей территории в виде размещения дворовых площадок:
                        детской, для отдыха взрослых.</p>
                    <p>Вокруг территории детской и спортивной площадок предусмотрено леерное ограждение.</p>
                    <p>В рамках благоустройства территории предусмотрено озеленение газонов путем посева многолетних
                        трав и деревьев.</p>
                </div>
                <?php
                echo $mustache->render(
                    'project-detail-about-side',
                    include(CONTEXT_DIR . '/project-detail/sidebar.php')
                );
                ?>
            </div>
        </section>
        <section class="project-detail__layout page-screen" id="project-layout">
            <div class="container">
                <h2>Типовые планировки объекта</h2>
                <h3>Многоквартирный жилой дом, СХПК Тепличный</h3>
                <?php
                echo $mustache->render(
                    'project-layout-tabs',
                    include(CONTEXT_DIR . '/project-detail/objects.php')
                );
                ?>
                <div class="project-layout-content-container">
                    <div id="liter-4" class="project-layout__content-item sections-list sections-list_show">
                        <h3 class="project-layout-content__title">Типовые планировки этажей</h3>
                        <div class="project-layout-content">
                            <?php
                            echo $mustache->render('project-layout-content');
                            echo $mustache->render(
                                'project-layout-content',
                                include(CONTEXT_DIR . '/project-detail/floors.php')
                            );
                            ?>
                            <?php
                            echo $mustache->render(
                                'project-layout-rooms-container',
                                include(CONTEXT_DIR . '/project-detail/apartments.php')
                            );
                            ?>
                        </div>
                    </div>
                    <div id="liter-5" class="project-layout__content-item sections-list">
                        <h3 class="project-layout-content__title">Типовые планировки этажей</h3>
                        <div class="project-layout-content">
                            <?php
                            echo $mustache->render('project-layout-content');
                            echo $mustache->render(
                                'project-layout-content',
                                include(CONTEXT_DIR . '/project-detail/floors-2.php')
                            );
                            ?>
                            <?php
                            echo $mustache->render(
                                'project-layout-rooms-container',
                                include(CONTEXT_DIR . '/project-detail/apartments-liter-5.php')
                            );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        echo $mustache->render('building', include(CONTEXT_DIR . '/project-detail/building.php'));
        ?>
        <?php
        echo $mustache->render('documents', include(CONTEXT_DIR . '/project-detail/documents.php'));
        ?>
        <?php
        echo $mustache->render(
            'infrastructure-map',
            include(CONTEXT_DIR . '/project-detail/infrastructure-map.php')
        )
        ?>
        <section class="project-detail__buy-ways page-screen" id="project-buy-ways">
            <?php
            echo $mustache->render(
                'buy-ways',
                include(CONTEXT_DIR . '/index/buy-ways.php')
            );
            ?>
        </section>
        <?php
        echo $mustache->render(
            'other-projects',
            include(CONTEXT_DIR . '/project-detail/other-projects.php')
        );
        echo $mustache->render('consultation-form_gray', include(CONTEXT_DIR . '/project-detail/consultation-form.php'));
        ?>
    </div>
</div>

<?php require_once 'footer.php'; ?>
<script>
    if (!window.initFeedback) {
        window.initFeedback = [];
    }
    window.initFeedback.push(function () {
        $('#article-form').feedbackForm();
    });
    window.initFeedback.push(function () {
        $('#index-tour-form').feedbackForm();
    });
</script>

<script type="text/javascript" src="https://static.yandex.net/browser-updater/v1/script.js" defer></script>
<script src="/local/assets/local/common/common.js" defer></script>
<script src="/local/assets/vendor/form/jquery.form.min.js" defer></script>
<script src="/local/assets/local/feedback-form/feedback-form.js" defer></script>
<script src="/local/assets/local/project-detail/project-detail.js" defer></script>
</body>
</html>

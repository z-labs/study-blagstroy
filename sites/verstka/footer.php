<footer class="footer clearfix">
    <div class="footer-container">
        <div class="footer-top">
            <div class="container footer-top-container">
                <div class="footer-top-section footer-top-section_contact">
                    <div class="footer__contact">
                        <a class="footer__contact-value desktop-hide"
                           href="tel:84162528017"
                           title="Позвонить 8 (4162) 528-017">8 (4162) 528-017</a>
                        <div class="footer__contact-value mobile-hide desktop-show-block">8 (4162) 528-017</div>
                        <div class="footer__contact-label">Отдел продаж</div>
                    </div>
                    <a href="#bell-form" class="call-button popup-link button button_linear-gray feedback-form-link">Заказать звонок</a>
                </div>

                <div class="footer-top-section">
                    <div class="footer-top__consultation">
                        <div class="footer-top__consultation-text">Требуется консультация?</div>
                        <a href="#email-form" class="footer-top__consultation-button popup-link button button_linear-gray feedback-form-link">Напишите нам</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="footer-content">
                <?php
                echo $mustache->render(
                    'footer-menu',
                    include(CONTEXT_DIR . '/footer-menu.php')
                );
                ?>
                <div class="footer-social">
                    <div class="footer__title">Мы в соцсетях</div>
                    <?php
                    echo $mustache->render(
                        'social-list',
                        include(CONTEXT_DIR . '/social.php')
                    )
                    ?>
                </div>
            </div>
            <div class="footer-copyrights">
                <div class="footer-copyrights-group">
                    <div class="footer-copyrights__company">АО «Благовещенскстрой», 2020</div>
                    <div class="footer-copyrights__text">Информация, предоставленная на сайте, не является публичной офертой. Сведения носят исключительно информационный характер.</div>
                </div>

                <!--noindex-->
                <a href="https://z-labs.ru/?source=blagstroy"
                   class="link-preset footer-dev button button_linear-gray" title="Z-Labs"
                   rel="nofollow" target="_blank"><span class="footer-dev__text">Разработано <strong>Студией Z-Labs</strong></span>
                </a>
                <!--/noindex-->
            </div>
        </div>
        <div class="footer__rectangles">
            <?php
            echo $mustache->render('background-rectangles')
            ?>
        </div>
    </div>
    <?php echo $mustache->render('modal-form', include(CONTEXT_DIR.'modal-form/bell-form.php')); ?>
    <?php echo $mustache->render('modal-form', include(CONTEXT_DIR.'modal-form/email-form.php')); ?>
</footer>
<script>
    if (!window.initFeedback) {
        window.initFeedback = [];
    }
    window.initFeedback.push(function () {
        $('#bell-form').feedbackForm();
    });
    window.initFeedback.push(function () {
        $('#email-form').feedbackForm();
    });
</script>
<script id="feedback-success-message"
        type="x-tmpl-mustache">
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/feedback-success-message.mustache') ?>
</script>
<script>
    console.info('%c%s', ['color: #8FD7FF;'].join(''), 'Вызови showSuccessMessage(), чтобы посмотреть модалку для формы')
    var showSuccessMessage = function () {
        $.fancybox.open(
            Mustache.render($('#feedback-success-message').html(), {
                title: 'Заявка успешно отправлена!',
                text: 'Наш специалист свяжется с вами в ближайшее время'
            }),
            window.customFancyboxSettings
        );
    }
</script>

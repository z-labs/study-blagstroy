<?php

use ZLabs\BxMustache\About\Stats\Item as StatItem;
use ZLabs\BxMustache\Svg;
use ZLabs\BxMustache\About\IndexItem;
use ZLabs\BxMustache\Image;
use ZLabs\BxMustache\Link;

$stats = collect([
    [
        'icon' => '/local/assets/images/index-about-stats/years.svg',
        'number' => "60",
        "unit" => "лет",
        "text" => "на строительном
            рынке <b>Приамурья</b>"
    ],
    [
        'icon' => '/local/assets/images/index-about-stats/meters.svg',
        'number' => "1000",
        "unit" => "м<sup>2</sup>",
        "text" => "<b>построили</b>
            с 1960 года"
    ],
    [
        'icon' => '/local/assets/images/index-about-stats/persent.svg',
        'number' => "0",
        "unit" => "%",
        "text" => "<b>беспроцентная</b> рассрочка
            от застройщика."
    ],
])->map(function ($arItem) {
    $item = new StatItem;
    $item->icon = new Svg();
    $item->icon->src = $arItem['icon'];
    $item->unit = $arItem['unit'];
    $item->text = $arItem['text'];
    $item->number = $arItem['number'];

    return $item;
});

$slides = collect([
    [
        "title" => "Многолетний опыт компании",
        "text" => "История современной строительной компании уходит корнями в 1960 год — именно тогда в Благовещенске 
появился строительно-монтажный трест №2 Амурского совнархоза. Амурского совнархоза.<br><br> В 1992 году трест поменял 
форму собственности и компания приобрела экономическую самостоятельность.",
        "image" => "/local/assets/images/index-about-slider/img-2.jpg",
        'link' => '#',
        "linkTabletText" => "Подробнее",
        "linkDesktopText" => "Узнать больше",
        "linkBlank" => false
    ],
    [
        "title" => "Многолетний опыт компании",
        "text" => "Даже сегодня амурчане видят результаты работы — научную библиотеку, центральный универмаг, стадион 
«Амур» и три гостиницы — «Дружба», «Зея» и «Юбилейная». Жители города пользуются ресурсами водозаборов на реке Амур и 
реке Зея.",
        "image" => "/local/assets/images/index-about-slider/img-3.jpg",
        'link' => '#',
        "linkTabletText" => "Подробнее",
        "linkDesktopText" => "Узнать больше",
        "linkBlank" => false
    ],
    [
        "title" => "Многолетний опыт компании",
        "text" => "Флагман компании «Благовещенскстрой» — собор Благовещенья Пресвятой Богородицы, одна из визитных 
карточек областного центра. Благовещенья Пресвятой Богородицы, одна из визитных карточек областного центра.<br><br>
Компания «Благовещенскстрой» не только выполнила все работы по зданию, но и являлась одним из крупных благотворителей
 этого проекта.",
        "image" => "/local/assets/images/index-about-slider/img-4.jpg",
        'link' => '#',
        "linkTabletText" => "Подробнее",
        "linkDesktopText" => "Узнать больше",
        "linkBlank" => false
    ],
])->map(function ($arItem) {
    $item = new IndexItem;
    $item->strMainId = rand(100, 1000);
    $item->title = $arItem['title'];
    $item->text = $arItem['text'];
    $item->image = new Image();
    $item->image->src = $arItem['image'];

    $item->link = new Link();
    $item->link->href = $arItem['link'];
    $item->link->targetBlank = $arItem['linkBlank'];
    $item->link->text = $arItem['linkDesktopText'];
    $item->link->additionalCssClass = $arItem['linkTabletText'];
    return $item;
});

return [
    'title' => 'Качество, проверенное временем',
    'stats' => $stats,
    'slides' => $slides
];
<?php

use ZLabs\BxMustache\MainSlider\Item;
use ZLabs\BxMustache\Link;
use ZLabs\BxMustache\AdaptiveImage;

$items = collect([
    [
        "title" => "<b>Строительство</b> и реконструция обьектов любой сложности",
        "subtitle" => "Работаем на строительном рынке Приамурья с <b>1960 года</b>",
        "href" => "#",
        "imgSrc" => "/local/assets/images/temp/index-first-screen/slider/background-desktop.jpg",
        "imgLg" => "/local/assets/images/temp/index-first-screen/slider/background-tablet.jpg",
        "bgColor" => "#F0F4F5"
    ],
    [
        "title" => "<b>Строительство</b> и реконструция обьектов любой сложности",
        "subtitle" => "Работаем на строительном рынке Приамурья с <b>1960 года</b>",
        "href" => "#",
        "imgSrc" => "/local/assets/images/temp/index-first-screen/slider/background-desktop.jpg",
        "imgLg" => "/local/assets/images/temp/index-first-screen/slider/background-tablet.jpg",
        "bgColor" => "#F0F4F5"
    ],
    [
        "title" => "<b>Строительство</b> и реконструция обьектов любой сложности",
        "subtitle" => "Работаем на строительном рынке Приамурья с <b>1960 года</b>",
        "href" => "#",
        "imgSrc" => "/local/assets/images/temp/index-first-screen/slider/background-desktop.jpg",
        "imgLg" => "/local/assets/images/temp/index-first-screen/slider/background-tablet.jpg",
        "bgColor" => "#F0F4F5"
    ],
])->map(function ($arItem){
    $item = new Item();
    $item->strMainId = rand(100, 1000);
    $item->title = $arItem['title'];
    $item->subtitle = $arItem['subtitle'];
    $item->image = new AdaptiveImage();
    $item->image->src = $arItem['imgSrc'];
    $item->image->lgSrc = $arItem['imgLg'];
    $item->backGround = $arItem['bgColor'];
    $item->link = new Link();
    $item->link->href = $arItem['href'];
    $item->link->text = "Задать вопрос";
    return $item;
});

return $items;
<?php

return [
    'ajax_component_id' => 'index-tour-form',
    'action' => '',
    'title' => 'Запишитесь на экскурсию по объекту',
    'sub_title' => 'Заполните форму и наш менеджер <b>согласует с вами удобную дату и время экскурсии</b> по интересующему <b>объекту</b>',
    'submit_text' => 'Записаться на экскурсию',
    'controls' => [
        'name' => [
            'placeholder' => 'Ваше имя',
            'code' => 'name',
            'required' => true,
            'requiredCssClass' => ' feedback-form__control_required',
            'additional_css_class' => ' feedback-form__name',
            'value' => ''
        ],
        'phone' => [
            'placeholder' => 'Телефон',
            'maskCssClass' => ' feedback-form__control_valid_phone',
            'code' => 'phone',
            'required' => true,
            'requiredCssClass' => ' feedback-form__control_required',
            'typeInput' => 'tel',
            'value' => '',
        ],
        'date' => [
            'placeholder' => 'Дата экскурсии',
            'code' => 'datetime',
            'required' => false,
            'requiredCssClass' => ' feedback-form__control_required',
            'additional_css_class' => ' feedback-form__control_type_date_disable_weekends'
        ],

    ],
    'user_consent' => 'Отправляя заявку, Вы соглашаетесь на обработку <a href="#" title="Согласие на обработку персональных данных" target="_blank">персональных данных</a>',
];

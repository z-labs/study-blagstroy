<?php

$ctx = include('tour-form.php');

$ctx['html_fields'] = [];
foreach ($ctx['controls'] as $control) {

    $template = 'form__control_type_text';
    if (isset($control['possibleValues'])) {
        $template = 'form__control_type_select';
    }

    if ($control['code'] === 'datetime') {
        $template = 'form__control_type_datetime';
    }
    $ctx['html_fields'][] = ['html' => $mustache->render($template, $control)];
}

return $ctx;

<?php

return [
    'needShowDropdown' => true,// ключ, который отвечает за показ выпадающего списка
    'categories' => collect([
        [
            'text' => "Все",
            'id' => 'announcements-all',
            "isFirst" => true
        ],
        [
            'text' => "Полезные статьи",
            'id' => 'announcements-articles'
        ],
        [
            'text' => "Новости",
            'id' => 'announcements-news'
        ],
        [
            'text' => "Акции",
            'id' => 'announcements-actions'
        ],
    ])
];

<?php

use Illuminate\Support\Collection;
use ZLabs\BxMustache\Link;
use ZLabs\BxMustache\Sale\Item;

/** @var Collection $items */
$items = include(__DIR__ . '/../sale.php');

$link = new Link;
$link->href = '#';
$link->text = 'Все предложения';

return collect([
    'title' => 'Горячие предложения',
    'categories' => include(__DIR__ . '/../sale-categories.php'),
    'needShowDropdown' => true,// ключ, который отвечает за показ выпадающего списка
    'items' => $items
        ->groupBy(function (Item $item) {
            return $item->categoryId;
        })
        ->map(function (Collection $items, $key) {
            return [
                'id' => $key,
                'active' => $key === 1,
                'items' => $items->values()
            ];
        })
        ->values(),
    'link' => $link
]);

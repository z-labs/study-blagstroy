<?php

use ZLabs\BxMustache\Image;

$items = collect([
    [
        'name' => 'Сбербанк',
        'photo' => '/local/assets/images/partners-list/logo-1.png'
    ],
    [
        'name' => 'ВТБ',
        'photo' => '/local/assets/images/partners-list/logo-2.png'
    ],
    [
        'name' => 'РоссельхозБанк',
        'photo' => '/local/assets/images/partners-list/logo-3.png'
    ],
    [
        'name' => 'АТБ',
        'photo' => '/local/assets/images/partners-list/logo-4.png'
    ],
    [
        'name' => 'Газпромбанк',
        'photo' => '/local/assets/images/partners-list/logo-5.png'
    ],
    [
        'name' => 'Промсвязьбанк',
        'photo' => '/local/assets/images/partners-list/logo-6.png'
    ]
])->map(function ($arItem){
    $item = new Image();
    $item->id = rand(100, 1000);
    $item->src = $arItem['photo'];
    $item->title = $arItem['name'];
    $item->alt = $arItem['name'];

    return $item;
});

return $items;
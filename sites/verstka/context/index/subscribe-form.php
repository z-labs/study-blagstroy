<?php

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);

return [
    'ajax_component_id' => 'subscribe-form',
    'action' => '',
    'name' => 'subscribe_form',
    'title' => 'Подпишитесь на рассылку',
    'sub_title' => 'Мы присылаем только полезные новости и специальные предложения раньше других',
    'submit_text' => '',
    'html_fields' => [
        'html' => $mustache->render('form__control_type_text', [
            'placeholder' => 'Электронная почта',
            'code' => 'subscribe_form_email',
            'required' => true,
            'requiredCssClass' => ' feedback-form__control_required',
            'value' => ''
        ]),
    ],
    'user_consent' => 'Отправляя заявку, Вы соглашаетесь на обработку <a href="#" title="Согласие на обработку персональных данных" target="_blank">персональных данных</a>',
];

<?php

use ZLabs\BxMustache\AdaptiveImage;

return collect([
    [
        "src" => "/local/assets/images/company/img-1-dt.jpg",
        "md" => "/local/assets/images/company/img-1.jpg",
        "lg" => "/local/assets/images/company/img-1-tab.jpg"
    ],
    [
        "src" => "/local/assets/images/company/img-2-dt.jpg",
        "md" => "/local/assets/images/company/img-2.jpg",
        "lg" => "/local/assets/images/company/img-2-tab.jpg"
    ],
    [
        "src" => "/local/assets/images/company/img-3-dt.jpg",
        "md" => "/local/assets/images/company/img-3.jpg",
        "lg" => "/local/assets/images/company/img-3-tab.jpg"
    ],
])->map(function ($arItem) {
    $item = new AdaptiveImage();
    $item->src = $arItem['src'];
    $item->mdSrc = $arItem['md'];
    $item->lgSrc = $arItem['lg'];
    $item->realSrc = $arItem['src'];

    return $item;
});

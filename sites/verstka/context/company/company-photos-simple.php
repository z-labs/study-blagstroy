<?php

use ZLabs\BxMustache\AdaptiveImage;

return collect([
    [
        'src' => '/local/assets/images/company/logo-bsk-desktop.png',
        'md' => '/local/assets/images/company/logo-bsk-m.png',
        'lg' => '/local/assets/images/company/logo-bsk-tablet.png'
    ]
])->map(function ($arItem) {
    $item = new AdaptiveImage();
    $item->src = $arItem['src'];
    $item->mdSrc = $arItem['md'];
    $item->lgSrc = $arItem['lg'];
});

<?php

use ZLabs\BxMustache\AdaptiveImage;

return collect([
    [
        'src' => "/local/assets/images/company/img1-dt.jpg",
        "md" => "/local/assets/images/company/img1.jpg",
        "lg" => "/local/assets/images/company/img1-t.jpg"
    ],
    [
        'src' => "/local/assets/images/company/img2-dt.jpg",
        "md" => "/local/assets/images/company/img2.jpg",
        "lg" => "/local/assets/images/company/img2-t.jpg"
    ],
])->map(function ($arItem) {
    $item = new AdaptiveImage();

    $item->src = $arItem['src'];
    $item->mdSrc = $arItem['md'];
    $item->lgSrc = $arItem['lg'];
    $item->realSrc = $arItem['src'];

    return $item;
});

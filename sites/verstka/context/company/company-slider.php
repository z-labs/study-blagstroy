<?php

use ZLabs\BxMustache\AdaptiveImage;

return collect([
    [
        'src' => "/local/assets/images/company-slider/image1-dt.jpg",
        'md' => "/local/assets/images/company-slider/image1-m.jpg",
        "lg" => "/local/assets/images/company-slider/image1-m.jpg"
    ],
    [
        'src' => "/local/assets/images/company-slider/image2-dt.jpg",
        'md' => "/local/assets/images/company-slider/image2-m.jpg",
        "lg" => "/local/assets/images/company-slider/image2-m.jpg"
    ],
    [
        'src' => "/local/assets/images/company-slider/image3-dt.jpg",
        'md' => "/local/assets/images/company-slider/image3-m.jpg",
        "lg" => "/local/assets/images/company-slider/image3-m.jpg"
    ],
])->map(function ($arItem) {
    $item = new AdaptiveImage();
    $item->src = $arItem['src'];
    $item->mdSrc = $arItem['md'];
    $item->lgSrc = $arItem['lg'];
    $item->realSrc = $arItem['src'];

    return $item;
});

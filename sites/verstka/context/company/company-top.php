<?php

use ZLabs\BxMustache\Image;

$slider = collect([
    [
        "image" => "/local/assets/images/company-top/img1.jpg",
        "description" => "Площадь В. И. Ленина 1970 г. Благовещенск",
    ],
    [
        "image" => "/local/assets/images/company-top/img2.jpg",
        "description" => "Второй текст",
    ],
    [
        "image" => "/local/assets/images/company-top/img3.jpg",
        "description" => "Третий текст",
    ]
])->map(function ($arItem) {
    $item = new Image();
    $item->src = $arItem['image'];
    $item->description = $arItem['description'];

    return $item;
});

return [
    "title" => "Как все начиналось",
    "firstText" => "Площадь В. И. Ленина 1970 г. Благовещенск",
    "slider" => $slider
];

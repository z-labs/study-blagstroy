<?php

use ZLabs\BxMustache\Company\EmployeeItem;
use ZLabs\BxMustache\Image;

return collect([
    [
        "image" => "/local/assets/images/company-employee/photo-1.png",
        "name" => "Ольга Васильева",
        "position" => "главный бухгалтер",
        "experience" => "11 лет в компании",
        "text" => "
                <b>Я работаю с профессионалами.</b>
                <p>Наши сотрудники — высококвалифицированные специалисты узкого профиля.</p>
                <p>Плечом к плечу мы трудимся с людьми старой закалки: они никогда не откажут в совете и помощи.
                    «Благовещенскстрой» стал настоящей кузницей кадров: всегда есть у кого поучиться, чтобы самой
                    стать лучшим профессионалом,
                    чем ты есть</p>"
    ],
    [
        "image" => "/local/assets/images/company-employee/photo-2.png",
        "name" => "Владимир Баранов",
        "position" => "начальник производственно - технического отдела",
        "experience" => "25 лет в компании",
        "text" => "
                <b>Компания держит слово</b>
                <p>Это видно и по работе с заказчиками, которым мы всегда стараемся сдать все объекты качественно и в
                    сроки, так и по обязательствам перед рядовыми сотрудниками. Ни разу нам не задержали зарплату, даже
                    во время кризисных времен. Я работаю с 1995 года в ОАО «Благовещенскстрой, и никогда предприятие не
                    уклонялось от своих обязательств.</p>"
    ],
    [
        "image" => "/local/assets/images/company-employee/photo-3.png",
        "name" => "Владимир Остапенко",
        "position" => "НО капитального строительства",
        "experience" => "11 лет в компании",
        "text" => "
                <b>В коллективе приятно работать.</b>
                <p>Это грамотные люди, которые знают
                        свое дело досконально и принимают взвешенные решения.</p>"
    ],
])->map(function ($arItem) {
    $item = new EmployeeItem();

    $item->name = $arItem['name'];
    $item->text = $arItem['text'];
    $item->experience = $arItem['experience'];
    $item->position = $arItem['position'];

    $item->image = new Image();
    $item->image->title = $arItem['name'];
    $item->image->alt = $arItem['name'];
    $item->image->src = $arItem['image'];

    return $item;
});

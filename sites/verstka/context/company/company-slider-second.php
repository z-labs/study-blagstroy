<?php

use ZLabs\BxMustache\AdaptiveImage;

return collect([
    [
        'src' => "/local/assets/images/company-slider-second/img-1-dt.jpg",
        'md' => "/local/assets/images/company-slider-second/img-1.jpg",
        "lg" => "/local/assets/images/company-slider-second/img-1-tab.jpg",
        "description" => "Собор Благовещенья Пресвятой Богородицы Благовешенск, Релочный пер., 15"
    ],
    [
        'src' => "/local/assets/images/company-slider-second/img-2-dt.jpg",
        'md' => "/local/assets/images/company-slider-second/img-1.jpg",
        "lg" => "/local/assets/images/company-slider-second/img-1-tab.jpg",
        "description" => "Собор Благовещенья Пресвятой Богородицы Благовешенск, Релочный пер., 15"
    ],
    [
        'src' => "/local/assets/images/company-slider-second/img-3-dt.jpg",
        'md' => "/local/assets/images/company-slider-second/img-1.jpg",
        "lg" => "/local/assets/images/company-slider-second/img-1-tab.jpg",
        "description" => "Собор Благовещенья Пресвятой Богородицы Благовешенск, Релочный пер., 15"
    ],
])->map(function ($arItem) {
    $item = new AdaptiveImage();
    $item->src = $arItem['src'];
    $item->mdSrc = $arItem['md'];
    $item->lgSrc = $arItem['lg'];
    $item->description = $arItem['description'];

    return $item;
});

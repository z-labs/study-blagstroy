<?php

$items = [
    [
        "name" => "Как всё начиналось",
        "code" => "top",
        "isFirst" => true
    ],
    [
        "name" => "Новое имя",
        "code" => "new-name",
        "isFirst" => false
    ],
    [
        "name" => "Карта строительства",
        "code" => "construction-map",
        "isFirst" => false
    ],
    [
        "name" => "Кадры и коллектив — главная ценность",
        "code" => "collective",
        "isFirst" => false
    ],
    [
        "name" => "С реальностью — в будущее",
        "code" => "future",
        "isFirst" => false
    ],
];

return [
    "items" => $items
];

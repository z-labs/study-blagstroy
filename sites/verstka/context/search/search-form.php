<?php

return [
    'name' => 'form-name',
    'control-name' => 'q',
    'q' => 'Квартира однокомнатная',
    'groups' => collect([
        [
            'id' => '1',
            'name' => 'Проекты компании',
            'checked' => true
        ],
        [
            'id' => '2',
            'name' => 'Горячие предложения',
            'checked' => true
        ],
        [
            'id' => '3',
            'name' => 'Новости и Статьи',
            'checked' => true
        ],
        [
            'id' => '4',
            'name' => 'Документы',
            'checked' => true
        ],
        [
            'id' => '5',
            'name' => 'Способы покупки',
            'checked' => false
        ]
    ])
];

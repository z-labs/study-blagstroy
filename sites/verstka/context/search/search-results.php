<?php

use \ZLabs\BxMustache\Search\Item;
use ZLabs\BxMustache\Image;
use ZLabs\BxMustache\Svg;
use ZLabs\BxMustache\Link;

return collect([
    [
        'name' => 'Проекты Компании',
        'items' => collect([
            [
                'name' => "Многоквартирный дом, Литер 2",
                "topInfo" => "Дата реализации: II квартал 2020 г.",
                "botInfo" => "Благовещенск, ул. Амурская, 270/1",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/blog-item/blog-item1-dt.jpg',
                    'alt' => 'Alt for picture',
                    'title' => 'Title for picture',
                    'height' => '80',
                    'width' => '120'
                ]
            ],
            [
                'name' => "Многоквартирный дом, Литер 2",
                "topInfo" => "Дата реализации: II квартал 2020 г.",
                "botInfo" => "Благовещенск, ул. Амурская, 270/1",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/blog-item/blog-item1-dt.jpg',
                    'alt' => 'Alt for picture',
                    'title' => 'Title for picture',
                    'height' => '80',
                    'width' => '120'
                ]
            ],
            [
                'name' => "Многоквартирный дом, Литер 2",
                "topInfo" => "Дата реализации: II квартал 2020 г.",
                "botInfo" => "Благовещенск, ул. Амурская, 270/1",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/blog-item/blog-item1-dt.jpg',
                    'alt' => 'Alt for picture',
                    'title' => 'Title for picture',
                    'height' => '80',
                    'width' => '120'
                ]
            ],
        ])
    ],
    [
        'name' => 'Горячие предложения',
        'items' => collect([
            [
                'name' => "Квартира-пентхаус, 110 м2",
                "topInfo" => "Офисы",
                "botInfo" => "II квартал 2020 г. Благовещенск, ул. Амурская, 270/1",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/blog-item/blog-item1-dt.jpg',
                    'alt' => 'Alt for picture',
                    'title' => 'Title for picture',
                    'height' => '80',
                    'width' => '120'
                ]
            ],
            [
                'name' => "Квартира-пентхаус, 110 м2",
                "topInfo" => "Офисы",
                "botInfo" => "II квартал 2020 г. Благовещенск, ул. Амурская, 270/1",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/blog-item/blog-item1-dt.jpg',
                    'alt' => 'Alt for picture',
                    'title' => 'Title for picture',
                    'height' => '80',
                    'width' => '120'
                ]
            ],
            [
                'name' => "Квартира-пентхаус, 110 м2",
                "topInfo" => "Офисы",
                "botInfo" => "II квартал 2020 г. Благовещенск, ул. Амурская, 270/1",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/blog-item/blog-item1-dt.jpg',
                    'alt' => 'Alt for picture',
                    'title' => 'Title for picture',
                    'height' => '80',
                    'width' => '120'
                ]
            ],
        ])
    ],
    [
        'name' => 'Новости и статьи',
        'items' => collect([
            [
                'name' => "Помощь в оформление ипотеки для многодетных семей",
                "topInfo" => "Рубрика",
                "botInfo" => "28 апреля 2020",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/blog-item/blog-item1-dt.jpg',
                    'alt' => 'Alt for picture',
                    'title' => 'Title for picture',
                    'height' => '80',
                    'width' => '120'
                ]
            ],
            [
                'name' => "Помощь в оформление ипотеки для многодетных семей",
                "topInfo" => "Рубрика",
                "botInfo" => "28 апреля 2020",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/blog-item/blog-item1-dt.jpg',
                    'alt' => 'Alt for picture',
                    'title' => 'Title for picture',
                    'height' => '80',
                    'width' => '120'
                ]
            ],
            [
                'name' => "Помощь в оформление ипотеки для многодетных семей",
                "topInfo" => "Рубрика",
                "botInfo" => "28 апреля 2020",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/blog-item/blog-item1-dt.jpg',
                    'alt' => 'Alt for picture',
                    'title' => 'Title for picture',
                    'height' => '80',
                    'width' => '120'
                ]
            ],
        ])
    ],
    [
        'name' => 'Новости и статьи',
        'items' => collect([
            [
                'name' => "Материнский капитал",
                "topInfo" => "Помощь в приобретении",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/temp/buying-methods/icon-1.svg',
                ]
            ],
            [
                'name' => "Материнский капитал",
                "topInfo" => "Помощь в приобретении",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/temp/buying-methods/icon-1.svg',
                ]
            ],
            [
                'name' => "Материнский капитал",
                "topInfo" => "Помощь в приобретении",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/temp/buying-methods/icon-1.svg',
                ]
            ],
        ])
    ],
    [
        'name' => 'Документы',
        'items' => collect([
            [
                'name' => "План контроля в сфере строительства жилых обьектов №1012",
                "botInfo" => "Добавлено 9 апреля 2020",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/file-icons/embed.svg',
                ],
                'isFile' => true
            ],
            [
                'name' => "План контроля в сфере строительства жилых обьектов №1012",
                "botInfo" => "Добавлено 9 апреля 2020",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/file-icons/embed.svg',
                ],
                'isFile' => true
            ],
            [
                'name' => "План контроля в сфере строительства жилых обьектов №1012",
                "botInfo" => "Добавлено 9 апреля 2020",
                'link' => [
                    'href' => '#'
                ],
                'image' => [
                    'src' => '/local/assets/images/file-icons/embed.svg',
                ],
                'isFile' => true
            ],
        ])
    ],
])->map(function ($arItem){
    $item = [
        'name' => $arItem['name'],
        'items' => $arItem['items']->map(function ($arElem){
            $item = new Item();
            $item->name = $arElem['name'];

            if(isset($arElem['topInfo']))
                $item->topInfo = $arElem['topInfo'];
            if(isset($arElem['botInfo']))
                $item->botInfo = $arElem['botInfo'];
            if(isset($arElem['isFile']))
                $item->isFile = $arElem['isFile'];

            $item->link = new Link();
            $item->link->href = $arElem['link']['href'];

            if(strpos($arElem['image']['src'],".svg")) {
                $item->image = new Svg();
                $item->image->src = $arElem['image']['src'];
            }else{
                $item->image = new Image();
                $item->image->src = $arElem['image']['src'];
            }
            return $item;
        })
    ];

    return $item;
});

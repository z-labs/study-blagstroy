<?php

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);

return [
    'ajax_component_id' => 'email-form',
    'action' => '',
    'name' => 'email-form',
    'title' => 'Написать нам',
    'sub_title' => 'Оставьте Ваш вопрос или предложение и мы обязательно ответим вам',
    'submit_text' => 'Написать нам',
    'html_fields' => [
        [
            'html' => $mustache->render('form__control_type_text', [
                'placeholder' => 'Ваше имя',
                'code' => 'name',
                'required' => true,
                'requiredCssClass' => ' feedback-form__control_required',
                'value' => ''
            ])
        ],
        [
            'html' => $mustache->render('form__control_type_text', [
                'placeholder' => 'E-mail',
                'maskCssClass' => ' feedback-form__control_valid_email',
                'code' => 'email',
                'required' => true,
                'requiredCssClass' => ' feedback-form__control_required',
                'value' => '',
                'typeInput' => '',
            ])
        ],
        [
            'html' => $mustache->render('form__control_type_textarea', [
                'placeholder' => 'Комментарий',
                'code' => 'text',
                'required' => true,
                'requiredCssClass' => ' ',
                'value' => ''
            ])
        ],
    ],
    'user_consent' => 'Отправляя заявку, Вы соглашаетесь на обработку <a href="#" title="Согласие на обработку персональных данных" target="_blank">персональных данных</a>',
];
<?php

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);

return [
    'ajax_component_id' => 'bell-form',
    'action' => '',
    'name' => 'bell-form',
    'title' => 'Заказать звонок',
    'sub_title' => 'Заполните форму и наш специалист свяжется с вами в ближайшее время',
    'submit_text' => 'Заказать звонок',
    'html_fields' => [
        [
            'html' => $mustache->render('form__control_type_text', [
                'placeholder' => 'Ваше имя',
                'code' => 'name',
                'required' => true,
                'requiredCssClass' => ' feedback-form__control_required',
                'value' => ''
            ])
        ],
        [
            'html' => $mustache->render('form__control_type_text', [
                'placeholder' => 'Телефон',
                'maskCssClass' => ' feedback-form__control_valid_phone',
                'code' => 'phone',
                'required' => true,
                'requiredCssClass' => ' feedback-form__control_required',
                'value' => '',
                'typeInput' => 'tel',
            ])
        ],
        [
            'html' => $mustache->render('form__control_type_text', [
                'placeholder' => 'E-mail',
                'maskCssClass' => ' feedback-form__control_valid_email',
                'code' => 'email',
                'required' => true,
                'requiredCssClass' => ' feedback-form__control_required',
                'value' => '',
                'typeInput' => '',
            ])
        ],
        [
            'html' => $mustache->render('form__control_type_textarea', [
                'placeholder' => 'Комментарий',
                'code' => 'text',
                'required' => true,
                'requiredCssClass' => ' ',
                'value' => ''
            ])
        ],
    ],
    'user_consent' => 'Отправляя заявку, Вы соглашаетесь на обработку <a href="#" title="Согласие на обработку персональных данных" target="_blank">персональных данных</a>',
];
<?php

use ZLabs\BxMustache\Menu\Item;

return [
    'needShowDropdown' => true,// ключ, который отвечает за показ выпадающего списка
    'categories' => collect([
        [
            'title' => 'Все статьи',
            'active' => true,
            'link' => '#',
        ],
        [
            'title' => 'Акции',
            'active' => false,
            'link' => '#',
        ],
        [
            'title' => 'Новости',
            'active' => false,
            'link' => '#',
        ],
        [
            'title' => 'Рекомендации',
            'active' => false,
            'link' => '#',
        ],
        [
            'title' => 'Юридические вопросы',
            'active' => false,
            'link' => '#',
        ],
        [
            'title' => 'Исторические объекты',
            'active' => false,
            'link' => '#',
        ],
    ])->map(function ($arItem) {
        $item = new Item;
        $item->text = $arItem['title'];
        $item->href = $arItem['link'];
        $item->active = $arItem['active'];

        return $item;
    })
];

<?php

use ZLabs\BxMustache\PageLink;

$links = collect([]);

$empty = new PageLink();
$empty->text = '. . .';
$empty->active = false;

for ($i = 0; $i < 6; ++$i) {
    $link = new PageLink;
    $link->href = '#';
    $link->text = $i + 1;
    $link->active = $i === 1;
    if($i == 1){
        $links->push($empty);
    }

    $links->push($link);
}

$last = new PageLink();
$last->href = '#';
$last->text = 9;
$last->active = false;

$links->push($empty);
$links->push($last);

$prev = new PageLink;
$prev->href = '#back';
$prev->text = 'Назад';

$next = new PageLink;
$next->href = '#next';
$next->text = 'Вперёд';

$pageNavihation = [
    'prevLink' => $prev,
    'links' => $links,
    'nextLink' => $next
];


return [
    'pageNavigation' => $pageNavihation
];
<?php

return [
    'ajax_component_id' => 'contact-us-form',
    'action' => '',
    'title' => 'Напишите нам',
    'sub_title' => 'Оставьте Ваш вопрос или предложение и мы обязательно ответим вам',
    'submit_text' => 'Отправить<span class="mobile-hide tablet-show"> сообщение</span>',
    'controls' => [
        'name' => [
            'placeholder' => 'Ваше имя',
            'code' => 'name',
            'required' => true,
            'requiredCssClass' => ' feedback-form__control_required',
            'value' => ''
        ],
        'phone' => [
            'placeholder' => 'Телефон',
            'code' => 'phone',
            'maskCssClass' => ' feedback-form__control_valid_phone',
            'required' => true,
            'requiredCssClass' => ' feedback-form__control_required',
            'typeInput' => 'tel',
            'value' => '',
        ],
        'email' => [
            'placeholder' => 'E-mail',
            'code' => 'email',
            'maskCssClass' => ' feedback-form__control_valid_email',
            'required' => true,
            'requiredCssClass' => ' feedback-form__control_required',
            'additional_css_class' => ' feedback-form__email',
            'value' => ''
        ],
        'message' => [
            'placeholder' => 'Текст сообщения',
            'code' => 'message',
            'required' => true,
            'typeInput' => 'textarea',
            'requiredCssClass' => ' feedback-form__control_required',
            'value' => ''
        ],
    ],
    'user_consent' => 'Отправляя заявку, Вы соглашаетесь на обработку <a href="#" title="Согласие на обработку персональных данных" target="_blank">персональных данных</a>',
];
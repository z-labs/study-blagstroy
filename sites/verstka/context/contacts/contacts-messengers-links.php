<?php

use ZLabs\BxMustache\MessengerLink;
use ZLabs\BxMustache\Svg;

$links = collect([
    [
        'title' => "Telegram",
        'href' => '#',
        'icon' => "/local/assets/images/contacts-messengers/telegram.svg"
    ],
    [
        'title' => "WhatsApp",
        'href' => '#',
        'icon' => "/local/assets/images/contacts-messengers/whatsapp.svg"
    ],
    [
        'title' => "Онлайн-чат",
        'href' => '#',
        'icon' => "/local/assets/images/contacts-messengers/onlinechat.svg"
    ],
])->map(function ($arItem){
    $item = new MessengerLink();
    $item->text = $arItem['title'];
    $item->href = $arItem['href'];
    $item->icon = new Svg();
    $item->icon->src = $arItem['icon'];

    return $item;
});
return [
    "title" => "Связатся с нами в мессенджерах",
    "links" => $links
];
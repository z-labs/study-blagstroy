<?php

use ZLabs\BxMustache\Contacts\Item;
use ZLabs\BxMustache\Phone;
use ZLabs\BxMustache\Link;

$phone1 = new Phone();
$phone1->value = "<b>+7 (4162)</b> 528-017";
$phone2 = new Phone();
$phone2->value = "<b>+7 (4162)</b> 528-017";

$email = new Link();
$email->href = "blagstroy@mail.ru";
$email->text = "Написать письмо";

$item = new Item();

$item->name = "Юридический отдел";
$item->address = 'г. Благовещенск, пер. Святителя Иннокентия, 1';
$item->phones = collect([$phone1, $phone2]);
$item->email = $email;
$item->workingDays = ['пн', 'вт', 'ср', "чт", "пт"];
$item->weekendDays = ['сб', "вс"];
$item->workMode = "пн-пт с 8:00-18:00 | перерыв с 12:00-13:00";
$item->map = [
    'dataX' => '50.256846',
    'dataY' => '127.523930',
    'balloonContent' => 'г. Благовещенск, пер. Святителя Иннокентия, 1',
    'iconCaption' => 'Отдел продаж'
];

return $item;
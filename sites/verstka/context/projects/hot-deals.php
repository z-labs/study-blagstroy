<?php

return collect([
    'title' => 'Горячие предложения',
    'categories' => include(__DIR__ . '/../sale-categories.php'),
    'items' => include(__DIR__ . '/../sale.php')
]);

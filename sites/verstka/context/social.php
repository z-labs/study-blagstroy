<?php

use ZLabs\BxMustache\SocialLink;
use ZLabs\BxMustache\Svg;

return collect([
    [
        'text' => 'Instagram',
        'href' => 'https://www.instagram.com/blagoveshchenskstroi/',
        'icon' => '/local/assets/images/temp/social/icon-1.svg'
    ],
    [
        'text' => 'Odnoklassniki',
        'href' => 'https://ok.ru/profile/577221515012',
        'icon' => '/local/assets/images/temp/social/icon-2.svg'
    ],
    [
        'text' => 'ВКонтакте',
        'href' => 'https://vk.com/id411181484',
        'icon' => '/local/assets/images/temp/social/icon-3.svg'
    ],
    [
        'text' => 'Facebook',
        'href' => 'https://www.facebook.com/profile.php?id=100015526065617',
        'icon' => '/local/assets/images/temp/social/icon-4.svg'
    ],
    [
        'text' => 'Youtube',
        'href' => 'https://www.facebook.com/profile.php?id=100015526065617',
        'icon' => '/local/assets/images/temp/social/icon-5.svg'
    ]
])->map(function ($arLink) {
    $item = new SocialLink;

    $item->href = $arLink['href'];
    $item->text = $arLink['text'];
    $item->icon = new Svg;
    $item->icon->src = $arLink['icon'];

    return $item;
});

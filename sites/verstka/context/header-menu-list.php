<?php

use ZLabs\BxMustache\Menu\Item;
use ZLabs\BxMustache\Menu;

if (!function_exists('prepareMenu')) {
    function prepareMenu($arItem)
    {
        $item = new Menu\ItemWithChildren;
        $item->level = $arItem['level'];
        $item->text = $arItem['text'];
        $item->href = $arItem['href'];
        $item->active = $arItem['active'];

        if (isset($arItem['params'])) {
            $item->params = collect($arItem['params']);
        }

        if (isset($arItem['menu'])) {
            $item->menu = new Menu\MenuItems($arItem['menu']->map('prepareMenu'));
            $item->menu->level = $arItem['level'] + 1;
        }

        return $item;
    }
}

return (new Menu\MenuItems([
    [
        'href' => '#',
        'text' => 'Проекты',
        'active' => true,
        'level' => 1,
        'menu' => collect([
            [
                'href' => '#',
                'text' => 'Жилые комплексы',
                'active' => false,
                'level' => 2
            ],
            [
                'href' => '#',
                'text' => 'Коммерческие помещения',
                'active' => false,
                'level' => 2
            ],
            [
                'href' => '#',
                'text' => 'Гаражи и машиноместа',
                'active' => true,
                'level' => 2
            ]
        ])
    ],
    [
        'href' => '#',
        'text' => 'Горячие предложения',
        'active' => true,
        'params' => [
            'isSale' => true
        ],
        'level' => 1
    ],
    [
        'href' => '#',
        'text' => 'Способы покупки',
        'active' => true,
        'level' => 1,
        'menu' => collect([
            [
                'href' => '#',
                'text' => 'Жилые комплексы',
                'active' => false,
                'level' => 2
            ],
            [
                'href' => '#',
                'text' => 'Коммерческие помещения',
                'active' => false,
                'level' => 2
            ],
            [
                'href' => '#',
                'text' => 'Гаражи и машиноместа',
                'active' => true,
                'level' => 2
            ]
        ])
    ],
    [
        'href' => '#',
        'text' => 'Полезная информация',
        'active' => true,
        'level' => 1
    ],
    [
        'href' => '#',
        'text' => 'О компании',
        'active' => true,
        'level' => 1
    ],
    [
        'href' => '#',
        'text' => 'Контакты',
        'active' => true,
        'level' => 1
    ]
]))->map('prepareMenu');

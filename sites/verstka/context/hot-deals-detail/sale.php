<?php

use ZLabs\BxMustache\Image;
use ZLabs\BxMustache\Sale\Item;

return collect([
    [
        'categoryId' => 1,
        'img' => '/local/assets/images/product-card/product-2.jpg',
        'name' => 'Офисное помещение',
        'square' => '249',
        'price' => '49590',
        'fullPrice' => '8130000',
        'address' => 'г. Благовещенск, ул.Ленина 171, корпус 4, строение 2'
    ],
    [
        'categoryId' => 1,
        'img' => '/local/assets/images/product-card/product-1.jpg',
        'name' => 'Офисное помещение',
        'square' => '249',
        'price' => '49590',
        'fullPrice' => '9260000',
        'discount' => '10',
        'address' => 'г. Благовещенск, ул.Ленина 171, корпус 4, строение 2'
    ],
    [
        'categoryId' => 1,
        'img' => '/local/assets/images/product-card/product-2.jpg',
        'name' => 'Офисное помещение',
        'square' => '249',
        'price' => '49590',
        'fullPrice' => '8130000',
        'address' => 'г. Благовещенск, ул.Ленина 171, корпус 4, строение 2'
    ],
    [
        'categoryId' => 1,
        'img' => '/local/assets/images/product-card/product-1.jpg',
        'name' => 'Офисное помещение',
        'square' => '249',
        'price' => '49590',
        'fullPrice' => '8130000',
        'discount' => '10',
        'address' => 'г. Благовещенск, ул.Ленина 171, корпус 4, строение 2'
    ],

])->map(function ($arItem, $key) {
    $item = new Item;

    $item->id = $key + 1;
    $item->strMainId = $key + 1;
    $item->categoryId = $arItem['categoryId'];

    $item->image = new Image;
    $item->image->src = $arItem['img'];
    $item->name = $arItem['name'];
    $item->square = +$arItem['square'];
    $item->price = +$arItem['price'];
    $item->fullPrice = +$arItem['fullPrice'];
    $item->address = $arItem['address'];
    if (isset($arItem['discount'])) {
        $item->discount = +$arItem['discount'];
    }

    return $item;
});

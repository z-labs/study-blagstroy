<?php

use ZLabs\BxMustache\Documents\Document;

return [
    'title' => 'Образцы документов',
    'items' => collect([
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'doc',
            'category' => '1',
            'date_change' => '10 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'doc',
            'category' => '2',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'jpg',
            'category' => '3',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'jpg',
            'category' => '1',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'pdf',
            'category' => '2',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'png',
            'category' => '3',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'pdf',
            'category' => '1',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'xls',
            'category' => '2',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'zip',
            'category' => '3',
            'date_change' => '9 апреля 2020',
            'isFile' => false
        ],
        [
            'href' => '#',
            'name' => 'План контроля в сфере строительства жилых обьектов №1012',
            'mime' => 'embed',
            'category' => '1',
            'date_change' => '9 апреля 2020',
            'isFile' => true
        ]
    ])->map(function ($arDocument, $key) {
        $document = new Document;

        $document->strMainId = $key;
        $document->isFile = $arDocument['isFile'];
        $document->href = $arDocument['href'];
        $document->text = $arDocument['name'];
        $document->extension = $arDocument['mime'];
        $document->categoryId = $arDocument['category'];
        $document->dateAt = "Добавлено {$arDocument['date_change']}";

        return $document;
    })
];

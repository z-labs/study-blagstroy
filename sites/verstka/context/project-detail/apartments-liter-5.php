<?php

use ZLabs\BxMustache\Image;
use ZLabs\BxMustache\Projects\Apartment;
use ZLabs\BxMustache\Projects\ApartmentProperty;

return [
    'rooms' => collect([
        [
            'active' => true,
            'id' => 'all',
            'name' => 'все'
        ],
        [
            'active' => false,
            'id' => 'studio',
            'name' => 'студия'
        ],
        [
            'active' => false,
            'id' => '1',
            'name' => '1 ком.'
        ],
        [
            'active' => false,
            'id' => '2',
            'name' => '2 ком.'
        ],
        [
            'active' => false,
            'id' => '3',
            'name' => '3 ком.'
        ]
    ]),
    'apartments' => collect([
        [
            'id' => '13',
            'room' => 'studio',
            'image-src' => collect(['/local/assets/images/project-layout-rooms/1_kom_42_16_kv_m.jpg']),
            'properties' => collect([
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ],
                [
                    'name' => 'Высота потолков',
                    'values' => collect(['2,7 м']),
                    'filledText' => false
                ],
                [
                    'name' => 'Жилая площадь',
                    'values' => collect(['15,1 м2 ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Сторона света',
                    'values' => collect(['СВ + ЮВ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Стоимость',
                    'values' => collect(['от 2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Стоимость кв.м',
                    'values' => collect(['81 676 ₽']),
                    'filledText' => false
                ]
            ])
        ],
        [
            'id' => '14',
            'room' => '1',
            'image-src' => collect(['/local/assets/images/project-layout-rooms/1_kom_42_16_kv_m.jpg']),
            'properties' => collect([
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ],
                [
                    'name' => 'Высота потолков',
                    'values' => collect(['2,7 м']),
                    'filledText' => false
                ],
                [
                    'name' => 'Жилая площадь',
                    'values' => collect(['15,1 м2 ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Сторона света',
                    'values' => collect(['СВ + ЮВ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Стоимость',
                    'values' => collect(['от 2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Стоимость кв.м',
                    'values' => collect(['81 676 ₽']),
                    'filledText' => false
                ]
            ])
        ],
        [
            'id' => '15',
            'room' => '1',
            'image-src' => collect(['/local/assets/images/project-layout-rooms/1_kom_42_16_kv_m.jpg']),
            'properties' => collect([
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ],
                [
                    'name' => 'Высота потолков',
                    'values' => collect(['2,7 м']),
                    'filledText' => false
                ],
                [
                    'name' => 'Жилая площадь',
                    'values' => collect(['15,1 м2 ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Сторона света',
                    'values' => collect(['СВ + ЮВ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Стоимость',
                    'values' => collect(['от 2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Стоимость кв.м',
                    'values' => collect(['81 676 ₽']),
                    'filledText' => false
                ]
            ])
        ],
        [
            'id' => '16',
            'room' => '1',
            'image-src' => collect(['/local/assets/images/project-layout-rooms/1_kom_42_16_kv_m.jpg']),
            'properties' => collect([
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ],
                [
                    'name' => 'Высота потолков',
                    'values' => collect(['2,7 м']),
                    'filledText' => false
                ],
                [
                    'name' => 'Жилая площадь',
                    'values' => collect(['15,1 м2 ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Сторона света',
                    'values' => collect(['СВ + ЮВ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Стоимость',
                    'values' => collect(['от 2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Стоимость кв.м',
                    'values' => collect(['81 676 ₽']),
                    'filledText' => false
                ]
            ])
        ],
        [
            'id' => '17',
            'room' => '1',
            'image-src' => collect(['/local/assets/images/project-layout-rooms/1_kom_42_16_kv_m.jpg']),
            'properties' => collect([
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ],
                [
                    'name' => 'Высота потолков',
                    'values' => collect(['2,7 м']),
                    'filledText' => false
                ],
                [
                    'name' => 'Жилая площадь',
                    'values' => collect(['15,1 м2 ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Сторона света',
                    'values' => collect(['СВ + ЮВ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Стоимость',
                    'values' => collect(['от 2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Стоимость кв.м',
                    'values' => collect(['81 676 ₽']),
                    'filledText' => false
                ]
            ])
        ],
        [
            'id' => '18',
            'room' => '1',
            'image-src' => collect(['/local/assets/images/project-layout-rooms/1_kom_42_16_kv_m.jpg']),
            'properties' => collect([
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ],
                [
                    'name' => 'Высота потолков',
                    'values' => collect(['2,7 м']),
                    'filledText' => false
                ],
                [
                    'name' => 'Жилая площадь',
                    'values' => collect(['15,1 м2 ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Сторона света',
                    'values' => collect(['СВ + ЮВ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Стоимость',
                    'values' => collect(['от 2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Стоимость кв.м',
                    'values' => collect(['81 676 ₽']),
                    'filledText' => false
                ]
            ])
        ],
        [
            'id' => '19',
            'room' => '2',
            'image-src' => collect(['/local/assets/images/project-layout-rooms/1_kom_42_16_kv_m.jpg']),
            'properties' => collect([
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ],
                [
                    'name' => 'Высота потолков',
                    'values' => collect(['2,7 м']),
                    'filledText' => false
                ],
                [
                    'name' => 'Жилая площадь',
                    'values' => collect(['15,1 м2 ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Сторона света',
                    'values' => collect(['СВ + ЮВ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Стоимость',
                    'values' => collect(['от 2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Стоимость кв.м',
                    'values' => collect(['81 676 ₽']),
                    'filledText' => false
                ]
            ])
        ],
        [
            'id' => '20',
            'room' => '3',
            'image-src' => collect(['/local/assets/images/project-layout-rooms/1_kom_42_16_kv_m.jpg']),
            'properties' => collect([
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ],
                [
                    'name' => 'Высота потолков',
                    'values' => collect(['2,7 м']),
                    'filledText' => false
                ],
                [
                    'name' => 'Жилая площадь',
                    'values' => collect(['15,1 м2 ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Сторона света',
                    'values' => collect(['СВ + ЮВ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Стоимость',
                    'values' => collect(['от 2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Стоимость кв.м',
                    'values' => collect(['81 676 ₽']),
                    'filledText' => false
                ]
            ])
        ],
        [
            'id' => '21',
            'room' => '3',
            'image-src' => collect(['/local/assets/images/project-layout-rooms/1_kom_42_16_kv_m.jpg']),
            'properties' => collect([
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ],
                [
                    'name' => 'Высота потолков',
                    'values' => collect(['2,7 м']),
                    'filledText' => false
                ],
                [
                    'name' => 'Жилая площадь',
                    'values' => collect(['15,1 м2 ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Сторона света',
                    'values' => collect(['СВ + ЮВ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Стоимость',
                    'values' => collect(['от 2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Стоимость кв.м',
                    'values' => collect(['81 676 ₽']),
                    'filledText' => false
                ]
            ])
        ],
        [
            'id' => '22',
            'room' => '3',
            'image-src' => collect(['/local/assets/images/project-layout-rooms/1_kom_42_16_kv_m.jpg.png']),
            'properties' => collect([
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ],
                [
                    'name' => 'Высота потолков',
                    'values' => collect(['2,7 м']),
                    'filledText' => false
                ],
                [
                    'name' => 'Жилая площадь',
                    'values' => collect(['15,1 м2 ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Сторона света',
                    'values' => collect(['СВ + ЮВ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Стоимость',
                    'values' => collect(['от 2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Стоимость кв.м',
                    'values' => collect(['81 676 ₽']),
                    'filledText' => false
                ]
            ])
        ],
        [
            'id' => '23',
            'room' => '3',
            'image-src' => collect(['/local/assets/images/project-layout-rooms/1_kom_42_16_kv_m.jpg']),
            'properties' => collect([
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ],
                [
                    'name' => 'Высота потолков',
                    'values' => collect(['2,7 м']),
                    'filledText' => false
                ],
                [
                    'name' => 'Жилая площадь',
                    'values' => collect(['15,1 м2 ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Сторона света',
                    'values' => collect(['СВ + ЮВ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Стоимость',
                    'values' => collect(['от 2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Стоимость кв.м',
                    'values' => collect(['81 676 ₽']),
                    'filledText' => false
                ]
            ])
        ],
        [
            'id' => '24',
            'room' => 'studio',
            'image-src' => collect(['/local/assets/images/project-layout-rooms/1_kom_42_16_kv_m.jpg']),
            'properties' => collect([
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ],
                [
                    'name' => 'Высота потолков',
                    'values' => collect(['2,7 м']),
                    'filledText' => false
                ],
                [
                    'name' => 'Жилая площадь',
                    'values' => collect(['15,1 м2 ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Сторона света',
                    'values' => collect(['СВ + ЮВ']),
                    'filledText' => false
                ],
                [
                    'name' => 'Стоимость',
                    'values' => collect(['от 2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Стоимость кв.м',
                    'values' => collect(['81 676 ₽']),
                    'filledText' => false
                ]
            ])
        ],
    ])->map(function ($arApartment, $key) {
        $apartment = new Apartment;

        $apartment->key = $key;
        $apartment->id = $arApartment['id'];
        $apartment->strMainId = $arApartment['id'];

        $apartment->room = $arApartment['room'];

        $apartment->images = $arApartment['image-src']->map(function ($src, $key) {
            $image = new Image;
            $image->id = $key;
            $image->realSrc = $src;
            $image->src = $src;
            $image->alt = 'альт';

            return $image;
        });
        $apartment->properties = $arApartment['properties']->map(function ($arProperty) {
            $property = new ApartmentProperty;
            $property->name = $arProperty['name'];
            $property->values = $arProperty['values'];
            $property->filledText = $arProperty['filledText'];

            return $property;
        });

        $count = $key + 1;
        $apartment->availableText = "Доступно {$count} квартир этого типа (2-5 этаж)";

        return $apartment;
    })
];

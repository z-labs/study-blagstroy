<?php

use Illuminate\Support\Collection;
use ZLabs\BxMustache\Projects\ObjectItem;
use ZLabs\BxMustache\Svg;

return collect([
    [
        'name' => 'Литер 4',
        'short-name' => 'Литер',
        'letter' => 4,
        'deadline' => 'II квартал 2020 г.',
        'icon' => '/local/assets/images/temp/objects/object-1.svg'
    ],
    [
        'name' => 'Литер 5',
        'short-name' => 'Литер',
        'letter' => 5,
        'deadline' => 'III квартал 2020 г.',
        'icon' => '/local/assets/images/temp/objects/object-2.svg'
    ],
    [
        'name' => 'Литер 6',
        'short-name' => 'Литер',
        'letter' => 6,
        'deadline' => 'IV квартал 2020 г.',
        'icon' => '/local/assets/images/temp/objects/object-1.svg'
    ]
])->map(function ($arObject, $key) {
    $object = new ObjectItem;

    $object->icon = new Svg;
    $object->icon->src = $arObject['icon'];

    $object->id = $arObject['letter'];
    $object->strMainId = $arObject['letter'];
    $object->name = $arObject['name'];
    $object->shortName = $arObject['short-name'];
    $object->letter = $arObject['letter'];
    $object->deadline = $arObject['deadline'];
    $object->active = $key === 0;

    return $object;
})
    ->chunk(2)
    ->map(function (Collection $chunk) {
        return [
            'hasOneItem' => $chunk->count() === 1,
            'objects' => $chunk
        ];
    });

<?php

use ZLabs\BxMustache\AdaptiveImage;
use ZLabs\BxMustache\Carousel;
use ZLabs\BxMustache\Image;
use ZLabs\BxMustache\Projects\Detail;
use ZLabs\BxMustache\Projects\DetailTrigger;
use ZLabs\BxMustache\Projects\RoomTrigger;
use ZLabs\BxMustache\Property\Property;
use ZLabs\BxMustache\Svg;

$detail = new Detail;
$detail->detailPicture = new Image;
$detail->detailPicture->lgSrc = '/local/assets/images/project-detail-first-screen/background.jpg';
$detail->detailPicture->src = '/local/assets/images/project-detail-first-screen/background.jpg';
$detail->firstScreenCarousel = collect([
    [
        'src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg'
    ],
    [
        'src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg'
    ],
    [
        'src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg'
    ],
    [
        'src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg'
    ]
])->map(function ($arItem) {
    $image = new Image;
    $image->src = $arItem['src'];

    return $image;
});

$detail->name = 'Многоквартирный жилой дом в районе СХПК "Тепличный"';
$detail->address = 'г. Благовещенск, ул. Тепличная, 1';

$detail->triggers = collect([
    [
        'name' => 'Литер 4',
        'text' => 'Дом сдан'
    ],
    [
        'name' => 'Литер 5',
        'text' => 'Срок сдачи IV кв. 2021'
    ],
    [
        'name' => 'Стоимость',
        'text' => 'от 79 000 ₽ | м2',
        'filled' => true
    ],
    [
        'name' => 'Дополнительно',
        'text' => 'Парковка на 60 мест'
    ]
])->map(function ($arItem) {
    $trigger = new DetailTrigger;
    $trigger->name = $arItem['name'];
    $trigger->text = $arItem['text'];

    if (isset($arItem['filled'])) {
        $trigger->filledText = $arItem['filled'];
    }

    return $trigger;
});

$detail->mainCharacteristics = collect([
    [
        'name' => 'Тип дома',
        'text' => 'кирпичный'
    ],
    [
        'name' => 'Срок сдачи',
        'text' => 'IV кв. 2020'
    ],
    [
        'name' => 'Этажность',
        'text' => '12'
    ],
    [
        'name' => 'Парковка',
        'text' => '60 мест'
    ],
    [
        'name' => 'Стоимость за М<sup>2</sup>',
        'text' => 'от 100 000 ₽'
    ]
])->map(function ($arProperty) {
    $property = new Property;
    $property->name = $arProperty['name'];
    $property->values = collect([$arProperty['text']]);

    return $property;
});

$detail->characteristics = collect([
    [
        'name' => 'Тип дома',
        'text' => 'кирпичный'
    ],
    [
        'name' => 'Срок сдачи',
        'text' => 'IV кв. 2020'
    ],
    [
        'name' => 'Этажность',
        'text' => '12'
    ],
    [
        'name' => 'Парковка',
        'text' => '60 мест'
    ],
    [
        'name' => 'Стоимость за М<sup>2</sup>',
        'text' => 'от 100 000 ₽'
    ],
    [
        'name' => 'Тип дома',
        'text' => 'кирпичный'
    ],
    [
        'name' => 'Срок сдачи',
        'text' => 'IV кв. 2020'
    ],
    [
        'name' => 'Этажность',
        'text' => '12'
    ],
    [
        'name' => 'Парковка',
        'text' => '60 мест'
    ],
    [
        'name' => 'Стоимость за М<sup>2</sup>',
        'text' => 'от 100 000 ₽'
    ]
])->map(function ($arProperty) {
    $property = new Property;
    $property->name = $arProperty['name'];
    $property->values = collect([$arProperty['text']]);

    return $property;
});

$detail->roomTriggers = collect([
    [
        'icon' => '/local/assets/images/temp/finishing-facilities/door.svg',
        'name' => 'Установлена железная входная дверь 0.3 мм'
    ],
    [
        'icon' => '/local/assets/images/temp/finishing-facilities/mounting.svg',
        'name' => 'Выполнена монтаж сухой стяжка пола'
    ],
    [
        'icon' => '/local/assets/images/temp/finishing-facilities/radiators.svg',
        'name' => 'Установлены алюминевые радиаторы отопления'
    ],
    [
        'icon' => '/local/assets/images/temp/finishing-facilities/walls.svg',
        'name' => 'Выровненные и оштукатуренные стены'
    ],
    [
        'icon' => '/local/assets/images/temp/finishing-facilities/electrician.svg',
        'name' => 'Разведена электрика'
    ],
    [
        'icon' => '/local/assets/images/temp/finishing-facilities/windows.svg',
        'name' => 'Металлопластиковые окна с гарантией 5 лет'
    ]
])->map(function ($arRoomTrigger) {
    $roomTrigger = new RoomTrigger;
    $roomTrigger->icon = new Svg;
    $roomTrigger->icon->src = $arRoomTrigger['icon'];
    $roomTrigger->name = $arRoomTrigger['name'];

    return $roomTrigger;
});

$detail->carousels = collect([
    new Carousel([
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ]
    ]),
    new Carousel([
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ],
        [
            'real-src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'src' => '/local/assets/images/project-detail-slider/img-1.jpg',
            'md-src' => '/local/assets/images/project-detail-slider/img-1-mobile.jpg',
            'alt' => 'Многоквартирный жилой дом, СХПК Тепличный',
            'title' => 'Многоквартирный жилой дом, СХПК Тепличный'
        ]
    ])
])->map(function (Carousel $carousel, $key) {
    $carousel = $carousel->map(function ($arImage) {
        $image = new AdaptiveImage;

        $image->realSrc = $arImage['real-src'];
        $image->src = $arImage['src'];
        $image->mdSrc = $arImage['md-src'];
        $image->alt = $arImage['alt'];
        $image->title = $arImage['title'];

        return $image;
    });

    $carousel->key = $key + 1;

    return $carousel;
});

return $detail;

<?php

use ZLabs\BxMustache\Projects\Apartment;
use ZLabs\BxMustache\Projects\ApartmentProperty;

return [
    'mainCharacteristics' => (include($_SERVER['DOCUMENT_ROOT'] . '/context/project-detail/detail.php'))
        ->mainCharacteristics,
    'apartments' => collect([
        [
            'name' => '1-комнатная квартира',
            'properties' => collect([
                [
                    'name' => 'Стоимость',
                    'values' => collect(['2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ]
            ])
        ],
        [
            'name' => '2-комнатная квартира',
            'properties' => collect([
                [
                    'name' => 'Стоимость',
                    'values' => collect(['2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ]
            ])
        ],
        [
            'name' => '3-комнатная квартира',
            'properties' => collect([
                [
                    'name' => 'Стоимость',
                    'values' => collect(['2 130 000 ₽']),
                    'filledText' => true
                ],
                [
                    'name' => 'Общая площадь',
                    'values' => collect(['26,4 м<sup>2</sup>']),
                    'filledText' => false
                ]
            ])
        ]
    ])->map(function ($arApartment) {
        $apartment = new Apartment;

        $apartment->name = $arApartment['name'];
        $apartment->properties = $arApartment['properties']->map(function ($arProperty) {
            $property = new ApartmentProperty;
            $property->name = $arProperty['name'];
            $property->values = $arProperty['values'];
            $property->filledText = $arProperty['filledText'];

            return $property;
        });

        return $apartment;
    }),
    'isArchive' => false
];

<?php

use ZLabs\BxMustache\Link;

return collect([
    [
        'href' => '#project-about',
        'text' => 'О проекте',
    ],
    [
        'href' => '#project-layout',
        'text' => 'Планировки',
    ],
    [
        'href' => '#map',
        'text' => 'Инфраструктура',
    ],
    [
        'href' => '#project-buy-ways',
        'text' => 'Способ покупки',
    ],
    [
        'href' => '#project-building',
        'text' => 'Ход строительства',
    ],
    [
        'href' => '#project-documents',
        'text' => 'Документация',
    ]
])->map(function ($arItem) {
    $item = new Link;
    $item->href = $arItem['href'];
    $item->text = $arItem['text'];

    return $item;
});

<?php

return [
    'ajax_component_id' => 'article-form',
    'action' => '',
    'title' => '<span class="tablet-hide">Появились<br>Вопросы?</span><span class="mobile-hide tablet-show">Необходима консультация?</span>',
    'sub_title' => 'Оставьте свои контакты, и мы обсудим все интересующие вас вопросы',
    'submit_text' => 'Получить консультацию',
    'controls' => [
        'name' => [
            'placeholder' => 'Ваше имя',
            'code' => 'name',
            'required' => true,
            'requiredCssClass' => ' feedback-form__control_required',
            'additional_css_class' => ' feedback-form__name',
            'value' => ''
        ],
        'phone' => [
            'placeholder' => 'Телефон',
            'maskCssClass' => ' feedback-form__control_valid_phone',
            'code' => 'phone',
            'required' => true,
            'requiredCssClass' => ' feedback-form__control_required',
            'typeInput' => 'tel',
            'value' => '',
        ],
    ],
    'user_consent' => 'Отправляя заявку, Вы соглашаетесь на обработку <a href="#" title="Согласие на обработку персональных данных" target="_blank">персональных данных</a>',
];
<?php

use ZLabs\BxMustache\AdaptiveImage;
use ZLabs\BxMustache\Link;
use ZLabs\BxMustache\Projects\Floor;

return [
    'needShowDropdown' => true,// ключ, который отвечает за показ выпадающего списка
    'items' => collect([
        [
            'id' => 1,
            'name' => '1 этаж',
            'image-src' => '/local/assets/images/project-layout-content/floor-1.png',
            'md-image-src' => 'local/assets/images/project-layout-content/floor-1-mobile.png',
            'is-vertical' => false
        ],
        [
            'id' => 2,
            'name' => '2-5 этаж',
            'image-src' => '/local/assets/images/project-layout-content/floor-3.png',
            'md-image-src' => 'local/assets/images/project-layout-content/floor-3.png',
            'is-vertical' => true
        ],
        [
            'id' => 3,
            'name' => '6 этаж',
            'image-src' => '/local/assets/images/project-layout-content/floor-3.png',
            'md-image-src' => 'local/assets/images/project-layout-content/floor-3.png',
            'is-vertical' => true
        ]
    ])->map(function ($arFloor, $key) {
        $floor = new Floor;

        $floor->id = $arFloor['id'];
        $floor->strMainId = $arFloor['id'];
        $floor->name = $arFloor['name'];
        $floor->image = new AdaptiveImage;
        $floor->image->realSrc = $arFloor['image-src'];
        $floor->image->src = $arFloor['image-src'];
        $floor->image->mdSrc = $arFloor['md-image-src'];
        $floor->active = $key === 0;
        $floor->isVertical = $arFloor['is-vertical'];

        $floor->printLink = new Link;
        $floor->printLink->href = 'project-layout-print.php';
        $floor->printLink->targetBlank = true;
        $floor->printLink->text = 'Напечатать';

        return $floor;
    })
];

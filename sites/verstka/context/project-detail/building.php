<?php

use ZLabs\BxMustache\Image;
use ZLabs\BxMustache\Projects\BuildProgress;
use ZLabs\BxMustache\Video\Link;

return [
    'title' => 'Ход строительства',
    'needShowQuartersAsDropdown' => false,
    'quarters' => collect([
        [
            'name' => 'I квартал 2020',
            'isActive' => true
        ],
        [
            'name' => 'II квартал 2020'
        ]
    ]),
    'items' => collect([
        [
            'quarter' => 'I квартал 2020',
            'date' => '14 Марта',
            'images' => collect([
                [
                    'realSrc' => '/local/assets/images/building/img1.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ],
                [
                    'realSrc' => '/local/assets/images/building/img3.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ],
                [
                    'realSrc' => '/local/assets/images/building/img2.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ],
                [
                    'realSrc' => '/local/assets/images/building/img3.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ],
                [
                    'realSrc' => '/local/assets/images/building/img2.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ]
            ]),
            'videos' => collect([])
        ],
        [
            'quarter' => 'I квартал 2020',
            'date' => '14 Марта',
            'images' => collect([]),
            'videos' => collect([
                [
                    'href' => 'https://www.youtube.com/watch?v=3CeYsZdvR64',
                    'preview' => [
                        'src' => '/local/assets/images/building/img2.jpg'
                    ],
                    'time' => '3:20'
                ]
            ])
        ],
        [
            'quarter' => 'I квартал 2020',
            'date' => '14 Марта',
            'images' => collect([
                [
                    'realSrc' => '/local/assets/images/building/img1.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ],
                [
                    'realSrc' => '/local/assets/images/building/img3.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ]
            ]),
            'videos' => collect([
                [
                    'href' => 'https://www.youtube.com/watch?v=3CeYsZdvR64',
                    'preview' => [
                        'src' => '/local/assets/images/building/img2.jpg'
                    ],
                    'time' => '3:20'
                ]
            ])
        ],
        [
            'quarter' => 'II квартал 2020',
            'date' => '14 Марта',
            'images' => collect([
                [
                    'realSrc' => '/local/assets/images/building/img1.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ],
                [
                    'realSrc' => '/local/assets/images/building/img3.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ],
                [
                    'realSrc' => '/local/assets/images/building/img2.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ],
                [
                    'realSrc' => '/local/assets/images/building/img3.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ],
                [
                    'realSrc' => '/local/assets/images/building/img2.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ]
            ]),
            'videos' => collect([])
        ],
        [
            'quarter' => 'I квартал 2020',
            'date' => '14 Марта',
            'images' => collect([
                [
                    'realSrc' => '/local/assets/images/building/img1.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ],
                [
                    'realSrc' => '/local/assets/images/building/img3.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ],
                [
                    'realSrc' => '/local/assets/images/building/img2.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ],
                [
                    'realSrc' => '/local/assets/images/building/img3.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ],
                [
                    'realSrc' => '/local/assets/images/building/img2.jpg',
                    'src' => '/local/assets/images/building/img1.jpg'
                ]
            ]),
            'videos' => collect([])
        ]
    ])->map(function ($arBuildProgress, $key) {
        $buildProgress = new BuildProgress;

        $buildProgress->id = $key + 1;
        $buildProgress->strMainId = $key + 1;
        $buildProgress->quarter = $arBuildProgress['quarter'];
        $buildProgress->date = $arBuildProgress['date'];
        $buildProgress->images = $arBuildProgress['images']->map(function ($arImage) {
            $image = new Image;

            $image->realSrc = $arImage['realSrc'];
            $image->src = $arImage['src'];

            return $image;
        });
        $buildProgress->videos = $arBuildProgress['videos']->map(function ($arVideo) {
            $video = new Link;

            $video->href = $arVideo['href'];
            $video->preview = new Image;
            $video->preview->src = $arVideo['preview']['src'];
            $video->time = $arVideo['time'];

            return $video;
        });

        return $buildProgress;
    })
];

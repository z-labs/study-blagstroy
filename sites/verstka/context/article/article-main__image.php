<?php

use ZLabs\BxMustache\AdaptiveImage;

$image = new AdaptiveImage();
$image->src = '/local/assets/images/article/article-img-dt.jpg';
$image->mdSrc = '/local/assets/images/article/article-img-m.jpg';
$image->lgSrc = '/local/assets/images/article/article-img-t.jpg';

return $image;
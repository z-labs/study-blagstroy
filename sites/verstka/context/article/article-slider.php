<?php

use ZLabs\BxMustache\AdaptiveImage;

$images = collect([
    [
        'src' => '/local/assets/images/article/slider-photo-dt.jpg',
        'srcMd' => '/local/assets/images/article/slider-photo-m.jpg',
        'srcLg' => '/local/assets/images/article/slider-photo-t.jpg'
    ],
    [
        'src' => '/local/assets/images/article/article-img-dt.jpg',
        'srcMd' => '/local/assets/images/article/article-img-m.jpg',
        'srcLg' => '/local/assets/images/article/article-img-t.jpg'
    ],
    [
        'src' => '/local/assets/images/article/slider-photo-dt.jpg',
        'srcMd' => '/local/assets/images/article/slider-photo-m.jpg',
        'srcLg' => '/local/assets/images/article/slider-photo-t.jpg'
    ],
])->map(function ($arImage){
    $image = new AdaptiveImage();
    $image->src = $arImage['src'];
    $image->mdSrc = $arImage['srcMd'];
    $image->lgSrc = $arImage['srcLg'];

    return $image;
});

return $images;
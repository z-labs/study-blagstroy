<?php

use ZLabs\BxMustache\Image;
use ZLabs\BxMustache\Link;

$image = new Image();
$image->src = '/local/assets/images/article/video-img.jpg';

$link = new Link();
$link->href = 'https://www.youtube.com/watch?v=379z_mfCprM';
$link->text = 'Прогнозы: что ждет рынок недвижимости в 2020 году';

return [
    'title' => 'Прогнозы: что ждет рынок недвижимости в 2020 году',
    'image' => $image,
    'link' => $link
];
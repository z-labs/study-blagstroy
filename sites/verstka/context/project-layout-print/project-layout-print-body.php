<?php

return [
    'projectName' => 'Многоквартирный жилой дом, СХПК Тепличный Литер 4, Срок сдачи IV квартал 2020',
    'name' => 'Типовая планировка: 1 этаж',
    'address' => 'г. Благовещенск, ул. Тепличная, 1',
    'qr' => [
        'src' => '/local/assets/images/project-layout-print/qr-code 1.png'
    ],
    'img' => [
        'src' => '/local/assets/images/project-layout-print/image.png'
    ]
];

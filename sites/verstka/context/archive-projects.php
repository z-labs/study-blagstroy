<?php

use ZLabs\BxMustache\AdaptiveImage;
use ZLabs\BxMustache\Link;
use ZLabs\BxMustache\Projects\Item;

return collect([
    [
        'image' => [
            'src' => '/local/assets/images/list-projects/img1.jpg'
        ],
        'name' => 'Многоквартирный дом, Литер 5',
        'address' => 'г. Благовещенск, ул. Тепличная ул, 1',
        'deadline' => 'III квартал 2020 г.'
    ],
    [
        'image' => [
            'src' => '/local/assets/images/list-projects/img2.jpg'
        ],
        'name' => 'Многоквартирный дом, Литер 5',
        'address' => 'Благовещенск, ул. Амурская, 270/1',
        'deadline' => 'III квартал 2020 г.'
    ],
    [
        'image' => [
            'src' => '/local/assets/images/list-projects/img3.jpg'
        ],
        'name' => 'Многоквартирный дом, Литер 5',
        'address' => 'Благовещенск, ул. Амурская, 270/1',
        'deadline' => 'III квартал 2020 г.'
    ],
    [
        'image' => [
            'src' => '/local/assets/images/list-projects/img3.jpg'
        ],
        'name' => 'Многоквартирный дом, Литер 5',
        'address' => 'Благовещенск, ул. Амурская, 270/1',
        'deadline' => 'III квартал 2020 г.'
    ]
])->map(function ($arItem) {
    $item = new Item;

    $item->image = new AdaptiveImage;
    $item->image->src = $arItem['image']['src'];

    $item->detailPageLink = new Link;
    $item->detailPageLink->href = '#';
    $item->detailPageLink->text = $arItem['name'];

    if (isset($arItem['area'])) {
        $item->area = $arItem['area'];
    }
    if (isset($arItem['fullPrice'])) {
        $item->fullPrice = $arItem['fullPrice'];
    }
    if (isset($arItem['price'])) {
        $item->price = $arItem['price'];
    }
    if (isset($arItem['address'])) {
        $item->address = $arItem['address'];
    }
    if (isset($arItem['deadline'])) {
        $item->deadline = $arItem['deadline'];
    }

    return $item;
});

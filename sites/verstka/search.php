<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/auth.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/../../vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/constants.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Благовещенскстрой: главная страница</title>
    <link rel="stylesheet" href="/local/assets/local/common/common.css">
    <link rel="stylesheet" href="/local/assets/local/search/search.css">
</head>
<body class="page">
<?php require_once 'header.php'; ?>
<div class="page-content">
    <div class="search">
        <div class="container search__container">
            <div class="search__svg1">
                <?php
                echo $mustache->render('background-circle-dashed');
                ?>
            </div>
            <div class="search__svg2">
                <?php
                echo $mustache->render('background-circle-dashed');
                ?>
            </div>
            <div class="search__svg3">
                <?php
                echo $mustache->render('background-circle-dashed');
                ?>
            </div>
            <?php
            echo $mustache->render('breadcrumbs', include(CONTEXT_DIR . '/article/breadcrumbs.php'));
            ?>
            <h1 class="page__title">
                Поиск по сайту
            </h1>
            <?php
            echo $mustache->render(
                'search-form',
                include(CONTEXT_DIR . '/search/search-form.php')
            );
            echo $mustache->render(
                'search-results',
                include(CONTEXT_DIR . '/search/search-results.php')
            );
            echo $mustache->render('search-no-results');
            ?>
        </div>
    </div>
</div>

<?php require_once 'footer.php'; ?>

<script id="popup-calendar-template" type="x-tmpl-mustache">
  <?php include($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/popup-calendar.mustache') ?>

</script>

<script type="text/javascript" src="https://static.yandex.net/browser-updater/v1/script.js" defer></script>
<script src="/local/assets/local/common/common.js" defer></script>
<script src="/local/assets/vendor/form/jquery.form.min.js" defer></script>
<script src="/local/assets/local/feedback-form/feedback-form.js" defer></script>
<script src="/local/assets/local/search/search.js" defer></script>
</body>
</html>

<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/auth.php';
use ZLabs\BxMustache\Projects\Detail;

require_once $_SERVER['DOCUMENT_ROOT'] . '/../../vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/constants.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);

/** @var Detail $detail */
$detail = include(CONTEXT_DIR . '/hot-deals-detail/detail.php');
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Благовещенскстрой: Горячие предложения. Детальная</title>
    <link rel="stylesheet" href="/local/assets/local/common/common.css">
    <link rel="stylesheet" href="/local/assets/local/feedback-form/feedback-form.css">
    <link rel="stylesheet" href="/local/assets/local/hot-deals-detail/hot-deals-detail.css">
</head>
<body class="page">
<?php require_once 'header.php'; ?>

<div class="page-content">
    <div class="project-detail hot-deals-detail">
        <div class="container">
            <?php
            echo $mustache->render('breadcrumbs', include(CONTEXT_DIR . '/project-detail/breadcrumbs.php'));
            ?>
            <h1 class="page__title">
                Офисное помещение
            </h1>
        </div>
        <?php
        echo $mustache->render('project-detail-first-screen-slider', $detail);
        ?>
        <section class="project-detail__about-section page-screen"
                 id="project-about">
            <div class="container project-detail-about-container">
                <div class="article-main">
                    <?php
                    echo $mustache->render('project-detail-about-slider', $detail->carousels->get(0));
                    ?>
                    <h3>Описание</h3>
                    <p>
                        9-этажный кирпичный моноговквартирный жилой дом с встроеными помещениями.
                        Внешнаяя отделка дома из силикатногго и керамического кирпича с расшивкой швов.
                        Внутренние перегородки из силикатного кирпича. На первом этаже предусмотрены коммерческие
                        помощения. В доме 4 подъезда, 210 квартир с черновой отделкой.
                    </p>
                    <blockquote>Проектная декларация размещена в единой информационной системе жилищного строительства
                        <a href="#" title="Перейти на сайт" target="_blank">наш.дом.рф</a>
                    </blockquote>
                    <?php
                    echo $mustache->render('characteristics-table', $detail);
                    ?>
                    <h3>Отделка помещений</h3>
                    <?php
                    echo $mustache->render('project-detail-futures', $detail);
                    ?>
                    <?php
                    echo $mustache->render('index-tour-form', include(CONTEXT_DIR . 'index/tour-form-normal.php'));
                    ?>
                    <h2>Дворовая территория</h2>
                    <?php
                    echo $mustache->render('project-detail-about-slider', $detail->carousels->get(1));
                    ?>
                    <h3>Благоустроенная дворовая территория</h3>
                    <p>Предусмотрено благоустройство прилегающей территории в виде размещения дворовых площадок:
                        детской, для отдыха взрослых.</p>
                    <p>Вокруг территории детской и спортивной площадок предусмотрено леерное ограждение.</p>
                    <p>В рамках благоустройства территории предусмотрено озеленение газонов путем посева многолетних
                        трав и деревьев.</p>
                </div>
                <?php
                echo $mustache->render(
                    'hot-deals-detail-about-side',
                    include(CONTEXT_DIR . '/hot-deals-detail/sidebar.php')
                );
                ?>
            </div>
        </section>
        <div class="container">
            <?php
            echo $mustache->render(
                'hot-deals',
                include(CONTEXT_DIR . '/hot-deals-detail/hot-deals.php')
            );
            ?>
        </div>
        <?php
        echo $mustache->render('consultation-form_gray', include(CONTEXT_DIR . '/article/article-form.php'));
        ?>
    </div>
</div>

<?php require_once 'footer.php'; ?>
<script>
    if (!window.initFeedback) {
        window.initFeedback = [];
    }
    window.initFeedback.push(function () {
        $('#article-form').feedbackForm();
    });
    window.initFeedback.push(function () {
        $('#index-tour-form').feedbackForm();
    });
</script>

<script type="text/javascript" src="https://static.yandex.net/browser-updater/v1/script.js" defer></script>
<script src="/local/assets/local/common/common.js" defer></script>
<script src="/local/assets/vendor/form/jquery.form.min.js" defer></script>
<script src="/local/assets/local/feedback-form/feedback-form.js" defer></script>
<script src="/local/assets/local/hot-deals-detail/hot-deals-detail.js" defer></script>
</body>
</html>

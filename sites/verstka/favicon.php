<link rel="apple-touch-icon-precomposed" sizes="57x57" href="apple-touch-icon.png"/>
<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32"/>
<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16"/>
<link rel="shortcut icon" href="favicon.ico">
<meta name="application-name" content="Благовещенскстрой"/>
<meta name="msapplication-TileColor" content="#FFFFFF"/>
<meta name="msapplication-TileImage" content="mstile-144x144.png"/>
<meta name="msapplication-square150x150logo" content="mstile-150x150.png"/>

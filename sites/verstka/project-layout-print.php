<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/auth.php';
use ZLabs\BxMustache\Projects\Detail;

require_once $_SERVER['DOCUMENT_ROOT'] . '/../../vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/constants.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Благовещенскстрой: Проекты - детальная</title>
    <link rel="stylesheet" href="/local/assets/local/common/common.css">
    <link rel="stylesheet" href="/local/assets/local/project-layout-print/project-layout-print.css">
</head>
<body class="page">

<div class="print-page-container">
    <div class="print-container">
        <?php
        echo $mustache->render('print-header');
        ?>
        <?php
        echo $mustache->render(
            'project-layout-print-body',
            include(CONTEXT_DIR . '/project-layout-print/project-layout-print-body.php')
        );
        ?>
    </div>
</div>

<script type="text/javascript" src="https://static.yandex.net/browser-updater/v1/script.js" defer></script>
<script src="/local/assets/local/common/common.js" defer></script>
<script src="/local/assets/local/project-layout-print/project-layout-print.js" defer></script>
</body>
</html>

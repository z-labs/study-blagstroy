<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/auth.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/../../vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/constants.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Благовещенскстрой: Как купить</title>
    <link rel="stylesheet" href="/local/assets/local/common/common.css">
    <link rel="stylesheet" href="/local/assets/local/feedback-form/feedback-form.css">
    <link rel="stylesheet" href="/local/assets/local/how-buy-page/how-buy-page.css">
</head>
<body class="page">
<?php require_once 'header.php'; ?>

<div class="page-content">
    <div class="container">
        <?php
        echo $mustache->render('breadcrumbs', include(CONTEXT_DIR . '/hot-deals-page/breadcrumbs.php'));
        ?>
        <h1 class="page__title">
            Способы покупки
        </h1>
    </div>
    <div class="how-buy-page">
        <div class="how-buy-page__pattern-circle-1 svg-pattern">
            <?php
            echo $mustache->render('background-circle-dashed');
            ?>
        </div>
        <div class="how-buy-page__pattern-circle-2 svg-pattern">
            <?php
            echo $mustache->render('background-circle-dashed');
            ?>
        </div>
        <div class="how-buy-page__pattern-dots svg-pattern">
            <?php
            echo $mustache->render('background-dots');
            ?>
        </div>
        <div class="container">
            <?php
            echo $mustache->render(
                'buy-ways-list',
                include(CONTEXT_DIR . 'index/buy-ways.php')
            );
            ?>
        </div>
        <?php
        echo $mustache->render('consultation-form_gray', include(CONTEXT_DIR . '/article/article-form.php'));
        ?>
    </div>
</div>

<?php require_once 'footer.php'; ?>
<script>
    if (!window.initFeedback) {
        window.initFeedback = [];
    }
    window.initFeedback.push(function () {
        $('#article-form').feedbackForm();
    });
</script>

<script type="text/javascript" src="https://static.yandex.net/browser-updater/v1/script.js" defer></script>
<script src="/local/assets/local/common/common.js" defer></script>
<script src="/local/assets/vendor/form/jquery.form.min.js" defer></script>
<script src="/local/assets/local/feedback-form/feedback-form.js" defer></script>
<script src="/local/assets/local/how-buy-page/how-buy-page.js" defer></script>
</body>
</html>

<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/auth.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/../../vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/constants.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Благовещенскстрой: Горячие предложения</title>
    <link rel="stylesheet" href="/local/assets/local/common/common.css">
    <link rel="stylesheet" href="/local/assets/local/feedback-form/feedback-form.css">
    <link rel="stylesheet" href="/local/assets/local/hot-deals-page/hot-deals-page.css">
</head>
<body class="page">
<?php require_once 'header.php'; ?>

<div class="page-content">
    <div class="container">
        <?php
        echo $mustache->render('breadcrumbs', include(CONTEXT_DIR . '/hot-deals-page/breadcrumbs.php'));
        ?>
        <h1 class="page__title">
            Горячие предложения
        </h1>
    </div>
    <div class="hot-deals-page">
        <div class="container">
            <div class="hot-deals-page__products">
            <?php
            echo $mustache->render(
                'index-products',
                include(CONTEXT_DIR . '/index/index-products.php')
            );
            echo $mustache->render('infinite');
            ?>
            </div>
        </div>

        <?php
        echo $mustache->render(
            'other-projects',
            include(CONTEXT_DIR . '/hot-deals-page/other-projects.php')
        );
        ?>

        <?php
        echo $mustache->render('consultation-form_gray', include(CONTEXT_DIR . '/article/article-form.php'));
        ?>
    </div>
    <section class="page-section">
        <div class="container">
            <?php
            echo $mustache->render('index-text', [
                'text' => '<h2>Коммерческая и жилая недвижимость в Благовещенске</h2>
<p>Ключевой набор SEO текста для повышения видимости страниц сайта. Цель - сделать так, чтобы поисковые роботы Яндекса и Google начали находить по запросам пользователей страницы именно вашего сайта. Безусловно, сбор ключевых слов (составление семантики) — первый шаг к этой цели.
Дальше набрасывается условный «скелет» для распределения ключевых слов по разным посадочным страницам. А затем уже пишутся и внедряются статьи/метатеги.</p> 

<p>Рынок строительства всегда был довольно насыщенным и переполнен предложениями, как транспорт в час пик. И в то же время, в этой сфере крутятся большие деньги. Хотите догнать конкурентов и встать впереди колонны – оригинальный продающий текст о строительной компании будет той самой лопатой, которой можно будет грести большие чеки заключённых договоров. Изюминка или фишка в рекламном тексте Вашей компании – это билет в первый ряд высоких продаж.</p> 
<p>Рынок строительства всегда был довольно насыщенным и переполнен предложениями, как транспорт в час пик. И в то же время, в этой сфере крутятся большие деньги. Хотите догнать конкурентов и встать впереди колонны – оригинальный продающий текст о строительной компании будет той самой лопатой, которой можно будет грести большие чеки заключённых договоров. Изюминка или фишка в рекламном тексте Вашей компании – это билет в первый ряд высоких продаж.</p>
']);
            ?>
        </div>
    </section>
</div>

<?php require_once 'footer.php'; ?>
<script>
    if (!window.initFeedback) {
        window.initFeedback = [];
    }
    window.initFeedback.push(function () {
        $('#article-form').feedbackForm();
    });
</script>

<script type="text/javascript" src="https://static.yandex.net/browser-updater/v1/script.js" defer></script>
<script src="/local/assets/local/common/common.js" defer></script>
<script src="/local/assets/vendor/form/jquery.form.min.js" defer></script>
<script src="/local/assets/local/feedback-form/feedback-form.js" defer></script>
<script src="/local/assets/local/hot-deals-page/hot-deals-page.js" defer></script>
</body>
</html>

<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/auth.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/../../vendor/autoload.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Благовещенскстрой</title>
</head>
<body>
<main>
    <h1>Благовещенскстрой</h1>
    <h2>Страницы:</h2>
    <ul>
        <li><a href="index-page.php">Главная страница</a></li>
        <li><a href="blog.php">Список статей</a></li>
        <li><a href="article.php">Статья</a></li>
        <li><a href="project-detail.php">Проекты - детальная</a></li>
        <li><a href="project-detail_one-liter.php">Проекты - детальная (один литер)</a></li>
        <li><a href="projects.php">Список проектов</a></li>
        <li><a href="archive-projects.php">Архив проектов</a></li>
        <li><a href="documentation.php">Документация</a></li>
        <li><a href="purchase.php">Способ покупки</a></li>
        <li><a href="contacts.php">Контакты</a></li>
        <li><a href="simple-project.php">Обычный проект</a></li>
        <li><a href="privacy-policy.php">Политика конфиденциальности</a></li>
        <li><a href="page-404.php">Страница 404</a></li>
        <li><a href="hot-deals-page.php">Горячие предложения. Список</a></li>
        <li><a href="hot-deals-detail.php">Горячие предложения. Детальная</a></li>
        <li><a href="how-buy-page.php">Способы покупки</a></li>
        <li><a href="search.php">Поиск</a></li>
        <li><a href="company.php">О компании</a></li>
    </ul>
</main>
</body>
</html>

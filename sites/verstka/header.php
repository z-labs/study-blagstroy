<?php

$mustache = new Mustache_Engine([
        'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
    ]
);
?>
<header class="header">
    <div class="container header-container">
        <div class="header-container-wrapper header-container-wrapper-main">
            <div class="container inner-header-container">
                <div class="header-section">
                    <a href="index.php" title="На главную" class="header-logo">
                        <img src="/local/assets/images/header/logo.png"
                             alt="Лого"
                             class="header-logo__img">
                    </a>
                    <div class="header-section__part">
                        <div class="header-contacts">
                            <div class="header-contacts__item">
                                <? echo $mustache->render('contact-phone'); ?>
                            </div>
                            <div class="header-contacts__item header-email">
                                <? echo $mustache->render('contact-email'); ?>
                            </div>
                        </div>
                        <a href="#bell-form"
                           class="header__call-button call-button popup-link button button_filled-yellow feedback-form-link">Заказать
                            звонок</a>
                    </div>
                    <button class="fullscreen-menu-close-button">
                        <svg class="fullscreen-menu-close-button__icon" width="20" height="20" viewBox="0 0 20 20"
                             fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15.5529 16.9671L2.58923 4.00347C2.26396 3.6782 2.31675 3.09743 2.70708 2.70711C3.0974 2.31678 3.67817 2.26399 4.00344 2.58926L16.9671 15.5529C17.2923 15.8781 17.2395 16.4589 16.8492 16.8492C16.4589 17.2396 15.8781 17.2924 15.5529 16.9671Z"
                                  fill="#342733"/>
                            <path d="M16.9671 4.00348L4.00347 16.9671C3.6782 17.2924 3.09743 17.2396 2.70711 16.8493C2.31678 16.4589 2.26399 15.8782 2.58926 15.5529L15.5529 2.58927C15.8781 2.264 16.4589 2.3168 16.8492 2.70712C17.2396 3.09744 17.2924 3.67821 16.9671 4.00348Z"
                                  fill="#342733"/>
                        </svg>
                        <span class="fullscreen-menu-close-button__text">Закрыть</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="header-container-wrapper">
            <div class="container inner-header-container">
                <div class="header-section header-section-menu">
                    <div class="header-menu">
                        <button class="header-menu-button">
                            <svg class="header-menu-button__icon" width="28" height="21" viewBox="0 0 28 21" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path class="button-cross-part button-cross-part_1" fill-rule="evenodd"
                                      clip-rule="evenodd"
                                      d="M28 17.8091V20.6662H0V17.8091H28Z" fill="#342733"/>
                                <path class="non-button-cross-part" fill-rule="evenodd" clip-rule="evenodd"
                                      d="M22.4 9.23779V12.0949H0V9.23779H22.4Z" fill="#342733"/>
                                <path class="button-cross-part button-cross-part_2" fill-rule="evenodd"
                                      clip-rule="evenodd"
                                      d="M28 0.666504V3.52365H0V0.666504H28Z" fill="#342733"/>
                            </svg>
                        </button>
                        <div class="header-menu-content">
                            <div class="header-menu-list-wrapper list-items-hover-container">
                                <?php
                                echo $mustache->render(
                                    'header-menu-list',
                                    include(CONTEXT_DIR . '/header-menu-list.php')
                                );
                                ?>
                                <div class="list-items-hover-marker"></div>
                            </div>
                            <div class="header-menu-side-group">
                                <div class="header-menu-search">
                                    <div class="container">
                                        <?php
                                        echo $mustache->render('header-search');
                                        ?>
                                        <button class="header-menu-search-close">
                                            <svg class="header-menu-search-close__icon" width="20" height="20"
                                                 viewBox="0 0 20 20"
                                                 fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M15.5529 16.9671L2.58923 4.00347C2.26396 3.6782 2.31675 3.09743 2.70708 2.70711C3.0974 2.31678 3.67817 2.26399 4.00344 2.58926L16.9671 15.5529C17.2923 15.8781 17.2395 16.4589 16.8492 16.8492C16.4589 17.2396 15.8781 17.2924 15.5529 16.9671Z"
                                                      fill="#342733"/>
                                                <path d="M16.9671 4.00342L4.00347 16.967C3.6782 17.2923 3.09743 17.2395 2.70711 16.8492C2.31678 16.4589 2.26399 15.8781 2.58926 15.5528L15.5529 2.58921C15.8781 2.26394 16.4589 2.31673 16.8492 2.70706C17.2396 3.09738 17.2924 3.67815 16.9671 4.00342Z"
                                                      fill="#342733"/>
                                            </svg>
                                            <span class="header-menu-search-close__text">Свернуть</span>
                                        </button>
                                    </div>
                                </div>
                                <button class="header-menu-search-button header-menu-search-button_desktop">
                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <g clip-path="url(#clip0)">
                                            <path d="M14.6771 12.9094C15.6612 11.5654 16.2499 9.91462 16.2499 8.12498C16.2499 3.64502 12.6049 0 8.12494 0C3.64498 0 0 3.64502 0 8.12498C0 12.6049 3.64502 16.25 8.12498 16.25C9.91462 16.25 11.5656 15.6612 12.9096 14.677L18.2324 19.9999L20 18.2323C20 18.2323 14.6771 12.9094 14.6771 12.9094ZM8.12498 13.75C5.02318 13.75 2.50001 11.2268 2.50001 8.12498C2.50001 5.02318 5.02318 2.50001 8.12498 2.50001C11.2268 2.50001 13.75 5.02318 13.75 8.12498C13.75 11.2268 11.2267 13.75 8.12498 13.75Z"
                                                  fill="black"/>
                                        </g>
                                        <defs>
                                            <clipPath id="clip0">
                                                <rect width="20" height="20" fill="white"/>
                                            </clipPath>
                                        </defs>
                                    </svg>
                                </button>
                                <div class="contact header-compact-contact">
                                    <span class="contact__icon">
                                        <svg width="22" height="22" viewBox="0 0 22 22" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path d="M20.2632 14.4853C18.9061 14.4853 17.5769 14.2734 16.317 13.8577C15.7022 13.6458 15.004 13.809 14.5999 14.2213L12.1023 16.1075C9.23653 14.5779 7.40243 12.745 5.89371 9.90001L7.72895 7.46148C8.19095 6.99948 8.35655 6.32325 8.15855 5.6899C7.73937 4.422 7.52634 3.09158 7.52634 1.73684C7.52634 0.779246 6.74709 0 5.7895 0H1.73684C0.779246 0 0 0.779246 0 1.73684C0 12.9094 9.09064 22 20.2632 22C21.2208 22 22 21.2208 22 20.2632V16.2221C22 15.2645 21.2208 14.4853 20.2632 14.4853Z"
                                                  fill="#E0E0E0"/>
                                            <path d="M20.2632 14.4853C18.9061 14.4853 17.5769 14.2734 16.317 13.8577C15.7022 13.6458 15.004 13.809 14.5999 14.2213L12.1023 16.1075C9.23653 14.5779 7.40243 12.745 5.89371 9.90001L7.72895 7.46148C8.19095 6.99948 8.35655 6.32325 8.15855 5.6899C7.73937 4.422 7.52634 3.09158 7.52634 1.73684C7.52634 0.779246 6.74709 0 5.7895 0H1.73684C0.779246 0 0 0.779246 0 1.73684C0 12.9094 9.09064 22 20.2632 22C21.2208 22 22 21.2208 22 20.2632V16.2221C22 15.2645 21.2208 14.4853 20.2632 14.4853Z"
                                                  fill="#E0E0E0"/>
                                        </svg>
                                    </span>
                                    <div class="contact-text">
                                        <div class="contact-text__value">8 (4162) 528-017</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-submenu-background"></div>
    </div>
    <div class="fullscreen-menu">
        <div class="container">
            <div class="fullscreen-menu__content">
                <?php
                echo $mustache->render('header-search')
                ?>
                <?php
                echo $mustache->render(
                    'fullscreen-submenu',
                    include(CONTEXT_DIR . '/fullscreeen-submenu.php')
                )
                ?>
                <div class="fullscreen-menu__bottom">
                    <div class="fullscreen-menu-actions-buttons-main">
                        <a class="fullscreen-menu-question-btn button button_linear-gray" href="#">Задать вопрос</a>
                        <span class="fullscreen-menu-contact-text">Есть жалобы или предложения?</span>
                        <a class="fullscreen-menu-write-btn button button_linear-gray" href="#">Написать
                            руководителю</a>
                    </div>

                    <div class="fullscreen-menu__part">
                        <div class="fullscreen-menu__phone">
                            <?php
                            echo $mustache->render('contact-phone')
                            ?>
                        </div>
                        <div class="fullscreen-menu-actions-buttons-other">
                            <a class="fullscreen-menu-call-btn button button_linear-gray" href="#">
                                <svg class="button__icon" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M18.4211 13.1684C17.1873 13.1684 15.979 12.9758 14.8337 12.5979C14.2747 12.4053 13.64 12.5537 13.2726 12.9284L11.0021 14.6432C8.39685 13.2527 6.72948 11.5863 5.35792 9.00001L7.02632 6.78316C7.44632 6.36316 7.59686 5.74841 7.41686 5.17264C7.0358 4.02 6.84213 2.81053 6.84213 1.57895C6.84213 0.708405 6.13372 0 5.26318 0H1.57895C0.708405 0 0 0.708405 0 1.57895C0 11.7358 8.26422 20 18.4211 20C19.2916 20 20 19.2916 20 18.4211V14.7474C20 13.8768 19.2916 13.1684 18.4211 13.1684Z" fill="#636F75" fill-opacity="0.6"/>
                                </svg>
                                <span class="button__text">Заказать звонок</span>
                            </a>
                            <a class="fullscreen-menu-whatsapp-btn button button_linear-gray" href="#">
                                <svg class="button__icon" width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M18.7946 3.19714C16.7265 1.13652 13.9761 0.00120849 11.046 0C5.00821 0 0.0943112 4.89025 0.0918825 10.9008C0.091073 12.8221 0.595415 14.6977 1.55404 16.351L0 22L5.80695 20.484C7.407 21.3526 9.20836 21.8104 11.0416 21.8109H11.0461C17.0833 21.8109 21.9977 16.9203 22 10.9095C22.0012 7.99648 20.8629 5.25763 18.7946 3.19714ZM11.046 19.9699H11.0422C9.40858 19.9692 7.80637 19.5323 6.4083 18.7068L6.07599 18.5104L2.63005 19.4101L3.54982 16.0664L3.33327 15.7236C2.42187 14.2809 1.9406 12.6135 1.94141 10.9014C1.9433 5.90565 6.02768 1.84121 11.0497 1.84121C13.4815 1.84202 15.7675 2.78572 17.4864 4.49843C19.2053 6.21113 20.1514 8.48767 20.1506 10.9088C20.1485 15.905 16.0643 19.9699 11.046 19.9699ZM16.04 13.1836C15.7664 13.0472 14.4207 12.3884 14.1697 12.2974C13.9191 12.2065 13.7364 12.1612 13.5541 12.4338C13.3715 12.7064 12.8471 13.32 12.6873 13.5017C12.5276 13.6835 12.3681 13.7064 12.0944 13.5699C11.8206 13.4336 10.9387 13.1459 9.89323 12.2179C9.07964 11.4956 8.53037 10.6036 8.37062 10.331C8.21114 10.0582 8.36927 9.92498 8.4907 9.77512C8.78699 9.40895 9.08369 9.02505 9.1749 8.84337C9.26624 8.66156 9.2205 8.50244 9.15196 8.36615C9.08369 8.22986 8.53631 6.8891 8.30829 6.34353C8.08593 5.8126 7.86048 5.8843 7.69236 5.87598C7.53288 5.86805 7.35033 5.86644 7.16778 5.86644C6.98537 5.86644 6.68881 5.93452 6.43785 6.20737C6.18703 6.48009 5.48003 7.13899 5.48003 8.47975C5.48003 9.82051 6.46079 11.1157 6.5976 11.2976C6.73441 11.4794 8.52767 14.2307 11.2732 15.4105C11.9262 15.6914 12.436 15.8588 12.8336 15.9844C13.4893 16.1917 14.0858 16.1624 14.5575 16.0923C15.0834 16.0141 16.1767 15.4333 16.405 14.7971C16.633 14.1608 16.633 13.6155 16.5645 13.5017C16.4962 13.3881 16.3137 13.32 16.04 13.1836Z" fill="#636F75" fill-opacity="0.6"/>
                                </svg>
                                <span class="button__text">WhatsApp</span>
                            </a>
                            <a class="fullscreen-menu-telegram-btn button button_linear-gray" href="#">
                                <svg class="button__icon" width="22" height="19" viewBox="0 0 22 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8.63262 12.5058L8.2687 17.5314C8.78937 17.5314 9.01488 17.3118 9.2853 17.0481L11.7264 14.7576L16.7847 18.3945C17.7124 18.9021 18.366 18.6348 18.6163 17.5566L21.9365 2.28183L21.9374 2.28093C22.2317 0.934542 21.4415 0.408044 20.5377 0.738343L1.02135 8.0742C-0.310594 8.5818 -0.290427 9.3108 0.794932 9.6411L5.78447 11.1648L17.3742 4.04493C17.9196 3.69033 18.4155 3.88653 18.0076 4.24112L8.63262 12.5058Z" fill="#636F75" fill-opacity="0.6"/>
                                </svg>
                                <span class="button__text">Telegram</span>
                            </a>
                        </div>
                    </div>
                    <div class="fullscreen-menu__socials">
                        <?php
                        echo $mustache->render(
                            'social-list',
                            include(CONTEXT_DIR . '/social.php')
                        )
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fullscreen-menu-pattern fullscreen-menu-pattern_rectangles">
        <?php
        echo $mustache->render('background-rectangles');
        ?>
    </div>
    <div class="fullscreen-menu-pattern fullscreen-menu-pattern_circle-big">
        <?php
        echo $mustache->render('background-circle-dashed');
        ?>
    </div>
    <div class="fullscreen-menu-pattern fullscreen-menu-pattern_circle-small">
        <?php
        echo $mustache->render('background-circle-dashed');
        ?>
    </div>
    <div class="fullscreen-menu-pattern fullscreen-menu-pattern_dots-big">
        <?php
        echo $mustache->render('background-dots');
        ?>
    </div>
    <div class="fullscreen-menu-pattern fullscreen-menu-pattern_dots-small">
        <?php
        echo $mustache->render('background-dots');
        ?>
    </div>
    <div class="screen-overlay"></div>
</header>

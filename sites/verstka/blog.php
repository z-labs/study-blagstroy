<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/auth.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/../../vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/constants.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Благовещенскстрой: Блог</title>
    <link rel="stylesheet" href="/local/assets/local/common/common.css">
    <link rel="stylesheet" href="/local/assets/local/feedback-form/feedback-form.css">
    <link rel="stylesheet" href="/local/assets/local/blog/blog.css">
</head>
<body class="page">
<?php require_once 'header.php'; ?>

<div class="page-content">
    <div class="container">
        <?php
        echo $mustache->render('breadcrumbs', include(CONTEXT_DIR . '/article/breadcrumbs.php'));
        ?>
        <h1 class="page__title">
            Полезная информация
        </h1>
    </div>
    <div class="blog">
        <div class="blog__svg1 svg-pattern">
            <?php
            echo $mustache->render('background-circle-dashed');
            ?>
        </div>
        <div class="blog__svg2 svg-pattern">
            <?php
            echo $mustache->render('background-dots');
            ?>
        </div>
        <div class="blog__svg3 svg-pattern">
            <?php
            echo $mustache->render('background-circle-dashed');
            ?>
        </div>
        <div class="blog__svg4 svg-pattern">
            <?php
            echo $mustache->render('background-circle-dashed');
            ?>
        </div>
        <div class="container">
            <?php
            echo $mustache->render('rubric', include(CONTEXT_DIR . 'blog/rubric.php'));
            ?>
            <div class="blog__main">
                <div>
                    <?php
                    echo $mustache->render('blog-items', include(CONTEXT_DIR . 'blog/blog-items.php'));
                    ?>
                    <div class="blog__pages">
                        <?php
                        echo $mustache->render('pages', include(CONTEXT_DIR . 'blog/pages.php'));
                        ?>
                    </div>
                </div>
                <div class="blog__sticky">
                    <svg class="blog__sticky-svg desktop-only" width="270" height="99" viewBox="0 0 270 99" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g style="mix-blend-mode:overlay">
                            <path d="M19.0824 7.94925L35.3682 12.9098L32.2646 29.4608L15.9794 24.4976L19.0824 7.94925Z" stroke="#D4DEE1" stroke-miterlimit="10"/>
                            <path d="M32.2641 29.4608L24.9955 34.3696L8.7103 29.4064L15.9789 24.4976L32.2641 29.4608Z" stroke="#D4DEE1" stroke-miterlimit="10"/>
                            <path d="M15.9795 24.4972L8.71093 29.4059L11.814 12.8576L19.0825 7.94878L15.9795 24.4972Z" stroke="#D4DEE1" stroke-miterlimit="10"/>
                        </g>
                        <g style="mix-blend-mode:overlay">
                            <path d="M226.437 58.9224L249.97 45.4862L265.742 67.1595L242.206 80.5921L226.437 58.9224Z" stroke="#D4DEE1" stroke-miterlimit="10"/>
                            <path d="M265.742 67.1591L263.485 80.9366L239.949 94.3692L242.206 80.5917L265.742 67.1591Z" stroke="#D4DEE1" stroke-miterlimit="10"/>
                            <path d="M242.206 80.5918L239.949 94.3693L224.18 72.6996L226.437 58.922L242.206 80.5918Z" stroke="#D4DEE1" stroke-miterlimit="10"/>
                        </g>
                    </svg>
                    <?php
                    echo $mustache->render('consultation-form_gray', include(CONTEXT_DIR . '/article/article-form.php'));
                    ?>
                </div>
            </div>
            <div class="blog__bottom">
                <?php
                echo $mustache->render('index-text', include(CONTEXT_DIR . '/blog/index-text.php') );
                ?>
            </div>
        </div>
    </div>
</div>

<?php require_once 'footer.php'; ?>
<script>
    if (!window.initFeedback) {
        window.initFeedback = [];
    }
    window.initFeedback.push(function () {
        $('#article-form').feedbackForm();
    });
</script>

<script type="text/javascript" src="https://static.yandex.net/browser-updater/v1/script.js" defer></script>
<script src="/local/assets/local/common/common.js" defer></script>
<script src="/local/assets/vendor/form/jquery.form.min.js" defer></script>
<script src="/local/assets/local/feedback-form/feedback-form.js" defer></script>
<script src="/local/assets/local/blog/blog.js" defer></script>
</body>
</html>

<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/auth.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/../../vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/constants.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Благовещенскстрой: Проекты компании</title>
    <link rel="stylesheet" href="/local/assets/local/common/common.css">
    <link rel="stylesheet" href="/local/assets/local/feedback-form/feedback-form.css">
    <link rel="stylesheet" href="/local/assets/local/projects/projects.css">
</head>
<body class="page">
<?php require_once 'header.php'; ?>

<div class="page-content">
    <div class="container">
        <?php
        echo $mustache->render('breadcrumbs', include(CONTEXT_DIR . '/projects/breadcrumbs.php'));
        ?>
        <h1 class="page__title">
            Проекты компании
        </h1>
    </div>
    <div class="projects">
        <div class="projects__svg-pattern projects-pattern-circle-top svg-pattern">
            <?php
            echo $mustache->render('background-circle-dashed');
            ?>
        </div>

        <div class="projects__svg-pattern svg-pattern">
            <?php
            echo $mustache->render('background-dots');
            ?>
        </div>

        <div class="container">
            <div class="projects__content">
                <section class="projects__list">
                    <div class="projects__list-top">
                        <?php
                        echo $mustache->render(
                            'projects-list-rubric',
                            include(CONTEXT_DIR . '/projects/projects-list-rubric.php')
                        );
                        ?>
                    </div>
                    <?php
                    echo $mustache->render(
                        'projects-list',
                        include(CONTEXT_DIR . '/projects.php')
                    );
                    ?>
                </section>
                <?php
                echo $mustache->render('infinite');
                ?>
            </div>
            <?php
            echo $mustache->render(
                'archive',
                include(CONTEXT_DIR . '/projects/archive.php')
            );
            ?>
            <?php
            echo $mustache->render(
                'hot-deals',
                include(CONTEXT_DIR . '/projects/hot-deals.php')
            );
            ?>
        </div>
        <?php
        echo $mustache->render('consultation-form', include(CONTEXT_DIR . '/article/article-form.php'));
        ?>
        <?php
            echo $mustache->render('index-text', include(CONTEXT_DIR . '/projects/text.php'))
        ?>
    </div>
</div>

<?php require_once 'footer.php'; ?>
<script>
    if (!window.initFeedback) {
        window.initFeedback = [];
    }
    window.initFeedback.push(function () {
        $('#article-form').feedbackForm();
    });
</script>

<script type="text/javascript" src="https://static.yandex.net/browser-updater/v1/script.js" defer></script>
<script src="/local/assets/local/common/common.js" defer></script>
<script src="/local/assets/vendor/form/jquery.form.min.js" defer></script>
<script src="/local/assets/local/feedback-form/feedback-form.js" defer></script>
<script src="/local/assets/local/projects/projects.js" defer></script>
</body>
</html>

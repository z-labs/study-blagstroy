import './index-about-slider.mustache'
import {Slider} from "../slider/slider";

export const indexAboutSlider = () => {
  new Slider({
    sliderContainerSelector: '.index-about-slider',
    wrapperClass: "index-about-slider-wrapper",
    slideClass: "index-about-slide",
    loop: true,
    pagination: {
      el: '.index-about-slider-pagination',
      clickable: true
    },
    on: {
      slideChangeTransitionEnd: function () {
        bLazy.revalidate();
      }
    },
    breakpoints: {
      1366: {
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
          renderBullet: renderAboutSliderBullets
        }
      },
      1920: {
        direction: 'vertical',
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
          renderBullet: renderAboutSliderBullets
        }
      },
    }
  });

  function renderAboutSliderBullets(index, className) {
    let strIndex = (index < 9 ? '0' + (index + 1) : index + 1);

    return `
      <span class="${className}">
        <span class="swiper-pagination-dot"></span>
        <span class="swiper-pagination-dot-value">${strIndex}</span>
      </span>
    `;
  }
};

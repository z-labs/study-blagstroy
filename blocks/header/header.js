import '../contact/contact-phone.mustache'
import '../contact/contact-email.mustache'
import '../header-search/header-search.mustache'
import {FullscreenMenu} from "../fullscreen-menu/fullscreen-menu"
import Headroom from "headroom.js"
import {HeaderMenu} from "../header-menu/header-menu";

export const header = () => {
  new HeaderMenu();
  new FullscreenMenu();

  if (!isDesktop()) {
    headroomInit();
  }

  function headroomInit() {
    const header = document.querySelector(".header");
    const headroom = new Headroom(header);
    headroom.init();
  }
};

import './projects-list.mustache'
import '../projects-list-rubric/projects-list-rubric.mustache'

import {Dropdown} from '../dropdown/dropdown'
import {projectsListRubric} from "../projects-list-rubric/projects-list-rubric";

export default () => {
  new Dropdown({
    selectors: {
      dropdown: '.projects__list-top .projects-list-rubric',
    },
    changeItemActivity: true,
    preventLinkClick: false,
    onInit: (dropdown) => {
      let activeItem = dropdown.find('.dropdown-item_selected')

      if(activeItem.length) {
        dropdown.find('.dropdown-current__text').text(activeItem.data('value'))
      }
    }
  });

  projectsListRubric();
}

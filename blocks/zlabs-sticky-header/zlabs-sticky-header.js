export class ZLabsStickyHeader {
  constructor(element, params) {

    if (typeof element === "string") {
      this.header = document.querySelector(element);
    }
    else {
      this.header = element;
    }

    this.params = extend({
      offsetTop: 0,
      zIndex: 9999,
      onStick: undefined,
      onUnstick: undefined
    }, params);

    this.sticky = false;
    this.init();
  }

  init() {
    this.elementStartTopPos = this.header.getBoundingClientRect().top + window.pageYOffset;
    this.attachEvents();
    this.checkPosition();
  }

  attachEvents() {
    window.addEventListener('scroll', this.checkPosition.bind(this));
  }

  checkPosition() {
    if (window.pageYOffset + this.params.offsetTop > (this.elementStartTopPos)) {
      if (!this.sticky) {
        this.sticky = true;

        this.originalAttr = {
          position: window.getComputedStyle(this.header, null).getPropertyValue('position')
        };

        this.header.setAttribute('style', `position: fixed; top: ${this.params.offsetTop}px; z-index: ${this.params.zIndex}`);

        if ( this.params.onStick && (typeof this.params.onStick === 'function') ) {
          this.params.onStick();
        }
      }
    }

    else {
      if (this.sticky) {
        this.sticky = false;

        this.header.removeAttribute('style');

        if ( this.params.onUnstick && (typeof this.params.onStick === 'function') ) {
          this.params.onUnstick();
        }
      }
    }
  }
}

function extend(a, b) {
  for (var key in b)
    if (b.hasOwnProperty(key))
      a[key] = b[key];
  return a;
}

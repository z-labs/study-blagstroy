import './project-detail-about.mustache'
import '../characteristics-table/characteristics-table.mustache'
import '../characteristics-table/characteristics-table_main.mustache'
import '../project-detail-about/project-detail-about-slider.mustache'
import '../index-tour-form/index-tour-form.mustache'
import '../project-detail-about/project-detail-about-side.mustache'
import {Slider} from "../slider/slider";
import {CollapsedBlock} from "../collapsed-block/collapsed-block";

export const projectDetailAbout = () => {
    $('.project-detail-slider-container').each((index, element) => {
        new Slider({
            thumbnailsMarker: '.project-detail-slider-thumbs__marker',
            sliderContainerSelector: element.querySelector('.project-detail-about-slider'),
            slideThumbnail: ".project-detail-slider-thumbs-item",
            slideThumbnailActiveClass: "project-detail-slider-thumbs-item_active",
            loop: true,
            pagination: {
                el: '.project-detail-slider-pagination',
                type: 'fraction',
                formatFractionCurrent: function (number) {
                    if (number <= 9) {
                        number = '0' + number
                    }
                    return number
                },
                formatFractionTotal: function (number) {
                    if (number <= 9) {
                        number = '0' + number
                    }
                    return number
                }
            },
            navigation: {
                nextEl: element.querySelector('.project-detail-about-slider__button-next'),
                prevEl: element.querySelector('.project-detail-about-slider__button-prev'),
            },
            on: {
                slideChangeTransitionEnd: function () {
                    bLazy.revalidate();
                }
            }
        });
    })

    let characteristicsAccordion = new CollapsedBlock({
        selectors: {
            container: '.characteristics-table__collapsed-block-container',
            triggerText: '.collapsed-block-trigger__text'
        },
        settings: {
            triggerText: 'Все характеристики',
            triggerTextOpened: 'Свернуть'
        }
    });

    $('.characteristics-table__btn-move-to-all').on('click', function () {
        let $header = $('.header');
        let $characteristicsTable = $('.article-main .characteristics-table');

        $("html, body").animate({
            scrollTop: $characteristicsTable.offset().top - $header.height() - 16
        }, 500);

        if (!characteristicsAccordion.opened) {
            characteristicsAccordion.toggle();
            characteristicsAccordion.expand();
        }
    });
};

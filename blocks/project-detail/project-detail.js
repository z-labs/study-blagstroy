import './project-detail.sass'
import {projectDetailSlider} from "../project-detail-slider/project-detail-slider";
import {pageMenuNavigation} from "../page-menu-navigation/page-menu-navigation";
import '../project-detail-prices/project-detail-prices.mustache'
import './project-detail-article.mustache'
import '../project-detail-first-screen/project-detail-first-screen-slider.mustache'
import '../project-detail-first-screen/project-detail-first-screen-info.mustache'
import '../project-detail-first-screen/project-detail-first-screen__background.mustache'
import '../project-detail-futures/project-detail-futures.mustache'
import '../consultation-form/consultation-form.mustache'
import {projectDetailAbout} from "../project-detail-about/project-detail-about";
import {projectLayout} from "../project-layout/project-layout";
import building from "../building/building";
import documents from "../documents/documents";
import {buyWays} from "../buy-ways/buy-ways";
import otherProjects from "../other-projects/other-projects"
import {ZLabsStickyHeader} from "../zlabs-sticky-header/zlabs-sticky-header";
import {infrastructureMap} from "../infrastructure-map/infrastructure-map";

$(document).ready(function() {
  projectDetailSlider();
  pageMenuNavigation();
  projectDetailAbout();
  projectLayout();
  building();
  buyWays();
  documents();
  otherProjects();
  infrastructureMap();

  new ZLabsStickyHeader('.page-menu-navigation-body', {
      offsetTop: 73,
      onStick: function() {
        $('.page-menu-navigation-body').addClass('page-menu-navigation-body_sticky');
      },
      onUnstick: function() {
        $('.page-menu-navigation-body').removeClass('page-menu-navigation-body_sticky');
      }
  });

});

import '../site-socials/site-socials.mustache'

export default () => {

  $(".ya-share3__list").addClass("hidden");

  if ($(window).width() < 1366) {
    $(".site-socials__button").click(function() {
      $(".site-socials__list").toggleClass("open");
      $(".site-socials__list").toggleClass("hidden");
    });
  }

  if ($(window).width() >= 1366) {
    $(".site-socials").mouseover(function(){
      $(".site-socials__list").toggleClass("open");
      $(".site-socials__list").toggleClass("hidden");
    }).mouseout(function(){
      $(".site-socials__list").toggleClass("open");
      $(".site-socials__list").toggleClass("hidden");
    });
  }

  $('.ya-share2__item_copy').on('click', function (event) {
    let div = document.createElement('div');
    div.className = "copy-popup copy-popup_show";
    div.innerHTML = "Ссылка скопирована в буфер обмена";
    document.body.append(div);
    let RealHint = $('.copy-popup');
    setTimeout(function () {
        $(RealHint).removeClass('copy-popup_show');
        $(RealHint).addClass('copy-popup_hide');
      }
      , 1000);
    setTimeout(function () {
        $(RealHint).remove()
      }
      , 3000);
    let content_link = $(this).find('.ya-share2__input_copy');
    content_link.select();
    document.execCommand("Copy", false, null);
  });
}


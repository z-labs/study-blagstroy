import './index-about.mustache'
import {indexAboutSlider} from "../index-about-slider/index-about-slider";
import {indexAboutStats} from "../index-about-stats/index-about-stats";

export const indexAbout = () => {
  indexAboutSlider();

  if (!isMobile()) {
    indexAboutStats();
  }
};

import {Slider} from "../slider/slider";

export const projectDetailSlider = () => {
  let firstScreenSlidesCount = $('.project-detail-first-screen-slider').find('.project-detail-slider-slide').length;

  if (firstScreenSlidesCount > 1) {
    new Slider({
      sliderContainerSelector: '.project-detail-first-screen-slider',
      wrapperClass: "project-detail-slider-wrapper",
      slideClass: "project-detail-slider-slide",
      slideThumbnail: ".project-detail-slider-thumbs-item",
      slideThumbnailActiveClass: "project-detail-slider-thumbs-item_active",
      loop: true,
      pagination: {
        el: '.project-detail-slider-pagination',
        type: 'fraction',
        formatFractionCurrent: function (number) {
          if (number <= 9) {
            number = '0' + number
          }
          return number
        },
        formatFractionTotal: function (number) {
          if (number <= 9) {
            number = '0' + number
          }
          return number
        }
      },
      navigation: {
        nextEl: '.project-detail-slider__button-next',
        prevEl: '.project-detail-slider__button-prev',
      },
      on: {
        slideChangeTransitionEnd: function () {
          bLazy.revalidate();
        }
      }
    });
  }

  $('.project-detail-slider-slide:not(.swiper-slide-duplicate) a[data-fancybox]').fancybox();
};

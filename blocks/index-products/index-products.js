import './index-products.mustache'
import '../product-card/product-card.mustache'
import '../product-card/product-card_sale.mustache'
import {Sections} from "../sections/sections";
import {ListItemsHover} from "../list-items-hover/list-items-hover";
import {Dropdown} from "../dropdown/dropdown";

export const IndexProducts = () => {
  let indexProductsSections = new Sections({
    selectors: {
      sectionsList: '.products-list',
      select: '.index-products-filter-combobox',
      tabItem: '.index-products-filter-tabs-item',
      tabItemTrigger: '.index-products-filter-tabs-item__button'
    },
    classes: {
      tabItemActive: 'sections-filter-tabs-item_active'
    },
    settings: {
      breakpoints: {
        768: {
          flexList: true
        }
      }
    }
  });

  new Dropdown({
    selectors: {
      dropdown: '.index-products-filter-combobox-wrapper',
    },
    changeItemActivity: true,
    preventLinkClick: false,
    onInit: (dropdown) => {
      let activeItem = dropdown.find('.dropdown-item_selected')

      if(activeItem.length) {
        dropdown.find('.dropdown-current__text').text(activeItem.text())
      }
    },
    onChange: (event) => {
      let currentItem = event.currentTarget;

      $(currentItem)
        .closest('.dropdown')
        .find('.dropdown-current__text').text($(currentItem).text());

      if (indexProductsSections && indexProductsSections.changeTab) {
        indexProductsSections.changeTab($(currentItem).data('value'));
      }
    }
  });

  if (isDesktop()) {
    new ListItemsHover({
      selectors: {
        container: '.index-products-filter-tabs-wrapper',
        listItem: '.index-products-filter-tabs-item'
      },
      classes: {
        listItemActive: 'sections-filter-tabs-item_active'
      }
    });
  }
}

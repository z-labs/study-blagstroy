import './contacts.sass'
import '../contacts-info/contacts-info.mustache'
import '../contacts-departments/contacts-departments.mustache'
import './contacts-map.mustache'
import '../contacts-messengers-links/contacts-messengers-links.mustache'
import '../contact-us-form/contact-us-form.mustache'

$( document ).ready(function() {
  let mapData = $('#contacts-map');
  ymaps.ready(init);
  function init(){
    var myMap = new ymaps.Map("contacts-map", {
      center: [mapData.attr('data-x'), mapData.attr('data-y')],
      zoom: 17
    });
    var placemark = new ymaps.Placemark([mapData.attr('data-x'), mapData.attr('data-y')], {
        balloonContent: mapData.attr('data-balloon-content'),
        iconCaption: mapData.attr('data-icon-caption')
      }, {

      });
    myMap.geoObjects.add(placemark);
  }
});

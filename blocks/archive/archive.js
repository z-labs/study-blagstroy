import './archive.mustache'
import Swiper from "swiper";

export default () => {

  let numberSlides = $('.archive__slide').length;

  function getNavigation() {
    return {
      nextEl: '.article-slider__button-next',
      prevEl: '.article-slider__button-prev',
    }
  };

  function getPagination() {
    return {
      clickable: true,
      el: '.archive__pagination',
      type: 'bullets',
      dynamicBullets: true,
      dynamicMainBullets: 1,
    }
  }

  var mySwiper = new Swiper('.archive__slider', {
    allowTouchMove: numberSlides > 1,
    spaceBetween: 10,
    pagination: {
      el: '.archive__pagination',
      type: 'fraction',
      formatFractionCurrent: function (number) {
        if (number <= 9) {
          number = '0' + number
        }
        return number
      },
      formatFractionTotal:	function(number) {
        if (number <= 9) {
          number = '0' + number
        }
        return number
      }
    },
    on: {
      slideChange: function () {
        bLazy.revalidate()
      }
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
        spaceBetween: 16,
        pagination: numberSlides > 2 ? getPagination() : false,
        allowTouchMove: numberSlides > 2,
      },
      1366: {
        slidesPerView: 3,
        spaceBetween: 24,
        pagination: false,
        navigation: numberSlides > 3 ? getNavigation() : false,
        allowTouchMove: numberSlides > 3,
      },
    },
  });
}

import './index-projects.mustache'
import './index-projects-cards.mustache'
import '../index-projects-card/index-projects-card-1.mustache'
import '../index-projects-card/index-projects-card-2.mustache'
import '../index-projects-card/index-projects-card-3.mustache'
import '../index-projects-card/index-projects-card-4.mustache'
import '../index-projects-card/index-projects-card.mustache'
import {Sections} from "../sections/sections";
import {Dropdown} from "../dropdown/dropdown";

export const indexProjects = () => {
  let indexProjectsSections = new Sections({
      selectors: {
          sectionsList: '.index-projects-lists-item',
          select: '.index-projects-filter-combobox',
          tabItem: '.index-projects-filter-tabs-item',
      },
      classes: {
          tabItemActive: 'sections-filter-tabs-item_active'
      }
  });

  new Dropdown({
    selectors: {
      dropdown: '.index-projects-filter-combobox-wrapper',
    },
    changeItemActivity: true,
    preventLinkClick: false,
    onInit: (dropdown) => {
      let activeItem = dropdown.find('.dropdown-item_selected')

      if(activeItem.length) {
        dropdown.find('.dropdown-current__text').text(activeItem.text())
      }
    },
    onChange: (event) => {
      let currentItem = event.currentTarget;

      $(currentItem)
        .closest('.dropdown')
        .find('.dropdown-current__text').text($(currentItem).text());

      if (indexProjectsSections && indexProjectsSections.changeTab) {
        indexProjectsSections.changeTab($(currentItem).data('value'));
      }
    }
  });
};

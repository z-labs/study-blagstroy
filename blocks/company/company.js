import '../company/company.sass'
import topSlider from '../company-top/company-top'
import './company-body.mustache'
import './company-photos-second.mustache'
import companySlider from '../company-slider/company-slider'
import '../company-photos/company-photos.mustache'
import '../company-slider-second/company-slider-second.mustache'
import "./company-photos-second.mustache"
import "./company-photos-simple.mustache"
import companySliderSecond from '../company-slider-second/company-slider-second'
import employee from '../company-employee/company-employee'
import navigation from '../company-navigation/company-navigation'
import '../svg/background-square-dots.mustache'
import sliderVideo from '../company-video/company-video'

$(document).ready(function () {
  topSlider();
  companySlider();
  companySliderSecond();
  employee();
  sliderVideo();

  let imgLoader = new Promise(resolve => {
    bLazy.load($('.b-lazy'), true)
    setTimeout(resolve, 2000)
  });

  imgLoader.then(() => {
    navigation()
    $(window).scroll()
  });
});

import './projects.sass'
import archiveSlider from '../archive/archive'
import projectsList from '../projects-list/projects-list'
import hotDeals from '../hot-deals/hot-deals'
import {InfinityNav} from '../infinite/infinite'
import {indexText} from "../index-text/index-text"

$(document).ready(function () {
  archiveSlider();
  projectsList();
  hotDeals();
  new InfinityNav;
  indexText();
});

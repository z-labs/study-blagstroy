import './documents.mustache'

import {Dropdown} from '../dropdown/dropdown'

export default () => {
  new Dropdown({
    changeItemActivity: true,
    preventLinkClick: true,
    selectors: {
      dropdown: '.documents__dropdown',
    },
    onChange: function (event) {
      let filter = $(event.currentTarget).find('.dropdown-item__content').attr('href');
      if ($(filter)) {
        $('.documents__list_active').removeClass('documents__list_active');
        setTimeout(function () {
          $(filter).addClass('documents__list_active')
        }, 150)
      }
    },
  });

  $('.documents__tab').on('click', function (e) {

    let active = $('.documents__tab_active').attr('href');

    if (active === $(this).attr('href')) {
      e.preventDefault();
      return
    }

    $('.documents__tab_active').removeClass('documents__tab_active');
    $(this).addClass('documents__tab_active');

    let filter = $(this).attr('href');
    if ($(filter)) {
      $('.documents__list_active').removeClass('documents__list_active');
      setTimeout(function () {
        $(filter).addClass('documents__list_active')
      }, 100)
    }
  })
}

import Swiper from 'swiper';

import './article-else.mustache'

let numberSlides = $('.article-else__slide').length;

function getNavigation() {
  return {
    nextEl: '.article-else__slider__button-next',
    prevEl: '.article-else__slider__button-prev',
  }
};

function getPagination() {
  return {
    el: '.article-else__slider-pagination',
    type: 'bullets',
    dynamicBullets: true,
    dynamicMainBullets: 1,
    clickable: true,
  }
};

export default () => {

  var mySwiper = new Swiper('.article-else__slider', {
    slidesPerView: 1,
    spaceBetween: 10,
    pagination: numberSlides > 1 ? getPagination() : false,
    allowTouchMove: numberSlides > 1,
    on: {
      slideChange: function () {
        bLazy.revalidate()
      }
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
        pagination: numberSlides > 2 ? getPagination() : false,
        allowTouchMove: numberSlides > 2,
      },
      1366: {
        slidesPerView: 3,
        navigation: numberSlides > 3 ? getNavigation() : false,
        pagination: numberSlides > 3 ? getPagination() : false,
        allowTouchMove: numberSlides > 3,
      }
    }
  });
}

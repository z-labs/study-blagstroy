export const articleText = () => {
  $(document).on('click', '.article-text-image-link', (event) => {
    event.preventDefault();
    let imageSrc = $(event.currentTarget).attr('href');

    $.fancybox.open({
      src: imageSrc
    });
  });

  $(document).on('click', '.article-slider-image-link', (event) => {
    event.preventDefault();
    let link = event.currentTarget;
    let currentSlideIndex = $(link).closest('.swiper-slide').data('swiper-slide-index');

    let $slides = $(link).closest('.swiper-wrapper').find('.swiper-slide:not(.swiper-slide-duplicate)');
    let fancyboxGallery = [];

    $slides.each((index, item) => {
      fancyboxGallery.push({
        src: $(item).find('.article-slider-image-link').attr('href')
      });
    });

    $.fancybox.open(fancyboxGallery, {
      loop: true
    }, currentSlideIndex);
  });
};

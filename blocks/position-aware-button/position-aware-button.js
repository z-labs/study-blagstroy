export default () => {
  $(function() {

    $('.position-aware-button')
      .on('mouseenter', function(e) {
        var parentOffset = $(this).offset(),
          relX = e.pageX - parentOffset.left,
          relY = e.pageY - parentOffset.top;
        $(this).find('.position-aware-button-span').css({top:relY, left:relX})
      })
      .on('mouseout', function(e) {
        var parentOffset = $(this).offset(),
          relX = e.pageX - parentOffset.left,
          relY = e.pageY - parentOffset.top;
        $(this).find('.position-aware-button-span').css({top:relY, left:relX})
      });
  });
}

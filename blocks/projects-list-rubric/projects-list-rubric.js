import {ListItemsHover} from "../list-items-hover/list-items-hover";

export const projectsListRubric = () => {
  if (isDesktop()) {
    $('.projects-list-tabs-wrapper').each(function(index, element) {
      new ListItemsHover({
        listElement: element,
        selectors: {
          container: '.projects-list-tabs-wrapper',
          listItem: '.projects-list__rubric-item'
        },
        classes: {
          listItemActive: 'projects-list__rubric-item_active'
        }
      });
    });
  }
};

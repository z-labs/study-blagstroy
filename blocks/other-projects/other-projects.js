import Swiper from 'swiper';

import './other-projects.mustache'

export default () => {

  let numberSlides = $('.other-projects__slide').length;

  function getNavigation() {
    return {
      nextEl: '.other-projects__button-next',
      prevEl: '.other-projects__button-prev',
    }
  };

  if (numberSlides > 1) {
    $('.other-projects__pagination').removeClass('other-projects__pagination_disabled');
  }

  let mySwiper = new Swiper('.other-projects__slider', {
    spaceBetween: 10,
    navigation: numberSlides > 1 ? getNavigation() : false,
    allowTouchMove: numberSlides > 1,
    on: {
      slideChange: function () {
        bLazy.revalidate()
      }
    },
    pagination: {
      el: '.other-projects__pagination',
      type: 'fraction',
      formatFractionCurrent: function (number) {
        if (number <= 9) {
          number = '0' + number
        }
        return number
      },
      formatFractionTotal: function (number) {
        if (number <= 9) {
          number = '0' + number
        }
        return number
      }
    },
    breakpoints: {
      768: {
        slidesPerView: 'auto',
        spaceBetween: 20,
        pagination: {
          el: null,
          type: null
        }
      },
      1366: {
        slidesPerView: 3,
        spaceBetween: 24,
        navigation: numberSlides > 3 ? getNavigation() : false,
        allowTouchMove: numberSlides > 3,
        pagination: {
          el: null,
          type: null
        }
      },
    },
  });
}

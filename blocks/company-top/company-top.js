import './company-top.mustache'
import Swiper from 'swiper'

export default () => {
  let mySwiper = new Swiper('.company-top__slider', {
    loop: true,
    slidesPerView: 1,
    spaceBetween: 10,
    pagination: {
      el: '.company-top__slider-pagination',
      type: 'bullets',
    },
    navigation: {
      nextEl: '.company-top__slider-button-next',
      prevEl: '.company-top__slider-button-prev',
    },
    on: {
      slideChange: function () {
        bLazy.revalidate()
      },
    },
  });

  mySwiper.on('slideChange', function () {
    $('.company-top__text').addClass('company-top__text_hidden');
  });

  mySwiper.on('slideChangeTransitionEnd', function() {
    let text = $('.company-top__slide.swiper-slide-active').data('text');
    $('.company-top__text').removeClass('company-top__text_hidden');
    $('.company-top__text').text(text);
  });
}

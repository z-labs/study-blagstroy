import '../project-layout-rooms-container/project-layout-rooms-container.mustache'
import './project-layout-rooms.mustache'
import './slides/project-layout-rooms-slide.mustache'
import Swiper from "swiper";

export class ProjectLayoutRooms {
  constructor(params) {
    this.params = $.extend(true, {
      layoutContentElement: null,
      selectors: {
        roomsTab: '.project-layout-rooms-tab'
      },
      classes: {
        tabActive: 'sections-filter-tabs-item_active'
      }
    }, params);

    this.hideNavigation = this.hideNavigation.bind(this);
    this.showNavigation = this.showNavigation.bind(this);
    this.getSlidesPerView = this.getSlidesPerView.bind(this);
    this.needSlider = this.needSlider.bind(this);
    this.getSlidesCount = this.getSlidesCount.bind(this);
    this.filterSlides = this.filterSlides.bind(this);
    this.showSlide = this.showSlide.bind(this);
    this.hideSlide = this.hideSlide.bind(this);
    this.initSwiper = this.initSwiper.bind(this);

    if (this.params.layoutContentElement) {
      this.$layoutContentElement = $(this.params.layoutContentElement);
    }

    this.$roomsSlider = this.$layoutContentElement.find('.project-layout-rooms');
    this.roomsSliderPagination = this.$roomsSlider.find('.project-layout-rooms__pagination').get(0);
    this.roomsSliderNavigation = {
      prev: this.$roomsSlider.parent().find('.project-layout-rooms-navigation__prev').get(0),
      next: this.$roomsSlider.parent().find('.project-layout-rooms-navigation__next').get(0)
    };

    this.sliderParams = {
      slidesPerView: 1,
      navigation: {
        nextEl: this.roomsSliderNavigation.next,
        prevEl: this.roomsSliderNavigation.prev
      },
      pagination: {
        el: this.roomsSliderPagination,
        type: 'bullets',
        clickable: true
      },
      on: {
        slideChange: (function () {
          bLazy.load(this.$roomsSlider.find('.room-slide__img'), true);
        }).bind(this),
      },
      breakpoints: {
        768: {
          slidesPerView: 2
        },
        1366: {
          slidesPerView: 3,
          spaceBetween: 20
        },
      },
    };

    this.init();
  }

  init() {
    this.initSwiper();
    this.attachEvents();
  }

  initSwiper() {
    if (this.needSlider()) {
      this.sliderParams.pagination.dynamicBullets = this.needDynamicBullets();
      if (!this.needDynamicBullets()) {
        this.$roomsSlider.find('.project-layout-rooms__pagination').removeClass('swiper-pagination-bullets-dynamic');
      }

      this.swiperInstance = new Swiper(this.$roomsSlider, this.sliderParams);
      this.showNavigation();

    this.initRoomFloorsSlider();
    }
    else {
      this.hideNavigation();
    }
    bLazy.revalidate();
  }

  initRoomFloorsSlider() {
    this.$roomsSlider.find('.room-floors-container').each(function (index, element) {
      let $slides = $(element).find('.room-floors-slide');

      if ($slides.length >= 2) {
        new Swiper($(element).get(0), {
          allowTouchMove: false,
          noSwiping: true,
          nested: true,
          loop: true,
          navigation: {
            prevEl: $(element).closest('.room-slide-img-container').find('.room-floors-navigation__prev').get(0),
            nextEl: $(element).closest('.room-slide-img-container').find('.room-floors-navigation__next').get(0)
          },
          on: {
            slideChangeTransitionEnd: function () {
              bLazy.revalidate();
            }
          }
        });
      } else {
        $(element)
          .closest('.room-slide-img-container')
          .find('.swiper-navigation-arrow')
          .addClass('swiper-navigation-arrow_hide');
      }
    });
  }

  openFancybox(event) {
    event.preventDefault();

    let nodeId = $(event.currentTarget).data('node'),
      index = $(event.currentTarget).data('index'),
      fancyboxGallery = [];

    roomsFloors[nodeId].forEach(function (value) {
      fancyboxGallery.push({
        src: value.realSrc,
        opts: {
          thumb: value.src
        }
      });
    });

    $.fancybox.open(fancyboxGallery, {
      loop: true
    }, index);
  }

  attachEvents() {
    this.$layoutContentElement.find('.project-layout-rooms-tab__button').on('click', (function(event) {
      let filterValue = $(event.currentTarget).val();

      this.clearTabsSelect();
      this.selectTab(event.currentTarget);

      setTimeout((function() {
        this.$layoutContentElement.find('.project-layout-rooms').removeClass('project-layout-rooms_hide');
      }).bind(this), 400);

      this.$layoutContentElement.find('.project-layout-rooms').addClass('project-layout-rooms_hide');

      this.filterSlides(filterValue);
    }).bind(this));

    this.$layoutContentElement.on('click', '.room-slide__img-link', this.openFancybox)
  }

  clearTabsSelect() {
    this.$layoutContentElement.find(this.params.selectors.roomsTab).removeClass('sections-filter-tabs-item_active');
  }

  selectTab(button) {
    $(button).parent().addClass('sections-filter-tabs-item_active');
  }

  hideNavigation() {
    this.$layoutContentElement.find('.project-layout-rooms-content').addClass('project-layout-rooms-content_hide-navigation');
  }

  showNavigation() {
    this.$layoutContentElement.find('.project-layout-rooms-content').removeClass('project-layout-rooms-content_hide-navigation');
  }

  needDynamicBullets() {
    let maxStaticBulletsCount = 8;
    return (this.getSlidesCount() > maxStaticBulletsCount);
  }

  getSlidesPerView() {
    let count = 1;

    if (window.innerWidth >= 768) {
      count = this.sliderParams.breakpoints['768'].slidesPerView;
    }

    if (window.innerWidth >= 1366) {
      count = this.sliderParams.breakpoints['1366'].slidesPerView;
    }

    return count;
  }

  needSlider() {
    return (this.getSlidesCount() > this.getSlidesPerView());
  }

  getSlidesCount() {
    return this.$layoutContentElement.find('.project-layout-rooms-slide-container:not(.non-swiper-slide)').length;
  }

  filterSlides(filterValue) {
    this.$layoutContentElement.find('.project-layout-rooms-slide-container')
      .each((function(index, element) {
        let elementValues = $(element).data('value').split(',');

        if (filterValue === this.params.allRoomsKey) {
          this.showSlide(element);
          return;
        }

        if (elementValues.includes(filterValue)) {
          this.showSlide(element);
        }
        else {
          this.hideSlide(element);
        }
      }).bind(this));

    setTimeout((function() {
      if (this.swiperInstance) {
        this.swiperInstance.destroy();

        if (this.getSlidesCount() > 0) {
          this.$layoutContentElement.find('.project-layout-rooms-content').show();
          this.initSwiper();
        }
        else {
          this.$layoutContentElement.find('.project-layout-rooms-content').hide();
        }
      }
    }).bind(this), 250);
  }

  showSlide(slideElement) {
    $(slideElement)
      .addClass('swiper-slide')
      .removeClass('non-swiper-slide');
  }

  hideSlide(slideElement) {
    $(slideElement)
      .removeClass('swiper-slide')
      .addClass('non-swiper-slide')
  }
}

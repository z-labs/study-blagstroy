import {Accordion} from "./accordion"

export class AdaptiveAccordion extends Accordion {
    attachEvents() {
        super.attachEvents();

        if (this.params.maxWidth || this.params.minWidth) {
            this.checkAccordion();
            this.params._resizeTimer = -1;
            $(window).resize('resize', this.windowResizeHandler.bind(this));
        }
    }

    windowResizeHandler() {
        clearTimeout(this.params._resizeTimer);

        this.params._resizeTimer = setTimeout(() => {
            this.checkAccordion();
        }, 250);
    }


    checkAccordion() {
        if (this.params.maxWidth) {
            this.params._active = $(window).outerWidth(true) <= this.params.maxWidth;
        }

        if (this.params.minWidth) {
            this.params._active = $(window).outerWidth(true) >= this.params.minWidth;
        }
    }
}

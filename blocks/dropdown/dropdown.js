export class Dropdown {
    constructor(params) {
        this.params = $.extend(true, {
                dropdownElement: null,
                selectors: {
                    dropdown: '.dropdown',
                    current: '.dropdown-current',
                    currentText: '.dropdown-current__text',
                    list: '.dropdown-list',
                    item: '.dropdown-list-item',
                },
                animationSpeed: 400,
                collapseOnMouseLeave: false,
                changeItemActivity: false,
                preventLinkClick: true,
                limitHeight: true,
                multiple: false,
                _active: true,
                classes: {
                    dropdownExpand: 'dropdown_expand',
                    dropdownHasValue: 'dropdown_has-value',
                    itemSelected: 'dropdown-item_selected',
                    disabled: 'dropdown_disabled'
                },
                scrollerParams: {
                    contentClass: 'dropdown-scroll',
                    paneClass: 'dropdown-pane',
                    sliderClass: 'dropdown-pane__slider',
                },
                onInit: false,
                onOpen: false,
                onClose: false,
                onChange: false,
            },
            params);

        this.init();
    }

    init() {
        if (this.params.dropdownElement) {
          this.$dropdown = $(this.params.dropdownElement);
        }
        else {
          this.$dropdown = $(this.params.selectors.dropdown);
        }

        this.$list = this.$dropdown.find(this.params.selectors.list);
        this.$list.hide();

        if (this.params.onInit && typeof this.params.onInit === 'function') {
            this.params.onInit(this.$dropdown);
        }

        this.attachEvents();
    }

    attachEvents() {
        this.$dropdown.on('click', this.params.selectors.item, this.itemClickHandler.bind(this))
            .on('click', this.params.selectors.current, this.currentClickHandler.bind(this))
            .on('mouseleave', this.dropdownMouseLeaveHandler.bind(this))
            .on('collapse', this.listCollapse.bind(this))
            .on('expand', this.listExpand.bind(this));

        $(document).on('click', this.documentClickHandler.bind(this));
    }

    currentClickHandler(event) {
        let current = $(event.currentTarget),
            dropdown = current.closest(this.params.selectors.dropdown),
            list = dropdown.find(this.params.selectors.list);

        if (!dropdown.hasClass(this.params.classes.disabled)) {
            this.listToggle(dropdown, list);
        }
    }

    dropdownMouseLeaveHandler(event) {
        if (this.params.collapseOnMouseLeave) {
            let dropdown = $(event.currentTarget),
                list = dropdown.find(this.params.selectors.list);

            if (this.params.collapseOnMouseLeave) {
                setTimeout(() => {
                    this.listCollapse(dropdown, list);
                }, this.params.animationSpeed);
            }
        }
    }

    listToggle(dropdown, list) {
        if (this.params._active) {
            if (dropdown.hasClass(this.params.classes.dropdownExpand)) {
                this.listCollapse(dropdown, list);
            } else {
                this.listExpand(dropdown, list);
            }
        }
    }

    listExpand(dropdown, list) {
        list.stop(true).slideDown(this.params.animationSpeed);
        dropdown.addClass(this.params.classes.dropdownExpand);

        if (this.params.limitHeight) {
            setTimeout(() => {
                list.nanoScroller(this.params.scrollerParams);
            }, this.params.animationSpeed)
        }
    }

    listCollapse(dropdown, list) {
        list.stop(true).slideUp(this.params.animationSpeed);
        dropdown.removeClass(this.params.classes.dropdownExpand);

        if (this.params.collapseOnMouseLeave) {
            dropdown.off('mouseleave');
        }
    }

    itemClickHandler(event) {
        if (this.params.preventLinkClick) {
            event.preventDefault();
        }

        if (this.params._active) {
            let item = $(event.currentTarget),
                dropdown = item.closest(this.params.selectors.dropdown),
                currentText = dropdown.find(this.params.selectors.currentText),
                current = dropdown.find(this.params.selectors.current),
                list = dropdown.find(this.params.selectors.list);

            if (!this.params.multiple) {
                this.listCollapse(dropdown, list);
            }

            if (this.params.changeItemActivity) {
                this.changeActiveItem(item, dropdown);

                if (dropdown.find(`.${this.params.classes.itemSelected}`).length) {
                    dropdown.addClass(this.params.classes.dropdownHasValue);
                } else {
                    dropdown.removeClass(this.params.classes.dropdownHasValue);
                }
            }

            if (this.params.multiple) {
                if (dropdown.find(`.${this.params.classes.itemSelected}`).length === 0) {
                    this.setDefaultValue(current);
                } else {
                    if (!item.hasClass(this.params.classes.itemSelected)) {
                        this.setCurrentValue(currentText, dropdown.find(`.${this.params.classes.itemSelected}:first`).data('value'));
                    } else {
                        this.setCurrentValue(currentText, item.data('value'));
                    }
                }
            } else {
                this.setCurrentValue(currentText, item.data('value'));
            }

            if (this.params.onChange && typeof this.params.onChange === 'function') {
                this.params.onChange(event);
            }
        }
    }

    changeActiveItem(nextItem, dropdown) {
        if (this.params.multiple) {
            nextItem.toggleClass(this.params.classes.itemSelected);
        } else {
            let activeItem = dropdown.find(`.${this.params.classes.itemSelected}`);

            activeItem.removeClass(this.params.classes.itemSelected);
            nextItem.addClass(this.params.classes.itemSelected);
        }
    }

    setDefaultValue(current) {
        if (current.data('placeholder') && current.data('placeholder')) {
            let currentText = current.find(this.params.selectors.currentText);

            this.setCurrentValue(currentText, current.data('placeholder'));
        }
    }

    setCurrentValue(currentText, value) {
        currentText.text(value);
    }

    documentClickHandler(event) {
        if (this.params._active) {
            const target = event.target,
                dropdown = target.closest(this.params.selectors.dropdown);

            if (!dropdown) {
                this.listCollapse(this.$dropdown, this.$list)
            }
        }

        /*if (activeDropdown && activeDropdown.length && dropdown !== activeDropdown) {
            const $activeDropdown = $(activeDropdown),
                $activeDropdownList = $activeDropdown.find(this.params.selectors.list);

            this.listCollapse($activeDropdown, $activeDropdownList);
        }*/
    }

    getActiveDropdown() {
        const self = this;

        return Array.prototype.filter.call(self.$dropdown, dropdown => {
            return dropdown.classList.contains(self.params.classes.dropdownExpand);
        });
    }
}

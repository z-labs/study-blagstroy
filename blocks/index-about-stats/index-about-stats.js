import './index-about-stats.mustache'
import 'jquery.animate-number/jquery.animateNumber.min'

export const indexAboutStats = () => {
  let animationDuration = 3000,
    isAnimationStated = false,
    blockTopPos = $('.index-about').offset().top,
    $window = $(window);

  function animateCounters() {
    $('.index-about-stats-item__value').each(function(index, item) {
      let maxNumber = Number.parseInt($(item).data('maxnumber'));
      $(item)
        .prop('number', Number.parseInt($(item).text()))
        .animateNumber({
        number: maxNumber
      }, animationDuration);
    });
  }

  $window.on('scroll', function() {
    if (!isAnimationStated) {
      if ($window.scrollTop() + $window.height() >= blockTopPos) {
        animateCounters();
        isAnimationStated = true;
      }
    }
  });
};

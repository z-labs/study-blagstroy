import '../project-layout-tabs/project-layout-tabs.mustache'
import '../project-layout-content/project-layout-content.mustache'
import '../project-layout-content/project-layout-content-2.mustache'
import '../project-layout-floor/project-layout-floor.mustache'
import {Sections} from "../sections/sections";
import {projectLayoutRoomsTabs} from "../project-layout-rooms-tabs/project-layout-rooms-tabs";
import {ProjectLayoutRooms} from "../project-layout-rooms/project-layout-rooms";
import {projectLayoutTabs} from "../project-layout-floor-tabs/project-layout-floor-tabs";
import {Dropdown} from "../dropdown/dropdown";

export const projectLayout = () => {
  new Sections({
    selectors: {
      sectionsList: '.project-layout__content-item',
      tabItem: '.project-layout-tab',
      tabItemTrigger: '.project-layout-tabs-item__button',
    },
    classes: {
      tabItemActive: 'sections-filter-tabs-item_active'
    },
    settings: {
      useInputTypeSelect: false,
      triggerPreventDefault: true
    },
    afterChangeTab: function () {
      let tabId = $(this).data('id');
      let currentTab = $('#' + tabId).find('.project-layout-rooms').get(0);
      let currentRoomsTabs = $('#' + tabId).find('.project-layout-rooms-tabs-container').get(0);
      let currentFloorTabs = $('#' + tabId).find('.project-layout-floor-tabs-container').get(0);

      setTimeout(function() {
        currentTab.swiper && currentTab.swiper.update();
        currentFloorTabs.swiper && currentFloorTabs.swiper.update();
        currentRoomsTabs.swiper && currentRoomsTabs.swiper.update();
      }, 500);
    }
  });

  $('.project-layout-content').each(function(index, element) {
    let projectFloorsSections = new Sections({
      container: element,
      selectors: {
        container: '.project-layout-content',
        sectionsList: '.project-layout-floor__item',
        select: '.project-layout-floor-combobox',
        tabItem: '.project-layout-floor-tab',
        tabItemTrigger: '.project-layout-floor-tab__button',
      },
      classes: {
        tabItemActive: 'sections-filter-tabs-item_active'
      }
    });

    new Dropdown({
      dropdownElement: element,
      selectors: {
        dropdown: '.project-layout-floor-combobox-wrapper',
      },
      changeItemActivity: true,
      preventLinkClick: false,
      onInit: (dropdown) => {
        let activeItem = dropdown.find('.dropdown-item_selected')

        if(activeItem.length) {
          dropdown.find('.dropdown-current__text').text(activeItem.text())
        }
      },
      onChange: (event) => {
        let currentItem = event.currentTarget;

        $(currentItem)
          .closest('.dropdown')
          .find('.dropdown-current__text').text($(currentItem).text());

        if (projectFloorsSections && projectFloorsSections.changeTab) {
          projectFloorsSections.changeTab($(currentItem).data('value'));
        }
      }
    });
  });

  projectLayoutTabs();
  projectLayoutRoomsTabs();

  $('.project-layout__content-item').each(function(index, element) {
    new ProjectLayoutRooms({
      allRoomsKey: 'room-all',
      layoutContentElement: element
    });
  });
};

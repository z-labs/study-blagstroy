export class ListItemsHover {

  constructor(params) {
    this.params = $.extend(true,
      {
        listElement: null, // html объект контейнера для точного управления списками с одинаковыми класами
        selectors: {
          container: '.list-items-hover-container',
          listItem: '.list-item',
          listItemText: null // элемент, по которому будет вычисляться ширина подчеркивания
        },
        classes: {
          listItemActive: 'list-item_active'
        },
        settings: {
          defaultLineWidth: 30
        }
      },
      params);

    this.init();
  }

  init() {
    if (this.params.listElement) {
      this.$container = $(this.params.listElement);
    }
    else {
      this.$container = $(this.params.selectors.container);
    }

    this.$marker = this.$container.find('.list-items-hover-marker');
    this.attachEvents();

    setTimeout((function() {
      this.resetMarkerProps();
    }).bind(this), 0);
  }

  attachEvents() {
    let instance = this;

    this.$container.find(this.params.selectors.listItem).hover(
      function() {
        let $itemText = $(this).find(instance.params.selectors.listItemText);

        instance.setMarkerProps({
          width: instance.getWidthByItem($(this)),
          offsetX: $(this).position().left
        })
      },
      function() {
        instance.resetMarkerProps();
      }
    );
  }

  setMarkerProps(props) {
    this.$marker.width(props.width);
    this.$marker.css('left', props.offsetX + 'px');
  }

  getWidthByItem($item) {
    let $widthSrc;

    if (this.params.selectors.listItemText) {
      $widthSrc = $item.find(this.params.selectors.listItemText);
    }
    else {
      $widthSrc = $item;
    }

    return $widthSrc.width();
  }

  resetMarkerProps() {
    let $activeItem = this.$container.find('.' + this.params.classes.listItemActive);

    if ($activeItem.length > 0) {
      this.$marker.width(this.getWidthByItem($activeItem));
      this.$marker.css('left', $activeItem.position().left + 'px');
    }
    else {
      this.hideMarker();
    }
  }

  hideMarker() {
    this.$marker.width(this.params.settings.defaultLineWidth);
    this.$marker.css({
      'left': -this.params.settings.defaultLineWidth
    });
  }
}

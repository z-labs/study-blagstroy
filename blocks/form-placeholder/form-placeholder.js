export class FormPlaceholder {
    constructor(params) {
        this.params = $.extend(true, {
                selectors: {
                    placeholder: '.form__placeholder',
                    control: '.form__control',
                    group: '.form-group'
                },
                classes: {
                    groupFilled: 'form-group_filled'
                }
            },
            params);

        this.init();
    }

    init () {
        let self = this;

        this.$placeholder = $(this.params.selectors.placeholder);
        this.$control = $(this.params.selectors.control);

        this.$control.each(function () {
            if ($(this).siblings(self.params.selectors.placeholder).length) {
                self.checkControlFilled($(this));
            }
        });

        this.attachEvents();
    }

    attachEvents () {
        this.placeholderClickHandler = this.placeholderClickHandler.bind(this);
        this.controlChangeHandler = this.controlChangeHandler.bind(this);

        this.$placeholder.on('click', this.placeholderClickHandler);
        this.$control.on('change', this.controlChangeHandler);
    }

    placeholderClickHandler (event) {
        $(event.currentTarget).closest(this.params.selectors.group).find(this.params.selectors.control).focus();
    }

    controlChangeHandler (event) {
        this.checkControlFilled($(event.currentTarget));
    }

    checkControlFilled (control) {
        if (control.val() === '') {
            control.closest(this.params.selectors.group).removeClass(this.params.classes.groupFilled);
        } else {
            control.closest(this.params.selectors.group).addClass(this.params.classes.groupFilled);
        }
    }
}



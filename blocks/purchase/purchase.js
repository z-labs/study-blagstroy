import './purchase.sass'
import scrollup from "../scrollup/scrollup";
import otherProjects from "../other-projects/other-projects";
import {buyWays} from "../buy-ways/buy-ways";
import '../how-buy/how-buy.mustache'
import './purchase__image.mustache'
import '../sample-documents/sample-documents.mustache'
import {InfinityNav} from "../infinite/infinite";


$(document).ready(function () {
  otherProjects();
  scrollup();
  buyWays();
  new InfinityNav;
});

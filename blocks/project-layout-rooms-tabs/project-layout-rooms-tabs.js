import './project-layout-rooms-tabs.mustache'
import {Slider} from "../slider/slider";

export const projectLayoutRoomsTabs = () => {
  new Slider({
    sliderContainerSelector: '.project-layout-rooms-tabs-container',
    wrapperClass: "project-layout-rooms-tabs",
    slideClass: "project-layout-rooms-tab",
    slidesPerView: 'auto',
    freeMode: true,
    on: {
      init: function() {
        this.update();
      }
    }
  });
};

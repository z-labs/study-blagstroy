import './infinite.mustache'

export class InfinityNav {
  constructor(params) {
    this.params = $.extend(
      true,
      {
        beforeCallback: null,
        afterCallback: null,
        loading: false,
        selectors: {
          item: null
        },
        classes: {
          itemShowed: null
        }
      },
      params
    );

    this.attachEvents();
    $(this.params.selectors.item).addClass(this.params.classes.itemShowed);
  }

  attachEvents() {
    $(window).on('load', this.onLoadHandler.bind(this));
    $(document).on('click', '#infinity-next-page', this.onClickNextInfinity.bind(this));
  }

  onLoadHandler() {
    $(this.params.selectors.item).addClass(this.params.classes.itemShowed);
  }

  onClickNextInfinity(e) {
    let link = $(e.currentTarget);

    if (!this.params.loading) {
      this.params.loading = true;

      link.text("Загрузка...");

      $.get(
        link.attr('href'),
        {is_ajax: 'y'},
        (data) => {
          var linkContainer = link.closest(".infinite");

          this.params.beforeCallback && this.params.beforeCallback()

          linkContainer.after(data);

          $(`${this.params.selectors.item}:not(.${this.params.classes.itemShowed})`)
            .addClass(this.params.classes.itemShowed);

          if (window.bLazy) {
            window.bLazy.revalidate();
          }

          linkContainer.remove();

          this.params.loading = false;

          this.params.afterCallback && this.params.afterCallback()
        }
      );
    }

    e.preventDefault();
  }
}

import './hot-deals-detail.sass'
import './hot-deals-detail-about-side.mustache'
import '../project-detail-first-screen/project-detail-first-screen-slider.mustache'
import {projectDetailSlider} from "../project-detail-slider/project-detail-slider";
import {projectDetailAbout} from "../project-detail-about/project-detail-about";
import hotDeals from "../hot-deals/hot-deals";
import {CollapsedBlock} from "../collapsed-block/collapsed-block";

$(document).ready(function () {
  projectDetailSlider();
  projectDetailAbout();
  hotDeals({
    withTabs: false
  });

  let characteristicsAccordion = new CollapsedBlock({
    selectors: {
      container: '.characteristics-table__collapsed-block-container',
      triggerText: '.collapsed-block-trigger__text'
    },
    settings: {
      triggerText: 'Все характеристики',
      triggerTextOpened: 'Свернуть'
    }
  });
});

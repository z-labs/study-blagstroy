'use strict';

export default function GoogleMetrics(params) {
  this.params = $.extend({
      ga: 'ga',
      selectors: {
        itemClick: '[data-ga-goals]'
      }
    },
    params
  );

  this.selectors = this.params.selectors;
  this.ga = this.params.ga;
  this.attachEvents();
};

GoogleMetrics.prototype = {
  constructor: window.GoogleMetrics,

  attachEvents: function () {
    this.onClickHandler = this.onClickHandler.bind(this);
    this.reachGoals = this.reachGoals.bind(this);
    this.isIsSetMetrics = this.isIsSetMetrics.bind(this);
    this.reachGoal = this.reachGoal.bind(this);

    $(document).on('click', this.selectors.itemClick, this.onClickHandler);
  },

  onClickHandler: function (e) {
    var goals = $(e.currentTarget).data('ga-goals');

    if (goals && goals.length) {
      this.reachGoals(goals);
    }
  },

  reachGoals: function (goals) {
    console.info(goals)
    console.info('send goals: ' + goals.map(goal => `${goal.eventCategory}:${goal.eventAction}:${goal.value}`).join(';'))
    if (this.isIsSetMetrics()) {
      $.each(goals, this.reachGoal);
    }
  },

  isIsSetMetrics: function () {
    return typeof window[this.ga] !== 'undefined';
  },

  reachGoal: function (key, value) {
    /* Нужно использовать не 'send' а 'gtmXXXXXXXXXX.send',
   * имя трекера *возможно* изменяется в каждой загрузке gtm*/
    var trackerName = window[this.ga].getAll()[0].get('name');

    window[this.ga](trackerName + '.send', 'event', value.eventCategory, value.eventAction, value.eventLabel);
  },
};

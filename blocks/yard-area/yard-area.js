import './yard-area.mustache'
import './yard-area-slider.mustache'
import {Slider} from "../slider/slider";

export const yardArea = () => {
  new Slider({
    sliderContainerSelector: '.yard-area-slider-container',
    wrapperClass: "project-detail-slider-wrapper",
    slideClass: "project-detail-slider-slide",
    slideThumbnail: ".project-detail-slider-thumbs-item",
    slideThumbnailActiveClass: "project-detail-slider-thumbs-item_active",
    loop: true,
    pagination: {
      el: '.project-detail-slider-pagination',
      type: 'fraction',
      formatFractionCurrent: function (number) {
        if (number <= 9) {
          number = '0' + number
        }
        return number
      },
      formatFractionTotal:	function(number) {
        if (number <= 9) {
          number = '0' + number
        }
        return number
      }
    },
    navigation: {
      nextEl: '.yard-area-slider__button-next',
      prevEl: '.yard-area-slider__button-prev',
    },
    on: {
      slideChangeTransitionEnd: function () {
        bLazy.revalidate();
      }
    }
  });
};

'use strict';

import $ from 'jquery';

import datepickerFactory from 'jquery-datepicker';
import datepickerRUFactory from 'jquery-datepicker/i18n/jquery.ui.datepicker-ru';

import 'timepicker/jquery.timepicker'
import 'timepicker/jquery.timepicker.css'

datepickerFactory($);
datepickerRUFactory($);

window.$ = window.jQuery = window.jquery = $;
import 'fonts/stylesheet.css'
import Blazy from 'blazy.min';
import YandexMetricsModule from 'yandex-metrics/yandex-metrics';
import GoogleMetricsModule from 'google-analytics/google-analytics';

window.YandexMetrics = YandexMetricsModule;
window.GoogleMetrics = GoogleMetricsModule;

window.bLazy = new Blazy({
    breakpoints: [
        {
            width: 767,
            src: 'data-src-md'
        },
        {
            width: 1366,
            src: 'data-src-lg'
        }
    ]
});

window.isDesktop = function() {
  return (window.innerWidth >= 1366);
};

window.isMobile = function() {
  return (window.innerWidth < 768);
}

import './common.sass';
import 'swiper/css/swiper.min.css'
import {header} from "../header/header";
import {footer} from "../footer/footer";
import {FormPlaceholder} from "../form-placeholder/form-placeholder";
import '../breadcrumbs/breadcrumbs.mustache';
import '../breadcrumbs/breadcrumbs-item.mustache';
import 'nanoscroller'
import {defaultButtons} from "../button/button";

$(document).ready(function () {
    header();
    footer();
    defaultButtons();
    new FormPlaceholder();

    $(window).on('load', function () {
        bLazy.revalidate();
    });

    window.initMetrics && window.initMetrics();
});

let browserUpdater = new ya.browserUpdater.init({
  theme: "yellow",
  lang: document.querySelector('html').getAttribute('lang'),
  exclusive: false,
  browsers: {
    chrome: 60,
    fx: 50,
    ie: 11
  },
  remember: true,
  rememberFor: 30,
  cookiePrefix: "yaBrowserUpdater",
  classNamePrefix: "ya-browser-updater",
  jsonpCallback: "yaBrowserUpdaterJSONPCallback",
  onStripeShow: null,
  onStripeHide: null
});

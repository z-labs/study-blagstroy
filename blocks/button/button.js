export const defaultButtons = () => {
  $('.button_filled-yellow').on('click', function(event) {
    if (!$(event.currentTarget).hasClass('click-animation')) {
      $(event.currentTarget).addClass('click-animation');

      setTimeout(function() {
        $(event.currentTarget).removeClass('click-animation')
      }, 800);
    }
  });
};

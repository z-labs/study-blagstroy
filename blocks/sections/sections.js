export class Sections {
    constructor(params) {
        this.params = $.extend(true, {
            container: null, // для более точной привязки обработчиков событий в случае одинаковых классов
            selectors: {
                select: '.sections-filter-combobox',
                tabs: '.sections-filter-tabs',
                tabItem: '.sections-filter-tabs-item',
                sectionsList: '.sections-list',
                tabItemTrigger: '.sections-filter-tabs-item__button'
            },
            classes: {
                sectionsListShow: 'sections-list_show',
                tabItemActive: 'sections-filter-tabs-item_active'
            },
            settings: {
                flexList: false,
                useInputTypeSelect: true,
                triggerPreventDefault: false
            },
            afterChangeTab: null
        }, params);

        this.init();
    }

    init() {
        this.attachEvents();
        this.applyBreakpointsSettings();
    }

    applyBreakpointsSettings() {
        for (let breakpoint in this.params.settings.breakpoints) {
            if (window.innerWidth >= Number.parseInt(breakpoint)) {
                this.extendParams(this.params.settings.breakpoints[breakpoint]);
            }
        }
    }

    extendParams(params) {
        this.params.settings = $.extend(true, this.params.settings, params);
    }

    attachEvents() {
        let instance = this;
        let $trigger;
        let $select;

        if (this.params.container) {
          $select = $(this.params.container).find(this.params.selectors.select);
        }
        else {
          $select = $(this.params.selectors.select);
        }

        if (this.params.settings.useInputTypeSelect) {
          $select.on('change', function() {
            instance.changeTab(this.value);
          });
        }

        if (this.params.container) {
          $trigger = $(this.params.container).find(this.params.selectors.tabItem + ' ' + this.params.selectors.tabItemTrigger);
        }
        else {
          $trigger = $(this.params.selectors.tabItem + ' ' + this.params.selectors.tabItemTrigger);
        }
        $trigger.on('click', function(event) {
            if (instance.params.settings.triggerPreventDefault) {
              event.preventDefault();
            }

            let tabId = $(this).closest(instance.params.selectors.tabItem).data('id');

            let $selectElement;
            if (instance.params.container) {
              $selectElement = $(instance.params.container).find(instance.params.selectors.select);
            }
            else {
              $selectElement = $(instance.params.selectors.select);
            }

            if (instance.params.settings.useInputTypeSelect) {
              $selectElement
                .val(tabId)
                .change();
            }
            else {
              instance.changeTab(tabId);
            }
        });
    }

    changeTab(tabId) {
        this.hideList();
        this.selectTab(tabId);
        this.showList(tabId);

        if (this.params.afterChangeTab) {
          let selectedTab = $(this.params.selectors.tabItem + `[data-id='${tabId}']`).get(0);
          this.params.afterChangeTab.call(selectedTab);
        }
    }

    hideList() {
      if (this.params.container) {
        $(this.params.container).find(this.params.selectors.sectionsList + '.' + this.params.classes.sectionsListShow)
          .removeClass(this.params.classes.sectionsListShow);
      }
      else {
        $(this.params.selectors.sectionsList + '.' + this.params.classes.sectionsListShow)
          .removeClass(this.params.classes.sectionsListShow);
      }
    }

    showList(id) {
      $(this.params.selectors.sectionsList + `[id=${id}]`)
        .addClass(this.params.classes.sectionsListShow)
      bLazy.revalidate();
    }

    selectTab(id) {
        let $tabsCollection;

        if (this.params.container) {
          $tabsCollection = $(this.params.container).find(this.params.selectors.tabItem);
        }
        else {
          $tabsCollection = $(this.params.selectors.tabItem);
        }

        $tabsCollection
            .removeClass(this.params.classes.tabItemActive)
            .find(this.params.selectors.tabItemTrigger)
            .removeAttr('disabled');

        $tabsCollection.filter(`[data-id='${id}']`)
            .addClass(this.params.classes.tabItemActive)
            .find(this.params.selectors.tabItemTrigger)
            .attr('disabled', 'disabled');
    }
}

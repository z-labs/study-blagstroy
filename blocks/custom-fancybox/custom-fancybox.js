export const customFancyboxSettings = {
  baseClass: 'custom-fancybox',
  title: false,
  padding: 0,
  btnTpl: {
    smallBtn: `<button type="button" data-fancybox-close class="button-preset custom-fancybox__close icon icon_close">
<svg class="custom-fancybox__close-icon" width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M19.4409 21.2089L3.23638 5.00434C2.82979 4.59775 2.89579 3.87179 3.38369 3.38388C3.8716 2.89598 4.59756 2.82998 5.00415 3.23657L21.2087 19.4411C21.6153 19.8477 21.5493 20.5736 21.0614 21.0616C20.5735 21.5495 19.8475 21.6155 19.4409 21.2089Z" fill="#342733"/>
<path d="M21.2089 5.00434L5.00434 21.2089C4.59775 21.6155 3.87179 21.5495 3.38388 21.0616C2.89598 20.5737 2.82998 19.8477 3.23657 19.4411L19.4411 3.23657C19.8477 2.82998 20.5736 2.89598 21.0615 3.38388C21.5495 3.87179 21.6155 4.59775 21.2089 5.00434Z" fill="#342733"/>
</svg>
       </button>`
  }
};


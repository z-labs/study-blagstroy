import './hot-deals.mustache'
import '../projects-list-rubric/projects-list-rubric1.mustache'
import Swiper from "swiper";
import {Dropdown} from "../dropdown/dropdown";

const optionsSlider = {
  slidesPerView: 1,
  spaceBetween: 10,
  pagination: {
    el: '.hot-deals__pagination',
    type: 'fraction',
    formatFractionCurrent: function (number) {
      if (number <= 9) {
        number = '0' + number
      }
      return number
    },
    formatFractionTotal: function (number) {
      if (number <= 9) {
        number = '0' + number
      }
      return number
    }
  },
  on: {
    slideChange: function () {
      bLazy.revalidate()
    }
  },
  breakpoints: {
    768: {
      slidesPerView: 'auto',
      pagination: {
        el: '.hot-deals__pagination',
        type: 'bullets',
        dynamicBullets: true,
        dynamicMainBullets: 1,
        clickable: true,
      }
    },
    1366: {
      slidesPerView: 4,
      spaceBetween: 24,
      pagination: false,
      navigation: {
        nextEl: '.hot-deals__button-next',
        prevEl: '.hot-deals__button-prev',
      },
    },
  },
}

let hotSlider;

const initSlider = period => {
  $('.hot-deals__slider').addClass('hot-deals__slider_hidden');

  hotSlider && hotSlider.destroy();

  $('.hot-deals__slide').each(function (index, item) {
    $(item).removeClass('swiper-slide-active').removeClass('swiper-slide-next');

    if (period) {
      if ($(item).data('value') === period) {
        $(item)
          .removeClass('non-swiper-slide')
          .addClass('swiper-slide')
      } else {
        $(item)
          .addClass('non-swiper-slide')
          .removeClass('swiper-slide')
      }
    }
  });

  setTimeout(function () {
    hotSlider = new Swiper('.hot-deals__slider', optionsSlider);

    if (needShowNavigation()) {
      $('.hot-deals__pagination').show();
      if (window.innerWidth >= 1366) {
        $('.hot-deals__button-prev').show();
        $('.hot-deals__button-next').show();
      }
    } else {
      $('.hot-deals__pagination').hide();
      $('.hot-deals__button-prev').hide();
      $('.hot-deals__button-next').hide();
    }

    $('.hot-deals__slider').removeClass('hot-deals__slider_hidden');

    setTimeout(function () {
      bLazy.revalidate();
    }, 400)
  }, 400);
};

const needShowNavigation = () => {
  let countSlides = $('.hot-deals__slide:not(.non-swiper-slide)').length;

  if ((window.innerWidth >= 768) && (window.innerWidth < 1366)) {
    return 2 < countSlides;
  }
  if (window.innerWidth >= 1366) {
    return optionsSlider.breakpoints['1366'].slidesPerView < countSlides;
  }
  else {
    return optionsSlider.slidesPerView < countSlides;
  }
};

export default (params) => {

  let options = $.extend(true, {
    withTabs: true
  }, params);

  if (options.withTabs) {
    new Dropdown({
      changeItemActivity: true,
      preventLinkClick: true,
      selectors: {
        dropdown: '.hot-deals__top .projects-list-rubric',
      },
      onInit: function (dropdown){
        if (dropdown.find('.dropdown-item_selected').length) {
          let period = dropdown.find('.dropdown-item_selected').data('value');
          initSlider(period);
        }
      },
      onChange: function (event) {
        if ($(event.currentTarget).data('value')) {
          initSlider($(event.currentTarget).data('value'));
        }

        let selectedItemText = $(event.currentTarget).find('.projects-list__dropdown-list-item-link').text();
        $(this.selectors.dropdown).find('.dropdown-current__text').text(selectedItemText);
      },
    });

    $('.hot-deals__top .projects-list__rubric-item').on('click', function (e) {
      e.preventDefault();

      let active = $('.hot-deals__top .projects-list__rubric-item_active').data('value');
      $('.hot-deals__top .projects-list__rubric-item_active').removeClass('projects-list__rubric-item_active');
      $(this).addClass('projects-list__rubric-item_active');
      let filter = $('.hot-deals__top .projects-list__rubric-item_active').data('value');

      if (active === filter) {
        return
      }

      initSlider(filter);
    })
  }
  else {
    let countSlides = $('.hot-deals__slide:not(.non-swiper-slide)').length;

    if (countSlides > getMinSlidesCount()) {
      initSlider();
    }
    else {
      $('.hot-deals__slider').addClass('hot-deals__slider_disabled');
    }
  }

  function getMinSlidesCount() {
    let count = 4;

    if (isMobile()) {
      count = 3;
    }

    return count;
  }
}

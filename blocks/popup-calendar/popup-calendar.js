import './popup-calendar.mustache'

export const popupCalendar = () => {
  $('.popup-calendar-buttons__item_close').on('click', function(event) {
    $.fancybox.close();
  });

  $('.popup-calendar-buttons__item_apply').on('click', function(event) {
    let $calendarInput = $('.feedback-form__control_type_datetime');

    let date = $(this)
      .closest('.popup-calendar')
      .find('.popup-calendar-datepicker')
      .datepicker('getDate');

    let formattedDate = $.datepicker.formatDate('dd.mm.yy', date);
    $calendarInput
      .val(formattedDate)
      .closest('.form-group')
      .addClass('form-group_filled');

    $.fancybox.close();
  });
};

"use strict";

import {customFancyboxSettings} from "../custom-fancybox/custom-fancybox"
import Inputmask from "inputmask"
import Mustache from 'mustache'
import 'fancybox/jquery.fancybox.min'
import 'fancybox/jquery.fancybox.min.css'
import './feedback-form.sass'
import 'feedback-success-message/feedback-success-message.mustache'
import 'form/form__control_type_text.mustache'
import 'form/form__control_type_textarea.mustache'
import 'form/form__control_type_file.mustache'
import 'form/form__control_type_hidden.mustache'
import 'form/form__control_type_datetime.mustache'
import 'svg/background-calendar.mustache'
import '../modal-form/modal-form.mustache'
import {popupCalendar} from "../popup-calendar/popup-calendar";

window.customFancyboxSettings = customFancyboxSettings;
window.Mustache = Mustache;

$(document).ready(function () {
    (function (window, document, $) {
        'use strict';

        var methods = {
            /**
             * Инициализация
             * @param options
             * @returns {methods}
             */
            init: function (options) {
                var data, _this;

                _this = this;
                data = $.extend({
                    group: '.feedback-form__group',
                    control: '.feedback-form__control',
                    control_required: '.feedback-form__control_required',

                    control_type_text: '.feedback-form__control_type_text',
                    control_type_textarea: '.feedback-form__control_type_textarea',
                    control_type_list: '.feedback-form__control_type_list',
                    control_type_radio: '.feedback-form__control_type_radio',
                    control_type_checkbox: '.feedback-form__control_type_checkbox',
                    control_type_file: '.feedback-form__control_type_file',
                    control_type_date: '.feedback-form__control_type_date',
                    control_type_datetime: '.feedback-form__control_type_datetime',
                    control_type_password: '.feedback-form__control_type_pswd',

                    pseudoFileControl: '.feedback-form__pseudo-file-control',
                    pseudoFilesList: '.feedback-form__files-list',
                    pseudoFilesItem: '.feedback-form__files-item',

                    groupNote: '.feedback-form__note',

                    control_type_date_disable_weekends: 'feedback-form__control_type_date_disable_weekends',
                    control_valid_email: '.feedback-form__control_valid_email',
                    control_valid_phone: '.feedback-form__control_valid_phone',
                    control_valid_textRu: '.feedback-form__control_valid_text-ru',
                    control_valid_phoneOrEmail: '.feedback-form__control_valid_phone_or_email',
                    control_valid_date: '.feedback-form__control_valid_date',

                    submitButton: '.feedback-form__submit',
                    submit_uploadProgress: '.feedback-form__submit_upload-progress',

                    has_error: '.feedback-form__group_has-error',
                    correct: '.feedback-form__group_correct',
                    uploadProgressHtml: 'Отправка ',

                    successMessageMustache: '#feedback-success-message',
                    anotherSuccessFunction: null,
                    messages: {
                        controlEmpty: 'Поле не заполнено!',
                        notValidRegExp: 'Ведено некорректное значение!',
                        notEqualToConf: 'Пароли не совпадают!',
                    }
                }, options);

                data.submitHtml = $(data.submitButton).find('.form__submit-content').html();

                if ($(data.control_valid_phone).length > 0) {
                    new Inputmask({
                        mask: '+7 (999) 999-99-99',
                        showMaskOnHover: false,
                    }).mask(data.control_valid_phone);
                }

                let currentDate = new Date();

                if ($(data.control_valid_date).length > 0) {
                    new Inputmask({
                        mask: '99.99.9999',
                        placeholder: '00.00.0000',
                        showMaskOnHover: false,
                    }).mask(data.control_valid_date);
                }

              if (_this.find(data.control_type_datetime).length > 0) {
                let disableWeekends = _this.find(data.control_type_datetime).hasClass(data.control_type_date_disable_weekends);

                _this.find(data.control_type_datetime)
                  .closest('.form-group_datetime')
                  .find('.popup-calendar-datepicker').datepicker({
                    dateFormat: 'dd.mm.yy',
                    hideScrollbar: false,
                    constraintInput: true,
                    defaultDate: null,
                    minDate: currentDate,
                    beforeShowDay: (disableWeekends ? $.datepicker.noWeekends : undefined)
                  });

                _this.find(data.control_type_datetime).on('click', function () {
                  $.fancybox.open($(this).closest('.form-group_datetime').find('.popup-calendar'), {
                    touch: false,
                    smallBtn: false
                  });
                });
              }

                return _this
                    .on('change', data.control, data, methods.validationControl)
                    .on('focus', data.control, data, (e) => {
                        $(e.currentTarget).closest(e.data.group).removeClass(e.data.has_error.substring(1))
                    })
                    .ajaxForm({
                        data: {
                            compid: _this.attr('id'),
                            action: 'submit'
                        },
                        beforeSubmit: function () {
                            return _this.feedbackForm('validationForm', {data: data});
                        },
                        uploadProgress: function (event, position, total, percentComplete) {
                            _this.find(data.submitButton).prop('disabled', true)
                                .find('.form__submit-content').text(data.uploadProgressHtml + percentComplete + '%...');
                        },
                        success: function (responseText, statusText, xhr, form) {
                            let view,
                                feedbackSuccessMustache,
                                hasError = (responseText.errors && responseText.errors.length)

                            _this.find(data.submitButton).prop('disabled', false).find('.form__submit-content').html(data.submitHtml);

                            if (!hasError) {//другие ошибки
                                feedbackSuccessMustache = $(data.successMessageMustache);

                                if (feedbackSuccessMustache.length) {
                                    view = {
                                        title: responseText.data.successMessageTitle,
                                        text: responseText.data.successMessage
                                    };
                                    $.fancybox.close();

                                    $.fancybox.open(
                                        Mustache.render(feedbackSuccessMustache.html(), view),
                                        customFancyboxSettings
                                    );
                                }

                                if (window.yandexMetricsInstance) {
                                    window.yandexMetricsInstance.reachGoals(responseText.data.ya_goals);
                                }

                                if (window.googleMetricsInstance) {
                                    window.googleMetricsInstance.reachGoals(responseText.data.ga_goals);
                                }

                                _this.clearForm();

                                _this.find('.form-group_filled').removeClass('form-group_filled')
                            } else {
                                if (responseText.errors) {
                                    console.error(responseText.errors);
                                }
                            }

                            if (!!data.anotherSuccessFunction && !hasError) {
                                data.anotherSuccessFunction(responseText);
                            }
                        }
                    })
                    .on('click', data.pseudoFileControl, data, methods.openFileDialog)
                    .on('change', data.control_type_file, data, methods.onChangeControlTypeFile);
            },
            /**
             *
             * @param e
             * @returns {boolean}
             */
            validationForm: function validationForm(e) {
                var formValid;

                formValid = true;

                $(this).find(e.data.control).each(function () {
                    if (!$(this).feedbackForm('validationControl', e)) {
                        formValid = false;
                    }
                });

                return formValid;
            },
            /**
             * todo: реализовать метод для остальных типов контролов
             * @param e
             */
            validationControl: function (e) {
                if ($(this).feedbackForm('isRequired', e)) {
                    switch ($(this).feedbackForm('getTypeControl', e)) {
                        default:
                        case 'text':
                        case 'textarea':
                            if ($(this).val() === '') {
                                $(this).feedbackForm('controlHasError', e, e.data.messages.controlEmpty);
                                return false;
                            } else {
                                // $(this).feedbackForm('controlValid', e);
                            }
                            break;
                    }
                }

                if ($(this).feedbackForm('getTypeControl', e) === 'password') {
                    if (!$(this).feedbackForm('checkPswdConf', e)) {
                        $(e.data.control_type_password)
                            .eq(0)
                            .feedbackForm('controlHasError', e, e.data.messages.notEqualToConf);

                        return false;
                    } else {
                        $(e.data.control_type_password).eq(0).feedbackForm('controlValid', e);
                    }
                }

                if (!$(this).feedbackForm('validForRegExp', e)) {
                    $(this).feedbackForm('controlHasError', e, e.data.messages.notValidRegExp);

                    return false;
                } else {
                    $(this).feedbackForm('controlValid', e);
                }

                return true;
            },

            controlValid: function (e) {
                let control = $(this);

                control
                    .parent(e.data.group)
                    .removeClass(e.data.has_error.substring(1))
                    .addClass(e.data.correct.substring(1));

                return control;
            },

            controlHasError: function (e, message) {
                let control = $(this);

                control
                    .parent(e.data.group)
                    .removeClass(e.data.correct.substring(1))
                    .addClass(e.data.has_error.substring(1))
                    .find(e.data.groupNote)
                    .text(message);

                return control;
            },

            checkPswdConf: function (e) {
                let passwords = $(e.data.control_type_password),
                    password = passwords.eq(0).val(),
                    passwordConfirm = passwords.eq(1).val();

                return (password !== '' && passwordConfirm === '')
                    || (password !== '' && passwordConfirm !== '' && password === passwordConfirm);
            },

            isRequired: function (e) {
                return $(this).hasClass(e.data.control_required.substring(1));
            },

            /**
             * todo: реализовать метод
             * @param e
             */
            getTypeControl: function (e) {
                if ($(this).hasClass(e.data.control_type_password.substring(1))) {
                    return 'password';
                }

                return 'text';
            },

            validForRegExp: function (e) {
                var pattern;

                if ($(this).length && $(this).val() !== '') {
                    if ($(this).hasClass(e.data.control_valid_email.substring(1))) {
                        pattern = /^([a-zA-Z0-9_\.\-])+@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    } else if ($(this).hasClass(e.data.control_valid_phone.substring(1))) {
                        pattern = /^[0-9() +-]+$/;
                    } else if ($(this).hasClass(e.data.control_valid_phoneOrEmail.substring(1))) {
                        pattern = /^([a-zA-Z0-9_\.\-])+@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})|^[0-9() +-]+$/;
                    } else if ($(this).hasClass(e.data.control_valid_textRu.substring(1))) {
                        pattern = /^[а-яА-Яёй -]+$/;
                    }
                    if (pattern) {
                        return pattern.test($(this).val());
                    }
                }
                return true;
            },

            openFileDialog: function (e) {
                $(this)
                    .closest(e.data.group)
                    .find(e.data.control_type_file)
                    .trigger('click');

                e.preventDefault();
            },

            onChangeControlTypeFile: function (e) {
                var file = $(this),
                    pseudoFileControl = $(this).closest(e.data.group).find(e.data.pseudoFileControl),
                    itemClass = e.data.pseudoFilesItem.substr(1),
                    imageType = /image.*/;

                if (file[0].files && file[0].files.length) {
                    var str = '';
                    for (let i = 0; i < file[0].files.length; i++) {
                        str += ', ' + file[0].files[i].name;
                    }
                    pseudoFileControl.find('.form-file-inner').text(str.slice(2));
                } else {
                    pseudoFileControl.find('.form-file-inner').html('<span class="form-file__text">Прикрепите файл</span> <span class="form-file__note">не более 10 mb</span>');
                }
            },

            onDeleteFileItem: function (e) {

            }
        };

        $.fn.feedbackForm = function (method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === "object" || !method) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('Метод с именем ' + method + ' не существует для jQuery.feedbackForm');
            }
        };

        $(window).on('load', function () {
            $('.feedback-form-link').fancybox(
                $.extend(
                    {},
                    customFancyboxSettings,
                    {
                        infobar: false,
                        toolbar: false
                    }
                )
            );

            if (window.initFeedback && window.initFeedback.length) {
                $.each(window.initFeedback, function (index, func) {
                    func();
                })
            }
        });
    }(window, document, jQuery))

    popupCalendar();
});

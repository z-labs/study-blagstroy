import '../announcements-content/announcements-content.mustache'
import '../announcements-filter/announcements-filter.mustache'
import '../announcements-filter/announcements-filter-combobox.mustache'
import '../announcements-filter/announcements-filter-tabs.mustache'
import '../announcements-list/announcements-list.mustache'
import '../announcements-list/announcements-list_actions.mustache'
import '../announcements-list/announcements-list_articles.mustache'
import '../announcements-list/announcements-list_news.mustache'
import '../announcements-card/announcements-card.mustache'
import '../announcements-card/announcements-card-2.mustache'
import '../announcements-card/announcements-card-3.mustache'
import {Sections} from "../sections/sections";
import {ListItemsHover} from "../list-items-hover/list-items-hover";
import {Dropdown} from "../dropdown/dropdown";

export const announcements = () => {
  let announcementsSections = new Sections({
    selectors: {
      sectionsList: '.announcements-lists-item',
      select: '.announcements-filter-combobox',
      tabItem: '.announcements-filter-tabs-item',
    },
    classes: {
      tabItemActive: 'sections-filter-tabs-item_active'
    }
  });

  new Dropdown({
    selectors: {
      dropdown: '.announcements-filter-combobox-wrapper',
    },
    changeItemActivity: true,
    preventLinkClick: false,
    onInit: (dropdown) => {
      let activeItem = dropdown.find('.dropdown-item_selected')

      if(activeItem.length) {
        dropdown.find('.dropdown-current__text').text(activeItem.text())
      }
    },
    onChange: (event) => {
      let currentItem = event.currentTarget;

      $(currentItem)
        .closest('.dropdown')
        .find('.dropdown-current__text').text($(currentItem).text());

      if (announcementsSections && announcementsSections.changeTab) {
        announcementsSections.changeTab($(currentItem).data('value'));
      }
    }
  });
};

import './building.mustache'

import {Dropdown} from '../dropdown/dropdown'
import Swiper from "swiper";

let needTouchMove = () => {
  let countSlides = $('.building__slide:not(.non-swiper-slide)').length;

  if ((window.innerWidth < 768) && (countSlides === 1)) {
    console.log(countSlides)
    return false;
  }
  if ((window.innerWidth >= 768) && (countSlides <= 3)) {
    console.log(countSlides)
    return false;
  }
  else {
    return true
  }
};

const optionsSlider = {
  slidesPerView: 1,
  spaceBetween: 10,
  centerInsufficientSlides: false,
  allowTouchMove: needTouchMove(),
  navigation: {
    nextEl: '.building__slider-button-next',
    prevEl: '.building__slider-button-prev',
  },
  pagination: {
    el: '.building__slider-pagination',
    type: 'bullets',
    clickable: true,
    dynamicBullets: true,
    dynamicMainBullets: 1,
  },
  on: {
    slideChange: function () {
      bLazy.revalidate()
    },
  },
  breakpoints: {
    768: {
      slidesPerView: 3,
      spaceBetween: 16,
    },
  },
};

let buildingSlider;

const initSlider = period => {
  $('.building__slider').addClass('building__slider_hidden');

  buildingSlider && buildingSlider.destroy();

  $('.building__slide').each(function (index, item) {
    $(item).removeClass('swiper-slide-active').removeClass('swiper-slide-next');

    if ($(item).data('value') === period) {
      $(item)
        .removeClass('non-swiper-slide')
        .addClass('swiper-slide')
    } else {
      $(item)
        .addClass('non-swiper-slide')
        .removeClass('swiper-slide')
    }
  });

  setTimeout(function () {
    buildingSlider = new Swiper('.building__slider', optionsSlider);

    if(needShowNavigation()) {
      $('.building__slider-pagination').show();

      if (isDesktop()) {
        $('.building__slider-button-prev').show();
        $('.building__slider-button-next').show();
      }
    } else {
      $('.building__slider-pagination').hide();
      $('.building__slider-button-prev').hide();
      $('.building__slider-button-next').hide();
    }

    $('.building__slider').removeClass('building__slider_hidden');

    setTimeout(function () {
      bLazy.revalidate();
    }, 400)
  }, 400);
};

const needShowNavigation = () => {
  let countSlides = $('.building__slide:not(.non-swiper-slide)').length;

  if (window.innerWidth >= 768) {
    return optionsSlider.breakpoints['768'].slidesPerView < countSlides;
  } else {
    return optionsSlider.slidesPerView < countSlides;
  }
};



export default () => {
  if ($('.building__dropdown').length > 0) {
    new Dropdown({
      changeItemActivity: true,
      preventLinkClick: true,
      selectors: {
        dropdown: '.building__dropdown',
      },
      onInit: function (dropdown){
        if (dropdown.find('.dropdown-item_selected').length) {
          let period = dropdown.find('.dropdown-item_selected').data('value');
          initSlider(period);
        }
      },
      onChange: function (event) {
        if ($(event.currentTarget).data('value')) {
          initSlider($(event.currentTarget).data('value'));
        }
      },
    });
  }
  else {
    let period = $('.building__quarter').data('value');
    initSlider(period);
  }
}

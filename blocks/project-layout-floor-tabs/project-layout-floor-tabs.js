import './project-layout-floor-tabs.mustache'
import {Slider} from "../slider/slider";

export const projectLayoutTabs = () => {

  new Slider({
    sliderContainerSelector: '.project-layout-floor-tabs-container',
    wrapperClass: "project-layout-floor-tabs",
    slideClass: "project-layout-floor-tab",
    slidesPerView: 'auto',
    freeMode: true,
    on: {
      init: function() {
        this.update();
      }
    }
  });
};

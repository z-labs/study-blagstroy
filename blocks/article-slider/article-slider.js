import Swiper from 'swiper';

import './article-slider.mustache'

export default () => {

  $('.article-slider__container').each((index, element) => {
    let mySwiper = new Swiper(element, {
      spaceBetween: 10,
      loop: true,
      pagination: {
        el: element.closest('.article-slider').querySelector('.article-slider__pagination'),
        type: 'fraction',
        formatFractionCurrent: function (number) {
          if (number <= 9) {
            number = '0' + number
          }
          return number
        },
        formatFractionTotal:	function(number) {
          if (number <= 9) {
            number = '0' + number
          }
          return number
        }
      },
      navigation: {
        nextEl: element.closest('.article-slider').querySelector('.article-slider__button-next'),
        prevEl: element.closest('.article-slider').querySelector('.article-slider__button-prev'),
      },
      on: {
        slideChange: function () {
          bLazy.revalidate()
        }
      }
    });
  })
}

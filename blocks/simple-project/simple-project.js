import './simple-project.sass'
import '../simple-project-main/simple-project-main.mustache'
import '../simple-project-main/simple-project-main__image.mustache'
import '../simple-project-main/simple-project-main__video.mustache'
import '../consultation-form/consultation-form.mustache'
import '../article-form/article-form.mustache'
import articleSlider from '../article-slider/article-slider'
import scrollup from '../scrollup/scrollup'
import otherProjects from "../other-projects/other-projects";
import {articleText} from "../article/article-text";

$(document).ready(function () {
  articleSlider();
  articleText();
  otherProjects();
  scrollup();
});

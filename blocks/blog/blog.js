import './blog.sass'
import '../blog-items/blog-items.mustache'
import '../blog-item/blog-item.mustache'
import '../blog-item/blog-item2.mustache'
import '../blog-item/blog-item_only-text.mustache'
import '../blog-item/blog-item_all-text.mustache'
import '../blog-credit/blog-credit.mustache'
import '../consultation-form/consultation-form_gray.mustache'
import '../pages/pages.mustache'
import {indexText} from "../index-text/index-text";
import rubric from "../rubric/rubric"
import positionAwareButton from "../position-aware-button/position-aware-button"

$(document).ready(function () {
  indexText();
  rubric();
  positionAwareButton();
});

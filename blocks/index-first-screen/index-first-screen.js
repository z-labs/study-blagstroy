import './index-first-screen.mustache';
import './index-first-screen-slide-1.mustache'
import {Slider} from "../slider/slider";

export const indexFirstScreen = () => {
    let $slides = $('.index-first-screen-slide');

    if ($slides.length < 2) {
      $slides.addClass('index-first-screen-slide_animate');
      $('.index-first-screen-slider').addClass('index-first-screen-slider_disabled');
      return;
    }

    new Slider({
        sliderContainerSelector: '.index-first-screen-slider',
        wrapperClass: "index-first-screen-slides",
        slideClass: "index-first-screen-slide",
        loop: true,
        navigation: {
            prevEl: '.swiper-pagination-arrow_prev',
            nextEl: '.swiper-pagination-arrow_next'
        },
        pagination: {
            el: '.index-first-screen-pagination',
            clickable: true
        },
        on: {
          slideChangeTransitionEnd: function() {
            let $activeSlide = $('.index-first-screen-slide.swiper-slide-active');

            $slides.removeClass('index-first-screen-slide_animate');
            $activeSlide.addClass('index-first-screen-slide_animate');
          }
        }
    });
};

import Swiper from 'swiper';
import "./slider__arrow-prev.mustache"
import "./slider__arrow-next.mustache"
import "./slider__pagination-arrow__prev.mustache"
import "./slider__pagination-arrow__next.mustache"

export class Slider {
    constructor(params) {
        this.attachEvents = this.attachEvents.bind(this)
        this.thumbnailsEvents = this.thumbnailsEvents.bind(this)
        this.changeThumbnail = this.changeThumbnail.bind(this)
        this.resetThumbnailsMarker = this.resetThumbnailsMarker.bind(this)

        this.params = $.extend(true, {
                sliderContainerSelector: '.swiper-container',
                thumbShowMoreLink: '.gallery-show-more-link',
                thumbnailsCount: 9
            },
            params);

        this.instanceWrapper = $(params.sliderContainerSelector).parent();
        this._slidesCount = this.instanceWrapper.find('.project-detail-slider-slide:not(.swiper-slide-duplicate)').length;

        this.params.thumbnailsCount = Math.min(this.params.thumbnailsCount, this._slidesCount);

        if (this.params.slideThumbnail) {
          this.$thumbnailsMarker = this.instanceWrapper.find(this.params.thumbnailsMarker);
        }

        if (this.params.slideThumbnail) {
            this.params.on = $.extend(true, this.params.on, {
                slideChange: () => {
                    if (this.instance) {
                      if (this.instance.activeIndex >= 0 && this.instance.activeIndex <= this.params.thumbnailsCount) {
                        this.changeThumbnail(this.instance.activeIndex - 1);
                      }
                      else {
                        if (this.instance.activeIndex > this._slidesCount) {
                          this.changeThumbnail(0);
                        }
                      }
                    }
                    else {
                      this.changeThumbnail(0);
                    }
                }
            })
        }

        this.init(this.params);
    }

    init(params) {
        this.instance = new Swiper(params.sliderContainerSelector, params);

        if (this.params.slideThumbnail) {
          this.resetThumbnailsMarker();
        }

        this.attachEvents();
    }

    attachEvents() {
        if (this.params.slideThumbnail) {
            this.thumbnailsEvents();
        }
    }

    thumbnailsEvents() {
        this.instanceWrapper.on('click', this.params.slideThumbnail + `:not(${this.params.thumbShowMoreLink})`, event => {
            let index = this.instanceWrapper.find(this.params.slideThumbnail).index(event.currentTarget);

            this.instance.slideTo(index + 1);
        });

        this.instanceWrapper.find(this.params.thumbShowMoreLink).on('click', event => {
            event.preventDefault();

            // -2, т.к. слайдер зацикленный и нумерация слайдов начинается с нуля
            let slideIndex = $(event.currentTarget).data('index') - 2;
            let $fancyboxStartSlide = this.instanceWrapper.find(`.swiper-slide[data-swiper-slide-index=${slideIndex}]`);

            $fancyboxStartSlide.find('a').trigger('click');
        })

        this.instanceWrapper.find(this.params.slideThumbnail).hover(
          (function(event) {
            let currentMarginLeft = Number.parseInt($(event.currentTarget).css('margin-left').replace('px', ''));
            this.setThumbnailsMarkerPos($(event.currentTarget).position().left + currentMarginLeft);
          }).bind(this),

          (function() {
              this.resetThumbnailsMarker();
          }).bind(this)
        );
    }

    resetThumbnailsMarker() {
      if (this.params.thumbnailsMarker) {
        let $activeThumbnail = this.instanceWrapper.find('.' + this.params.slideThumbnailActiveClass);

        let currentMarginLeft = Number.parseInt($activeThumbnail.css('margin-left').replace('px', ''));
        let markXPos = $activeThumbnail.position().left;
        this.setThumbnailsMarkerPos(markXPos + currentMarginLeft);
      }
    }

    setThumbnailsMarkerPos(xpos) {
      this.$thumbnailsMarker.css({
        'margin-left': xpos
      })
    }

    changeThumbnail(index = 0) {
        this.instanceWrapper.find(this.params.slideThumbnail)
            .removeClass(this.params.slideThumbnailActiveClass)
            .eq(index)
            .addClass(this.params.slideThumbnailActiveClass);

        this.resetThumbnailsMarker();
    }
}

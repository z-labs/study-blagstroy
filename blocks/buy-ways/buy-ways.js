import './buy-ways.mustache'
import '../buy-ways-list-item/buy-ways-list-item.mustache'
import {Slider} from "../slider/slider";

export const buyWays = () => {
  let minSlidesCount = 4;

  if (!isDesktop()) {
    minSlidesCount = 2;
    if (isMobile()) {
      minSlidesCount = 1;
    }
  }


  if ($('.buy-ways-list-item').length > minSlidesCount) {
    new Slider({
      sliderContainerSelector: '.buy-ways-list',
      wrapperClass: "buy-ways-list-wrapper",
      slideClass: "buy-ways-list-item",
      loop: true,
      pagination: {
        el: '.buy-ways-list-pagination',
        clickable: true
      },
      navigation: {
        prevEl: '.buy-ways-list-arrows__prev',
        nextEl: '.buy-ways-list-arrows__next'
      },
      breakpoints: {
        768: {
          slidesPerView: 'auto',
          loop: false
        }
      }
    });
  }
  else {
   $('.buy-ways-list').addClass('buy-ways-list_slider-disabled');
   $('.buy-ways-list-arrows').addClass('buy-ways-list-arrows_hidden');
  }
};

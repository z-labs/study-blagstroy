import './company-slider-second.mustache'
import Swiper from 'swiper'

export default () => {

  let slides = $('.company-slider-second__slide').length;

  let mySwiper = new Swiper('.company-slider-second', {
    spaceBetween: 10,
    slidesPerView: 1,
    pagination: {
      el: '.company-slider-second__pagination',
      type: 'bullets',
      dynamicBullets: slides > 3 ? true : false,
      clickable: true,
    },
    on: {
      slideChange: function () {
        bLazy.revalidate()
      },
    },
  })
}

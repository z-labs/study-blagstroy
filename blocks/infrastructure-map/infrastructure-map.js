import './infrastructure-map.mustache'
import {demo} from './demo'
import {Dropdown} from "../dropdown/dropdown";

let map
if (process.env.NODE_ENV === 'development' && !window.infrastructureData) {
  window.infrastructureData = demo
}

export const infrastructureMap = () => {
  loadMap()
  new Dropdown({
    selectors: {
      dropdown: '.infrastructure-map__dropdown',
    },
    changeItemActivity: false,
    preventLinkClick: true,
    multiple: true,
    onChange: filter
  })

  $(document).on('click', '.infrastructure-map__category', filter)
}

const filter = (event) => {
  event.preventDefault()

  let category = event.currentTarget.getAttribute('data-category')
  document.querySelector('.infrastructure-map__category[data-category="' + category + '"').classList.toggle('infrastructure-map__category_active')
  document.querySelector('.infrastructure-map__dropdown-item-link[data-category="' + category + '"').classList.toggle('infrastructure-map__dropdown-item-link_active')

  placeItems()
}

const loadMap = () => {
  var script = document.createElement('script')

  script.src = 'https://api-maps.yandex.ru/2.1/?apikey=2645f180-5285-43fe-bacc-aad4cec933f6&lang=ru_RU';
  (document.head || document.documentElement).appendChild(script);
  script.onload = function () {
    this.parentNode.removeChild(script);

    if (window.infrastructureData && $('#infrastructure-map').length) {
      ymaps.ready(initMap);
    }
  }
}

const initMap = () => {
  map = new ymaps.Map(
    'infrastructure-map',
    {
      center: window.infrastructureData.item.coords,
      zoom: 16,
      controls: ['zoomControl']
    },
    {
      yandexMapDisablePoiInteractivity: false
    }
  );
  map.behaviors.disable('scrollZoom')

  placeItems()
}

const placeItems = () => {
  map.geoObjects.removeAll()

  let categoriesLinks = document.querySelectorAll('.infrastructure-map__category_active')
  let categories = []

  for (let i = 0; i < categoriesLinks.length; i++) {
    categories.push(categoriesLinks[i].getAttribute('data-category'))
  }

  placeBalloons(window.infrastructureData.balloons.filter(balloon => categories.includes(balloon.category)))
  placeItem(window.infrastructureData.item, document.querySelector('[data-template-id=balloon_item]'))
}

const placeItem = (item, iconLayoutTemplate) => {
  let options = {}

  if (item.content) {
    options.balloonContentHeader = item.content.header
    options.balloonContentBody = item.content.body
  }

  let properties = {
    iconImageSize: [35, 35],
    iconShape: {
      type: 'Rectangle',
      coordinates: [
        [0, 0],
        [35, 35]
      ]
    },
    hideIconOnBalloonOpen: false,
    iconLayout: ymaps.templateLayoutFactory.createClass(Mustache.render(iconLayoutTemplate.innerHTML, item))
  }

  let Placemark = new ymaps.Placemark(
    item['coords'],
    options,
    properties
  );

  map.geoObjects.add(Placemark);
}

const placeBalloons = balloons => {
  for (let i = 0; i < balloons.length; i++) {
    placeItem(
      balloons[i],
      document.querySelector(`[data-template-id=balloon_${balloons[i].category}]`),
      document.querySelector('[data-template-id=balloon_content]')
    )
  }
}

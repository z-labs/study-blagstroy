import './page-menu-navigation.mustache'
import {ListItemsHover} from "../list-items-hover/list-items-hover";

export const pageMenuNavigation = () => {
  let pageNavigationLine;

  if (isDesktop()) {
    pageNavigationLine = new ListItemsHover({
      selectors: {
        container: '.page-menu-navigation-wrapper',
        listItem: '.page-menu-navigation-item',
        listItemText: '.page-menu-navigation-item__link-text'
      },
      classes: {
        listItemActive: 'page-menu-navigation-item_active'
      }
    });
  }

  $('.page-menu-navigation-item__link').on('click', function(event) {
    event.preventDefault();

    let href = $(event.currentTarget).attr('href');
    let topOffset = $('.header').height() + $('.project-detail__navigation').height();

    if (href === '#') return;

    if ($(href).length > 0) {
      let destinationPos = $(href).offset().top - topOffset;

      $("html, body").animate({
        scrollTop: destinationPos
      },500);
    }
  });

  let topOffset = $('.header').height() + $('.project-detail__navigation').height();

  $(window).on('scroll', function() {
    let currentSection = 'project-about';

    $('.page-screen').each(function(index, element) {
      if ($(window).scrollTop() + topOffset > $(element).offset().top) {
        currentSection = $(element).attr('id');
      }
    });

    let $currentItemLink = $(`.page-menu-navigation-item__link[href="#${currentSection}"]`);
    let $currentItem = $currentItemLink.closest('.page-menu-navigation-item');

    $('.page-menu-navigation-item').removeClass('page-menu-navigation-item_active');

    if (!$currentItem.hasClass('page-menu-navigation-item_active')) {
      $currentItem.addClass('page-menu-navigation-item_active');

      if (pageNavigationLine) {
        pageNavigationLine.resetMarkerProps();
      }
    }

  });
};

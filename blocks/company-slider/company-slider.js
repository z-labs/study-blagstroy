import './company-slider.mustache'
import Swiper from 'swiper'

export default () => {
  let mySwiper = new Swiper('.company-slider__container', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    on: {
      slideChange: function () {
        bLazy.revalidate()
      },
    },
    breakpoints: {
      768: {
        slidesPerView: 3,
      },
    }
  })
}

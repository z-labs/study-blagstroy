#!/bin/sh

cd /home/p/panters/blagstroy/production

git checkout production;
git pull;
composer install;
./vendor/bin/jedi env:init prod;
./vendor/bin/jedi cache:clear;
php migrator migrate;
bash build.sh;

echo "$(date)";

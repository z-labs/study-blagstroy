<?php

use ZLabs\DeferredFunctions;
use ZLabs\Helpers\BxFrontendChecker;
use ZLabs\Frontend\MustacheSingleton;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


$mustache = MustacheSingleton::getInstance();
$isMainPage = CSite::InDir('/index.php'); ?>

<!doctype html>
<html lang="<?= ZLabs\App::$context->getLanguage(); ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php ZLabs\App::CMain()->ShowTitle(); ?></title>
    <?
   /* echo DeferredFunctions\Asset\InlineStyles::show();
    echo DeferredFunctions\Asset\InlineJs::show();

    ZLabs\App::CMain()->ShowMeta('robots', false);
    ZLabs\App::CMain()->ShowMeta('keywords', false);
    ZLabs\App::CMain()->ShowMeta('description', false);
    ZLabs\App::CMain()->ShowLink('canonical', null);
    ZLabs\App::CMain()->AddBufferContent([&ZLabs\App::$cmain, 'GetHeadStrings'], 'BEFORE_CSS');

    if ((new BxFrontendChecker)->needAddFrontend()) {
        ZLabs\App::CMain()->ShowHeadStrings();
        ZLabs\App::CMain()->ShowHeadScripts();
        ZLabs\App::CMain()->SetPageProperty('needBxStyles', 'Y');
    }*/
    ?>

    <style>
      .page #bx-panel {
        position: relative !important;
        width: 100% !important;
        top: 0;
      }
    </style>
</head>
<body class="page">
    <?php ZLabs\App::CMain()->ShowPanel(); ?>
    <div class="page-content">
        <?php ZLabs\App::Include('header/template');?>
        <main class="<?php /*DeferredFunctions\MainClass::show() */?>">
            <?php /*DeferredFunctions\ShowNavChain::show();
            DeferredFunctions\ShowContainer::showHead();
            DeferredFunctions\InnerTitle::show();*/?>
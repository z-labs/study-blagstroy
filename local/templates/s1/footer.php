<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 */

use ZLabs\DeferredFunctions;

/*DeferredFunctions\ShowContainer::showFooter(); */?>

    </main>
</div>

<?php /*ZLabs\App::Include('footer/template');*/ ?>

<?php
/*
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "section",
    array(
        "AREA_FILE_RECURSIVE" => "Y",    // Рекурсивное подключение включаемых областей разделов
        "AREA_FILE_SHOW" => "sect",    // Показывать включаемую область
        "AREA_FILE_SUFFIX" => "form_for_sect",    // Суффикс имени файла включаемой области
        "EDIT_TEMPLATE" => "standard.php",    // Шаблон области по умолчанию
        "COMPONENT_TEMPLATE" => ".default"
    ),
    false
);
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "page",
    array(
        "AREA_FILE_RECURSIVE" => "N",
        "AREA_FILE_SHOW" => "page",    // Показывать включаемую область
        "AREA_FILE_SUFFIX" => "form_for_page",    // Суффикс имени файла включаемой области
        "EDIT_TEMPLATE" => "standard.php",    // Шаблон области по умолчанию
        "COMPONENT_TEMPLATE" => ".default"
    ),
    false
);
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "global",
    array(
        "AREA_FILE_RECURSIVE" => "Y",
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "",
        "EDIT_TEMPLATE" => "standard.php",
        "COMPONENT_TEMPLATE" => "global",
        "PATH" => "/local/include_areas/template/global_forms.php"
    ),
    false
);
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    array(
        "AREA_FILE_RECURSIVE" => "Y",
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "form_for_sect",
        "EDIT_TEMPLATE" => "standard.php",
        "COMPONENT_TEMPLATE" => "global",
        "PATH" => "/local/include_areas/internal/feedback-form-success.php"
    ),
    false
);

DeferredFunctions\Asset\DeferredStyles::show();
DeferredFunctions\Asset\DeferredJs::show();
DeferredFunctions\Asset\AsyncJs::show();

$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "social-head",
    array(
        "AREA_FILE_RECURSIVE" => "Y",
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "form_for_sect",
        "EDIT_TEMPLATE" => "standard.php",
        "COMPONENT_TEMPLATE" => "social-head",
        "PATH" => "/local/include_areas/external/external-services-in-head.php"
    ),
    false
);
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "social",
    array(
        "AREA_FILE_RECURSIVE" => "Y",
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "form_for_sect",
        "EDIT_TEMPLATE" => "standard.php",
        "COMPONENT_TEMPLATE" => "social",
        "PATH" => "/local/include_areas/external/external-services.php"
    ),
    false
);*/
echo '</body></html>';

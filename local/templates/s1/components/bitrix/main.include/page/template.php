<?php

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$this->setFrameMode(true);

if ($APPLICATION->GetShowIncludeAreas()) :
    echo '<div class="system-info"><div class="container">';
    ?>
    <h2>Формы обратной связи страницы (доступны только на этой странице)</h2>
    <?php
endif;

if ($arResult["FILE"] <> '') {
    include($arResult["FILE"]);
}

if ($APPLICATION->GetShowIncludeAreas()) {
    echo '</div></div>';
}

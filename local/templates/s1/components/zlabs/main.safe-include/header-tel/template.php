<?php

/** @var array $arResult */

use ZLabs\Frontend\MustacheSingleton;

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$mustache = MustacheSingleton::getInstance();

?>

<?= $mustache->render('contact-phone', $arResult['context']) ?>
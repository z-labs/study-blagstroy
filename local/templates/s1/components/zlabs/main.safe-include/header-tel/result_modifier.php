<?php

/**
 * @var $arParams
 * @var $arResult
*/

use ZLabs\BxMustache\Link;
use ZLabs\Helpers\SocialLinksFormatter;

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

if (!empty($arParams['TEL'])) {
    $tels = collect($arParams['TEL']);

    $arResult['context']['links'] = $tels->map(function ($arTel) {
        $tel = new Link;

        $tel->href = SocialLinksFormatter::getTelHref($arTel);
        $tel->text = $arTel;

        return $tel;
    });

    $arResult['context']['title'] = $arParams['TITLE'];
}

<?php defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$arTemplateParameters = [
    "TEL" => [
        'NAME' => 'Номер телефона',
        'PARENT' => 'BASE',
        'COLS' => 35,
        'TYPE' => 'STRING',
        'MULTIPLE' => 'Y'
    ],
    "TITLE" => [
        'NAME' => 'Заголовок',
        'PARENT' => 'BASE',
        'COLS' => 35,
        'TYPE' => 'STRING'
    ],
];


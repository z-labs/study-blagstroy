<!-- Global site tag (gtag.js) - Google Analytics -->
<script data-move-skiping="true" async src="https://www.googletagmanager.com/gtag/js?id=UA-162016581-1"></script>
<script data-move-skiping="true">
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-162016581-1');
</script>
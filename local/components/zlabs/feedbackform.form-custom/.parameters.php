<?php

/**
 * @var array $arCurrentValues
 */

use Bitrix\Main\Localization\Loc;
use ZLabs\Components\Feedback\Fields\Types\CustomTypes;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!\Bitrix\Main\Loader::includeModule('bex.bbc')) {
    return false;
}

if (!\Bitrix\Main\Loader::includeModule('zlabs.feedbackform')) {
    return false;
}

Loc::loadMessages(__FILE__);

try {
    $parameters = new \ZLabs\FeedbackForm\Components\Parameters\BaseForm(new CustomTypes);

    $parameters->addParameter('USER_CONSENT', []);

    $arComponentParameters = $parameters->getParameters($arCurrentValues);
} catch (Exception $e) {
    ShowError($e->getMessage());
}
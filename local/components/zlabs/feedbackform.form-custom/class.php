<?php

namespace ZLabs\FeedbackForm\Components;

use Bitrix\Main\Loader;
use ZLabs\Components\Feedback\FeedbackFormCustom;

/**
 * @global \CMain $APPLICATION
 */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!Loader::includeModule('bex.bbc')) {
    return false;
}

if (!Loader::includeModule('zlabs.feedbackform')) {
    return false;
}

class FeedbackForm extends FeedbackFormCustom
{
}

#!/bin/sh

cd /home/p/panters/blagstroy/frontend

git checkout frontend;
git pull;
composer install;
bash build.sh;

echo "$(date)";

<?php

namespace ZLabs\DeferredFunctions;

use ZLabs\BxMustache;
use ZLabs\Frontend\MustacheSingleton;

class InnerTitle extends DeferredFunctionAbstract
{
    const TITLE_PROPERTY_CODE = 'not_show_h1';

    public static function get(...$params)
    {
        $showTitle = 'Y' !== $GLOBALS['APPLICATION']->GetProperty(static::TITLE_PROPERTY_CODE, '');

        if ($showTitle) {
            return
                '<h1 class="title">' . $GLOBALS['APPLICATION']->GetTitle() . '</h1>';
        }
        return '';
    }

}

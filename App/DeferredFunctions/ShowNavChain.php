<?php

namespace ZLabs\DeferredFunctions;

use ZLabs\BxMustache;
use ZLabs\Frontend\MustacheSingleton;

class ShowNavChain extends DeferredFunctionAbstract
{
    const PROPERTY_CODE = 'not_show_nav_chain';

    public static function get(...$params)
    {
        return 'Y' !== $GLOBALS['APPLICATION']->GetProperty(static::PROPERTY_CODE, '')
            ? '<div class="container">' . $GLOBALS['APPLICATION']->GetNavChain(...$params) . '</div>' : '';
    }

}

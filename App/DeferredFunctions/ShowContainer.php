<?php

namespace ZLabs\DeferredFunctions;

use ZLabs\BxMustache;
use ZLabs\Frontend\MustacheSingleton;

class ShowContainer extends DeferredFunctionAbstract
{
    const CONTAINER_PROPERTY_CODE = 'not_show_container';

    public static function get(...$params)
    {
        return $GLOBALS['APPLICATION']->GetProperty(static::CONTAINER_PROPERTY_CODE) === 'Y' ?
            '' :
            '<div class="container">';
    }

    public static function getFooter(...$params)
    {
        return $GLOBALS['APPLICATION']->GetProperty(static::CONTAINER_PROPERTY_CODE) === 'Y' ?
            '' :
            '</div>';
    }

    public static function showHead(...$params)
    {
        return $GLOBALS['APPLICATION']->AddBufferContent([static::class, 'get'], ...$params);
    }

    public static function showFooter(...$params)
    {
        return $GLOBALS['APPLICATION']->AddBufferContent([static::class, 'getFooter'], ...$params);
    }

}

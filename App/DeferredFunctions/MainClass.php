<?php

namespace ZLabs\DeferredFunctions;

use ZLabs\BxMustache;
use ZLabs\Frontend\MustacheSingleton;

class MainClass extends DeferredFunctionAbstract
{
    const TITLE_PROPERTY_CODE = 'main_class';

    public static function get(...$params)
    {
        return $GLOBALS['APPLICATION']->GetProperty(static::TITLE_PROPERTY_CODE) ?
            ' ' . $GLOBALS['APPLICATION']->GetProperty(static::TITLE_PROPERTY_CODE) :
            null;
    }

}

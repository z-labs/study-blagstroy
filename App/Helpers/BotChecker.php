<?php

namespace ZLabs\Helpers;

class BotChecker
{
    const DEFAULT_AGENTS = [
        'Google',
        'Yahoo',
        'Rambler',
        'Bot',
        'Yandex',
        'Spider',
        'Snoopy',
        'Crawler',
        'Finder',
        'Mail',
        'curl',
        'Chrome-Lighthouse'
    ];

    protected $userAgent;
    protected $arAgents = [];

    public function __construct($userAgent = null, $arAgents = null)
    {
        $this->userAgent = $userAgent ?: $_SERVER['HTTP_USER_AGENT'];
        $this->arAgents = $arAgents ?: static::DEFAULT_AGENTS;
    }

    public function check()
    {
        return preg_match("~(" . implode('|', $this->arAgents) . ")~i", $this->userAgent);
    }

    public function checkGooglePageInsights()
    {
        return preg_match("~(" . implode('|', ['Chrome-Lighthouse']) . ")~i", $this->userAgent);
    }
}

<?php


namespace ZLabs\Helpers;


class SocialLinksFormatter
{
    static public function getTelHref($text) : string {
        return 'tel:+7' . self::getHref($text);
    }

    static public function getWAHref($text) : string {
        return 'https://wa.me/+7' . self::getHref($text);
    }

    static public function getMailHref($text) : string {
        return 'mailto:' . $text;
    }

    static public function getYTHref($text) : string {
        return 'https://www.youtube.com/channel/' . $text . '/';
    }

    static public function getIgHref($text) : string {
        return 'https://www.instagram.com/' . $text . '/';
    }

    static public function getFbHref($text) : string {
        return 'https://www.facebook.com/' . $text . '/';
    }

    static public function getPinHref($text) : string {
        return 'https://www.pinterest.ru/' . $text . '/';
    }

    static public function getDzenHref($text) : string {
        return 'https://zen.yandex.ru/' . $text . '/';
    }

    static public function getTgHref($text) : string {
        return 'https://t.me/' . $text . '/';
    }

    static public function getVKHref($text) : string {
        return 'https://vk.com/' . $text . '/';
    }

    static public function getHref($text) : string {
        return substr(preg_replace('/\D/', '', $text), 1);
    }
}
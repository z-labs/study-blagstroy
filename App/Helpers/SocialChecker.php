<?php

namespace ZLabs\Helpers;

use Bitrix\Main\Context;

class SocialChecker
{
    public function check()
    {
        global $USER;
        $server = Context::getCurrent()->getServer();

        return !(new BotChecker())->check() && !$USER->IsAdmin() && $server->get('REMOTE_ADDR') !== '127.0.0.1';
    }
}

<?php

namespace ZLabs\BxMustache\MainSlider;

use ZLabs\BxMustache\HermitageTrait;
use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\Link;
use ZLabs\BxMustache\AdaptiveImage;

class Item implements ItemInterface
{
    use HermitageTrait;
    /**@var string*/
    public $title;
    /**@var string*/
    public $subtitle;
    /**@var string*/
    public $backGround;
    /**@var AdaptiveImage*/
    public $image;
    /**@var Link*/
    public $link;
}

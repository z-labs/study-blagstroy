<?php

namespace ZLabs\BxMustache\About\Stats;

use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\Svg;

class Item implements ItemInterface
{
    /**@var Svg*/
    public $icon;
    /**@var string*/
    public $preNum;
    /**@var string*/
    public $text;
    /**@var string*/
    public $number;
    /**@var string*/
    public $unit;

}
<?php

namespace ZLabs\BxMustache\Sale;

use Illuminate\Support\Collection;
use ZLabs\BxMustache\AdaptiveImage;
use ZLabs\BxMustache\ItemInterface;
use ZLabs\Frontend\MustacheSingleton;

class Detail implements ItemInterface
{
    /** @var int */
    public $id;
    /** @var string */
    public $name;
    /** @var Collection */
    public $mainCharacteristics;
    /** @var Collection */
    public $characteristics;
    /** @var string */
    public $detailText;
    /** @var Collection */
    public $carousels;
    /** @var Collection */
    public $roomTriggers;

    /** @var float */
    public $price;
    /** @var float */
    public $fullPrice;
    /** @var float */
    public $discount;

    public function detailTextDisplay()
    {
        $this->prepareCarousels();
        $this->prepareCharacteristics();
        $this->prepareRoomTriggers();

        return $this->detailText;
    }

    protected function prepareCarousels()
    {
        $this->prepareAssetItems($this->carousels, 'CAROUSEL', 'project-detail-about-slider');
    }

    /**
     * @param Collection $collection
     * @param string $code
     * @param string $template
     * @todo - исправить на шаблон
     */
    protected function prepareAssetItems(Collection $collection, string $code, string $template)
    {
        if ($collection->count() > 0) {
            $collection
                ->values()
                ->each(function ($data, $key) use ($code, $template) {
                    $counter = $key + 1;

                    $this->detailText = str_replace(
                        '#' . $code . '_' . $counter . '#',
                        MustacheSingleton::getInstance()->render($template, $data),
                        $this->detailText
                    );
                });
        } else {
            for ($i = 1; $i < 15; $i++) {
                $this->detailText = str_replace(
                    "#{$code}_${i}#",
                    "",
                    $this->detailText
                );
            }
        }
    }

    protected function prepareCharacteristics()
    {
        $text = '';

        if ($this->characteristics->isNotEmpty()) {
            $text .= MustacheSingleton::getInstance()->render('characteristics-table', $this);
        }

        $this->detailText = str_replace(
            '#CHARACTERISTICS#',
            $text,
            $this->detailText
        );
    }

    protected function prepareRoomTriggers()
    {
        $this->detailText = str_replace(
            "#ROOM_TRIGGERS#",
            MustacheSingleton::getInstance()->render('project-detail-futures', $this),
            $this->detailText
        );
    }

    public function withSale()
    {
        return $this->discount > 0;
    }

    public function price()
    {
        return $this->formatPrice($this->calculatePrice($this->price));
    }

    public function fullPrice()
    {
        return $this->formatPrice($this->calculatePrice($this->fullPrice));
    }

    public function oldFullPrice()
    {
        return $this->formatPrice($this->fullPrice);
    }

    protected function calculatePrice($price)
    {
        if ($this->withSale()) {
            return $price / 100 * (100 - $this->discount);
        }

        return $price;
    }

    protected function formatPrice($price)
    {
        $price = number_format($price, 0, '.', ' ');

        return "$price";
    }
}

<?php

namespace ZLabs\BxMustache;

use Illuminate\Support\Collection;

class Carousel extends Collection
{
    public $key;
    public $countThumbs = 9;

    public function needShowControls()
    {
        return $this->values()->count() > 1;
    }

    public function thumbs()
    {
        if ($this->count() <= $this->countThumbs) {
            return $this->values();
        } else {
            return $this->slice(0, 8);
        }
    }

    public function needShowMoreLink()
    {
        return $this->count() >= $this->countThumbs + 1;
    }

    public function lastThumb()
    {
        /** @var AdaptiveImage $lastThumb */
        $lastThumb = $this
            ->values()
            ->get($this->countThumbs);

        return [
            'count' => $this->count() - $this->countThumbs + 1,
            'index' => $this->countThumbs,
            'mdSrc' => $lastThumb->mdSrc
        ];
    }
}

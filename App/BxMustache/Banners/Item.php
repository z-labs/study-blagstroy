<?php

namespace ZLabs\BxMustache\Banners;

use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\Link;
use ZLabs\BxMustache\Svg;

class Item implements ItemInterface
{
    /**@var string*/
    public $title;
    /**@var string*/
    public $text;
    /**@var Link*/
    public $link;
    /**@var Svg*/
    public $icon;
    /**@var Svg*/
    public $backgroundIcon;
    /**@var string*/
    public $backgroundColor;
}
<?php

namespace ZLabs\BxMustache\Search;

use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\Link;

class Item implements ItemInterface
{
    /**@var string*/
    public $name;
    /**@var string*/
    public $topInfo;
    /**@var string*/
    public $botInfo;
    /** @var Link */
    public $link;
    /** @var mixed */
    public $image;
    /**@var bool*/
    public $isFile = false;

    public function isNull()
    {
        return false;
    }
}

<?php

namespace ZLabs\BxMustache\Company;

use ZLabs\BxMustache\Image;
use ZLabs\BxMustache\ItemInterface;

class EmployeeItem implements ItemInterface
{
    /**@var string*/
    public $name;
    /**@var string*/
    public $position;
    /**@var string*/
    public $experience;
    /**@var string*/
    public $text;
    /**@var Image*/
    public $image;
}

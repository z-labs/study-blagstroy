<?php

namespace ZLabs\BxMustache\Company;

use ZLabs\BxMustache\AdaptiveImage;
use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\Video\Link;

class VideoItem implements ItemInterface
{
    /**@var Link*/
    public $link;
    /**@var AdaptiveImage*/
    public $image;
    /**@var int*/
    public $number;
}

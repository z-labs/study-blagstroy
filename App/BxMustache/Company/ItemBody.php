<?php

namespace ZLabs\BxMustache\Company;

use ZLabs\BxMustache\ItemInterface;

class ItemBody implements ItemInterface
{
    /**@var string*/
    public $code;
    /**@var string*/
    public $text;
}

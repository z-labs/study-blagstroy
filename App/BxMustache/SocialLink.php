<?php

namespace ZLabs\BxMustache;

class SocialLink extends Link implements ItemInterface
{
    use HermitageTrait;

    /** @var Svg */
    public $icon;
}

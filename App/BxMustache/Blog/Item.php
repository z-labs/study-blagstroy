<?php

namespace ZLabs\BxMustache\Blog;

use ZLabs\BxMustache\DetailPageLink;
use ZLabs\BxMustache\HermitageTrait;
use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\AdaptiveImage;

class Item implements ItemInterface
{
    use HermitageTrait;
    use DetailPageLink;
    /**@var string*/
    public $title;
    /**@var string*/
    public $text;
    /**@var AdaptiveImage*/
    public $image;
    /**@var string*/
    public $views;
    /**@var string*/
    public $category;
    /**@var bool*/
    public $isSale = false;
    /**@var string*/
    public $date;

}
<?php

namespace ZLabs\BxMustache\Contacts;

use Illuminate\Support\Collection;
use ZLabs\BxMustache\HermitageTrait;
use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\Link;

class SimpleItem implements ItemInterface
{
    use HermitageTrait;
    /**@var string*/
    public $name;
    /**@var Collection*/
    public $phones;
    /**@var Link*/
    public $email;
    /**@var string*/
    public $workMode;
}
<?php

namespace ZLabs\BxMustache\Contacts;

use ZLabs\BxMustache\ItemInterface;

class Item extends SimpleItem implements ItemInterface
{
    /**@var string*/
    public $address;
    /**@var array*/
    public $workingDays;
    /**@var array*/
    public $weekendDays;
    /**@var array*/
    public $map;
}
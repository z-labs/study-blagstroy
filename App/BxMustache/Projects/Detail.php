<?php

namespace ZLabs\BxMustache\Projects;

use Illuminate\Support\Collection;
use ZLabs\BxMustache\AdaptiveImage;
use ZLabs\BxMustache\Image;
use ZLabs\BxMustache\ItemInterface;
use ZLabs\Frontend\MustacheSingleton;

class Detail implements ItemInterface
{
    /** @var bool */
    public $isArchive = false;
    public $showAsidePrice = true;

    /** @var int */
    public $id;
    /** @var int */
    public $categoryId;
    /** @var Collection */
    public $firstScreenCarousel;
    /** @var AdaptiveImage */
    public $detailPicture;
    /** @var string */
    public $name;
    /** @var string */
    public $address;
    /** @var Collection */
    public $triggers;
    /** @var Collection */
    public $mainCharacteristics;
    /** @var Collection */
    public $characteristics;
    /** @var string */
    public $detailText;
    /** @var Collection */
    public $carousels;
    /** @var Collection */
    public $roomTriggers;

    /** @var Collection */
    public $plansOfObjects;
    /** @var Collection */
    public $plansOfFloors;

    /** @var Collection */
    public $paymentMethods;
    /** @var Collection */
    public $buildProgress;
    /** @var Collection */
    public $documents;

    /** @var Image */
    public $pictureForMap;
    /** @var array */
    public $coords;
    /** @var int */
    public $infrastructureCategory;

    public function hasObjects()
    {
        return $this->plansOfObjects->isNotEmpty();
    }

    public function hasFloors()
    {
        return $this->plansOfFloors->isNotEmpty();
    }

    public function detailTextDisplay()
    {
        $this->prepareCarousels();
        $this->prepareCharacteristics();
        $this->prepareRoomTriggers();

        return $this->detailText;
    }

    protected function prepareCarousels()
    {
        $this->prepareAssetItems($this->carousels, 'CAROUSEL', 'project-detail-about-slider');
    }

    /**
     * @param Collection $collection
     * @param string $code
     * @param string $template
     * @todo - исправить на шаблон
     */
    protected function prepareAssetItems(Collection $collection, string $code, string $template)
    {
        if ($collection->count() > 0) {
            $collection
                ->values()
                ->each(function ($data, $key) use ($code, $template) {
                    $counter = $key + 1;

                    $this->detailText = str_replace(
                        '#' . $code . '_' . $counter . '#',
                        MustacheSingleton::getInstance()->render($template, $data),
                        $this->detailText
                    );
                });
        } else {
            for ($i = 1; $i < 15; $i++) {
                $this->detailText = str_replace(
                    "#{$code}_${i}#",
                    "",
                    $this->detailText
                );
            }
        }
    }

    protected function prepareCharacteristics()
    {
        $text = '';

        if ($this->characteristics->isNotEmpty()) {
            $text .= MustacheSingleton::getInstance()->render('characteristics-table', $this);
        }

        $this->detailText = str_replace(
            '#CHARACTERISTICS#',
            $text,
            $this->detailText
        );
    }

    protected function prepareRoomTriggers()
    {
        $this->detailText = str_replace(
            "#ROOM_TRIGGERS#",
            MustacheSingleton::getInstance()->render('project-detail-futures', $this),
            $this->detailText
        );
    }

    public function needShowPaginationForFirstScreenCarousel()
    {
        return $this->firstScreenCarousel instanceof Collection && $this->firstScreenCarousel->count() > 1;
    }
}

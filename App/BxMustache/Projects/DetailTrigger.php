<?php

namespace ZLabs\BxMustache\Projects;

class DetailTrigger
{
    /** @var string */
    public $name;
    /** @var string */
    public $text;
    /** @var bool */
    public $filledText = false;
}

<?php

namespace ZLabs\BxMustache\Projects;

use Illuminate\Support\Collection;
use ZLabs\BxMustache\HermitageTrait;
use ZLabs\BxMustache\IdTrait;
use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\Svg;

class ObjectItem implements ItemInterface
{
    use IdTrait;
    use HermitageTrait;

    /** @var string */
    public $name;
    /** @var Svg */
    public $icon;
    /** @var string */
    public $deadline;
    /** @var string */
    public $letter;
    /** @var string */
    public $shortName;
    /** @var Collection */
    public $plansOfFloors;
    /** @var bool */
    public $active = false;
}

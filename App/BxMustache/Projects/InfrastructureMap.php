<?php

namespace ZLabs\BxMustache\Projects;

use JsonSerializable;
use Illuminate\Support\Collection;

class InfrastructureMap implements JsonSerializable
{
    /** @var string */
    public $title = 'Инфраструктура';
    /** @var Collection */
    public $categories;
    /** @var array */
    public $item;
    /** @var Collection */
    public $balloons;

    public function toJson()
    {
        return json_encode($this);
    }

    public function jsonSerialize()
    {
        return [
            'item' => $this->item,
            'categories' => $this->categories,
            'balloons' => $this->balloons
        ];
    }
}

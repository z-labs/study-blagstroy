<?php

namespace ZLabs\BxMustache\Projects;

use Illuminate\Support\Collection;
use ZLabs\BxMustache\HermitageTrait;
use ZLabs\BxMustache\IdTrait;
use ZLabs\BxMustache\Image;
use ZLabs\BxMustache\ItemInterface;

class Apartment implements ItemInterface
{
    use IdTrait;
    use HermitageTrait;

    /** @var int */
    public $key;
    /** @var string */
    public $name;
    /** @var int */
    public $room;
    /** @var Collection */
    public $images;
    /** @var Collection */
    public $properties;
    /** @var string */
    public $availableText;
    /** @var Collection */
    public $floors;

    public function floorsAsString()
    {
        if ($this->floors instanceof Collection) {
            return $this->floors->implode(',');
        }

        return '';
    }

    public function imagesAsJson()
    {
        if ($this->images instanceof Collection) {
            return json_encode($this->images->map(function (Image $image) {
                return [
                    'realSrc' => $image->realSrc,
                    'src' => $image->src
                ];
            }));
        }

        return json_encode([]);
    }
}

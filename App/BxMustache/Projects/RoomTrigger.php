<?php

namespace ZLabs\BxMustache\Projects;

use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\Svg;

class RoomTrigger implements ItemInterface
{
    /** @var string */
    public $name;
    /** @var Svg */
    public $icon;
}

<?php

namespace ZLabs\BxMustache\Projects;

use Illuminate\Support\Collection;
use ZLabs\BxMustache\AdaptiveImage;
use ZLabs\BxMustache\HermitageTrait;
use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\IdTrait;
use ZLabs\BxMustache\Link;

class Floor implements ItemInterface
{
    use IdTrait;
    use HermitageTrait;

    /** @var string */
    public $name;
    /** @var bool */
    public $isVertical = false;
    /** @var AdaptiveImage */
    public $image;
    /** @var Collection */
    public $plansOfApartments;
    /** @var bool */
    public $active = false;
    /** @var Link */
    public $printLink;
}

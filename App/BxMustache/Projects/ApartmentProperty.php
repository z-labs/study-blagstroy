<?php

namespace ZLabs\BxMustache\Projects;

use ZLabs\BxMustache\Property\Property;

class ApartmentProperty extends Property
{
    /** @var string */
    public $measure;
    /** @var bool */
    public $filledText = false;
    public $prefixText = 'от';

    protected function prepareValues()
    {
        return parent::prepareValues()
            ->map(function ($value) {
                return "$value {$this->measure}";
            });
    }
}

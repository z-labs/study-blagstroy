<?php

namespace ZLabs\BxMustache\PaymentMethods;

use ZLabs\BxMustache\HermitageTrait;
use ZLabs\BxMustache\IdTrait;
use ZLabs\BxMustache\ItemInterface;
use ZLabs\BxMustache\Link;
use ZLabs\BxMustache\Svg;

class Item implements ItemInterface
{
    use IdTrait;
    use HermitageTrait;

    /** @var Svg */
    public $icon;
    /** @var string */
    public $name;
    /** @var string */
    public $text;
    /** @var Link */
    public $link;
}

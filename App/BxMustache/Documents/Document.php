<?php

namespace ZLabs\BxMustache\Documents;

use ZLabs\BxMustache\Docs\Doc;
use ZLabs\BxMustache\ItemInterface;
use ZLabs\Components\Traits\HermitageTrait;

class Document extends Doc implements ItemInterface
{
    use HermitageTrait;

    /** @var string */
    public $dateAt;
    /** @var int */
    public $categoryId;

    public function iconInline()
    {
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/local/assets/images/file-icons/{$this->extension}.svg")) {
            return file_get_contents(
                $_SERVER['DOCUMENT_ROOT'] . "/local/assets/images/file-icons/{$this->extension}.svg"
            );
        }

        return file_get_contents(
            $_SERVER['DOCUMENT_ROOT'] . "/local/assets/images/file-icons/embed.svg"
        );
    }
}

<?php

namespace ZLabs\Components\Menu\Builder;

use ZLabs\BxMustache\Menu\ItemWithChildren;
use ZLabs\BxMustache\Menu\MenuItems;
use ZLabs\Components\Menu;


class HeaderMenuBuilder extends AbstractBuilder implements BuilderInterface
{
    protected $bxItems;

    public function __construct(array $bxItems)
    {
        $this->bxItems = collect($bxItems);
    }

    public function handleMenuItem()
    {
        return function($arItem) {
            $item = new ItemWithChildren;

            $item->level = $arItem['level'];
            $item->text = $arItem['text'];
            $item->href = $arItem['href'];
            $item->active = $arItem['active'];
            $item->additionalCssClass = isset($arItem['params']['additionalCssClass']) ? $arItem['params']['additionalCssClass'] : null;
            $item->params = $arItem['params'];

            $item->menu = new MenuItems($arItem['menu']->map($this->handleMenuItem()));
            $item->menu->level = $arItem['level'] + 1;

            return $item;
        };
    }

    public function createMenu()
    {
        $this->bxItems = (new Menu\Helpers\Tree($this->bxItems
            ->map(function ($arItem) {
                return [
                    'text' => $arItem['TEXT'],
                    'href' => $arItem['LINK'],
                    'active' => $arItem['SELECTED'],
                    'level' => $arItem['DEPTH_LEVEL'],
                    'depthLevel' => $arItem['DEPTH_LEVEL'],
                    'isParent' => $arItem['IS_PARENT'],
                    'params' => $arItem['PARAMS'],
                ];
            })))
            ->transform();

        $this->menu = new MenuItems($this->bxItems->map($this->handleMenuItem()));
    }
}

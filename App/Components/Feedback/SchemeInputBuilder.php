<?php

namespace ZLabs\Components\Feedback;

use Illuminate\Support\Collection;

class SchemeInputBuilder
{
    protected Collection $scheme;
    protected Collection $sections;
    protected Collection $html_fields;

    public function __construct(Collection $scheme, Collection $html_fields)
    {
        $this->sections = collect([]);
        $this->scheme = $scheme;
        $this->html_fields = $html_fields;

        $this->create();
    }

    protected function create()
    {
        if ($this->scheme->isNotEmpty()) {
            $this->scheme->map(function ($arSection) {
                if ($arSection || $arSection === "") {
                    $fieldIndexes = collect(explode(',', $arSection));

                    $this->createSection($fieldIndexes);
                }
            });
        }
    }

    protected function createSection(Collection $fieldIndexes)
    {
        $section = [
            'html_fields' => $fieldIndexes
                ->filter(function ($fieldIndex) {
                    return is_numeric(+$fieldIndex);
                })
                ->map(function ($fieldIndex) {
                    return $this->html_fields->get(+$fieldIndex - 1);
                })
        ];

        $section['fieldsClass'] = $section['html_fields']->count() > 3 ? 'request-form-fields_columns_3' : 'request-form-fields_columns_' . $section['html_fields']->count();

        $this->sections->push($section);
    }

    public function get(): Collection
    {
        return $this->sections;
    }
}
<?php

namespace ZLabs\Components\Feedback;

use CEvent;
use CIBlockElement;
use Exception;
use CHTTP;
use ZLabs\Components\Feedback\Fields\Types\CustomTypes;
use ZLabs\FeedbackForm\Components\BaseForm;
use ZLabs\FeedbackForm\Field\FieldInterface;
use ZLabs\Models\Agreement\Agreement;
use Bitrix\Main\UserConsent\Consent;
use function explode;

/**
 * Class FeedbackFormCustom
 * @package ZLabs\Components\Feedback
 */
class FeedbackFormCustom extends BaseForm
{
    const FORM_LINK_CSS_CLASS = 'feedback-form-link';

    public function onPrepareComponentParams($arParams)
    {
        $arParams = parent::onPrepareComponentParams($arParams);
        $arParams['email_to'] = collect($arParams['email_to'])->diff([""])->toArray();
        return $arParams;
    }

    protected function executeProlog()
    {
        $this->fieldTypes = new CustomTypes;
    }

    public function executeMain()
    {
        if (!$this->isAjax()) {
            $this->obtainForm();
        } else {
            try {
                CHTTP::SetStatus("200 OK");
                @define("ERROR_404", "N");
                $this->doAction();
            } catch (Exception $exception) {
                $this->arResult['errors'][] = $exception->getMessage();
            }
        }
    }

    protected function obtainForm()
    {
        parent::obtainForm();
        $this->prepareUserConsent();
    }

    protected function prepareUserConsent()
    {
        if ($this->arParams['USER_CONSENT'] && $this->arParams['USER_CONSENT_ID']) {
            $this->arResult['user_consent'] = Agreement::query()
                ->filter(['ID' => $this->arParams['USER_CONSENT_ID']])
                ->cache(600000)
                ->select(['LABEL_TEXT', 'ID'])
                ->first()['LABEL_TEXT'];
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    protected function obtainFields()
    {
        $result = parent::obtainFields();

        $this->arResult['controls'] = collect($this->arResult['fields'])->keyBy('code');

        return $result;
    }

    protected function obtainFormData()
    {
        /** @var FieldInterface $field */
        foreach ($this->arResult['fields'] as $field) {
            if ($field->getTypeAsString() !== 'file') {
                $fieldValue = $this->request->get($field->getCode());
                if ($fieldValue) {
                    $this->formDataAsText .= $field->getTitle() . ': ';
                    if (is_array($fieldValue)) {
                        for ($i = 0; $i < count($fieldValue); $i++) {
                            $this->formDataAsText .= $fieldValue[$i] . ($i + 1 == count($fieldValue) ? "\n" : ', ');
                        }
                    } else {
                        $this->formDataAsText .= $fieldValue . "\n";
                    }
                }
            }
        }

        if (!$this->formDataAsText) {
            throw new \Exception('Данных с формы не поступило');
        }
    }

    protected function submitAction()
    {
        if ($this->needSaveUserConsent()) {
            $this->saveUserConsent();
        }

        if (isset($this->arParams['save_to_iblock']) && $this->arParams['save_to_iblock'] === 'Y' && !empty($this->arParams['class'])) {
            $this->saveToIblock();
        }

        parent::submitAction();
    }

//    protected function getPageUriLink($link)
//    {
//        $href = 'http://'. SITE_SERVER_NAME . $link;
//        return '<a href="'.$href.'">'.$link.'</a>';
//    }

    protected function needSaveUserConsent()
    {
        return $this->arParams['USER_CONSENT'] && $this->arParams['USER_CONSENT_ID'];
    }

    protected function saveUserConsent()
    {
        Consent::addByContext($this->arParams['USER_CONSENT_ID']);
    }

    protected function prepareSuccessData()
    {
        $this->arResult['data']['ya_goals'] = $this->arParams['goals'];
        $this->prepareGaGoals();
        $this->arResult['data']['successMessage'] = $this->arParams['~success_message'];
        $this->arResult['data']['successMessageTitle'] = $this->arParams['~success_message_title'];
    }

    protected function prepareGaGoals()
    {
        if (is_array($this->arParams['ga_goals'])) {
            $this->arResult['data']['ga_goals'] = $this->obtainGAGoals($this->arParams['ga_goals']);
        }
    }

    protected function obtainGAGoals(array $arGoals)
    {
        $arNormalizeGoals = [];

        foreach ($arGoals as $goal) {
            if ($goal) {
                $arGoal = explode(':', $goal);
                $arNormalizeGoals[] = [
                    'eventCategory' => $arGoal[0],
                    'eventAction' => $arGoal[1],
                    'eventLabel' => $arGoal[2]
                ];
            }
        }

        return $arNormalizeGoals;
    }

    protected function sendMessages()
    {
        $arEventMessageId = $this->arParams['event_message_id'];
        $emailTo = $this->arParams['email_to'];

        if (!$emailTo) {
            throw new Exception('Отсутствует email получателя формы');
        }

        if (!$arEventMessageId) {
            throw new Exception('Не найдены почтовые шаблоны, по которым необходимо отправить форму');
        }

        global $APPLICATION;

        $productName = "";
        if (isset($this->arParams['PRODUCT_ID']) && $this->arParams['PRODUCT_ID']) {
            $element = CIBlockElement::GetByID($this->arParams['PRODUCT_ID'])->fetch();
            if ($element) {
                $productName = 'Товар: ' . $element['NAME'] . '<br>';
            }
        }

        $userName = $this->request['name'];

        foreach ($arEventMessageId as $eventMessageId) {
            CEvent::Send(
                'ZLABS_FEEDBACK',
                SITE_ID,
                array(
                    'EMAIL_TO' => $emailTo,
                    'FORM_NAME' => $this->arParams['name'],
                    'DATA' => $this->formDataAsText,
                    'DATE_TIME' => date('d.m.Y'),
                    'PRODUCT_NAME' => $productName,
                    'USER_NAME' => $userName,
                    'PAGE_URI' => $APPLICATION->GetCurUri() == '/' ? 'Главная страница' : $APPLICATION->GetCurUri(),
                ),
                'Y',
                $eventMessageId,
                $this->arFileIds
            );
        }
    }

    protected function saveToIblock() {
        $title = 'Заявка от ';

        if ($_REQUEST['name']) {
            $title .= $_REQUEST['name'] . ' от ';
        }

        $title .= date('d.m.Y');

        $this->arParams['class']::create([
            'ACTIVE' => 'Y',
            'NAME' =>  $title,
            "PROPERTY_VALUES"=> $this->getProps($this->arParams['class']::iblockId()),
        ]);
    }

    protected function getProps($iblockId)
    {
        $ret = [];
        $res = \CIBlock::GetProperties($iblockId, array(), array());
        while ($resArr = $res->Fetch()) {
            $value = $this->request->get(strtolower($resArr['CODE']));

            if (is_string($value)) {
                $value = trim($value);
            }

            if (is_array($value)) {
                $value = implode(', ', $value);
            }

            if (!$value && !empty($_FILES)) {
                $value = $_FILES[strtolower($resArr['CODE'])];

                // проверка размер загружаемого файла меньше 10Мб
                if ($value['size'] > 1048576) {
                    $value = null;
                }
            }

            if ($value) $ret[$resArr['ID']] = $value;
        }

        return $ret;
    }
}



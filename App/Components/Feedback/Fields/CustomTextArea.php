<?php

namespace ZLabs\Components\Feedback\Fields;

use ZLabs\FeedbackForm\Field\TextAreaField;
use ZLabs\Components\Feedback\Fields\Traits\CustomRequired;

class CustomTextArea extends TextAreaField
{
    use CustomRequired;
}

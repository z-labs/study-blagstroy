<?php

namespace ZLabs\Components\Feedback\Fields;

use ZLabs\Components\Feedback\Fields\Traits\CustomRequired;
use ZLabs\FeedbackForm\Components\Parameters\ComponentParametersInterface;
use ZLabs\FeedbackForm\Field\Helpers\ParamKeyHelper;
use ZLabs\FeedbackForm\Field\Types\TypesInterface;
use ZLabs\FeedbackForm\Field\BaseAbstract;
use ZLabs\FeedbackForm\Field\Traits\PossibleValues;


class CustomCheckbox extends BaseAbstract
{
    use CustomRequired;
    use PossibleValues;

    public $multiple;
    public $buttonMode;
    public $items = [];


    public function __construct(int $i, array $arParams, TypesInterface $types = null)
    {
        parent::__construct($i, $arParams, $types);

        $multiple = $this->getParam('multiple');
        $buttonMode = $this->getParam('buttonMode');

        $this->multiple = $multiple ? $multiple === 'Y' : false;
        $this->buttonMode = $buttonMode ? $buttonMode === 'Y' : false;
        $this->items = $this->generatePossibleValuesContext();
    }

    public function generateComponentParameters(ComponentParametersInterface $componentParameters)
    {
        parent::generateComponentParameters($componentParameters);

        $componentParameters->addParameter(
            ParamKeyHelper::getParamKey($this->index, 'multiple'),
            $this->generateMultipleParam()
        );

        $componentParameters->addParameter(
            ParamKeyHelper::getParamKey($this->index, 'buttonMode'),
            $this->generateButtonModeParam()
        );

        $componentParameters->addParameter(
            ParamKeyHelper::getParamKey($this->index, 'possible_values'),
            $this->generatePossibleValuesParam()
        );
    }

    protected function generateMultipleParam()
    {
        return [
            'PARENT' => ParamKeyHelper::getGroupKey($this->index),
            'NAME' => 'Множественные значения',
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'N'
        ];
    }

    protected function generateButtonModeParam()
    {
        return [
            'PARENT' => ParamKeyHelper::getGroupKey($this->index),
            'NAME' => 'Отображение в виде кнопок?',
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'N'
        ];
    }

    protected function generatePossibleValuesContext() {
        $values = collect($this->getParam('possible_values'));

        if ($values->isNotEmpty()) {
            $code = $values->count() > 1 ? $this->getParam('code') . '[]' : $this->getParam('code');

            return $values
                ->filter(function ($arValue) {
                    return !!$arValue;
                })
                ->map(function ($arValue, $key) use ($code) {
                return [
                    'index' => $key,
                    'code' => $code,
                    'value' => $arValue,
                    'text' => $arValue,
                    'checked' => !$this->multiple && $key === 0,
                ];
            });
        }
    }

    public function getTypeAsString()
    {
        return 'checkbox';
    }

    public function singleValue()
    {
        return $this->items->count() === 1;
    }
}

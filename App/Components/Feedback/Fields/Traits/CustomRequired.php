<?php

namespace ZLabs\Components\Feedback\Fields\Traits;

use ZLabs\FeedbackForm\Field\Helpers\ParamKeyHelper;

trait CustomRequired
{
    protected $index;
    public $required;

    protected function generateRequireParam()
    {
        return [
            'PARENT' => ParamKeyHelper::getGroupKey($this->index),
            'NAME' => 'Обязательное для заполнения',
            'TYPE' => 'CHECKBOX'
        ];
    }

    public function requiredCssClass()
    {
        $additionalCssClass = '';
        if ($this->required) {
            $additionalCssClass .= ' form__control_required';
        }

        return $additionalCssClass;
    }
}

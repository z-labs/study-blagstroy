<?php

namespace ZLabs\Components\Feedback\Fields;

use ZLabs\FeedbackForm\Field\ListField;

class CustomList extends ListField
{
    public function getTypeAsString()
    {
        return 'select';
    }
}

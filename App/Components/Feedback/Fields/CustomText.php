<?php

namespace ZLabs\Components\Feedback\Fields;

use ZLabs\FeedbackForm\Field\TextField;
use ZLabs\Components\Feedback\Fields\Traits\CustomRequired;
use ZLabs\FeedbackForm\Field\Types\TypesInterface;

class CustomText extends TextField
{
    use CustomRequired;

    const TEXT_FIELD_MASK = [
        'simple' => 'Обычное',
        'phone' => 'Телефонный номер',
        'email' => 'Электронный адрес',
        'phone_or_email' => 'Телефон или электронная почта',
        'ru_or_en' => 'Русские буквы'
    ];

    public function __construct(int $i, array $arParams, TypesInterface $types = null)
    {
        parent::__construct($i, $arParams, $types);

        if ($this->mask === 'phone') {
            $this->typeInput = 'tel';
            $this->additional_css_class = ' form__control_tel';
        }
    }

    public function maskCssClass()
    {
        switch ($this->mask) {
            case 'phone':
                return ' form__control_valid-tel';
                break;
            case 'email':
                return ' form__control_valid-email';
                break;
            case 'phone_or_email':
                return ' form__control_valid-email-tel';
                break;
            case 'ru_or_en':
                return ' form__control_valid-text-ru';
                break;
        }

        return '';
    }
}

<?php

namespace ZLabs\Components\Feedback\Fields\Types;

use ZLabs\Components\Feedback\Fields\CustomCheckbox;
use ZLabs\Components\Feedback\Fields\CustomTextArea;
use ZLabs\Components\Feedback\Fields\CustomText;
use ZLabs\FeedbackForm\Field\Types\DefaultTypes;
use ZLabs\FeedbackForm\Field\Types\TypesInterface;
use ZLabs\FeedbackForm\Field;

class CustomTypes extends DefaultTypes implements TypesInterface
{
    protected $arTypes = [
        CustomText::class => 'Текстовое поле',
        CustomTextArea::class => 'Поле ввода сообщения',
        CustomCheckbox::class => 'Поле выбора вариантов',
        Field\ListField::class => 'Список',
        Field\FileField::class => 'Файл',
        Field\HiddenField::class => 'Скрытое поле',
    ];
}
